'use strict';

var path = require('path');
const Sitemap = require('./_bin/sitemap');
var packageJson = require('./package.json');
var path = require('path');
var swPrecache = require('sw-precache');


module.exports = function (grunt) {



    require('load-grunt-tasks')(grunt);
    /* require('./screenshots.js')(grunt);*/

    require('time-grunt')(grunt);


    let viewports = [{
        'name': 'xs',
        'size': [375, 667],
        'model': 'iPhone 6s portrait'
    }, {
        'name': 'sm',
        'size': [667, 375],
        'model': 'iPhone 6s landscape'
    }, {
        'name': 'md',
        'size': [768, 1024],
        'model': 'iPad mini 4 portrait'
    }, {
        'name': 'lg',
        'size': [1024, 768],
        'model': 'iPad mini 4 landscape'
    }, {
        'name': 'xl',
        'size': [1366, 1024],
        'model': 'iPad Pro landscape'
    }];





    let sitemapObject = new Sitemap('_source/content/sitemap.yaml');


    var options = {
        config: {
            src: '_grunt-config/*.*'
        },
        viewports: viewports
    };


    //loads the various task configuration files
    var configs = require('load-grunt-configs')(grunt, options);


    configs.screenshots = {
        options: {
            output: '_build/debug/_screenshots/images/'
        }
    };

    let shots = [];
    sitemapObject.linearization.forEach(page => {
        shots.push({
            name: page.name,
            url: 'http://localhost:9001' + page.url
        })
    });

    viewports.forEach(function (viewport) {
        let subtask = {};
        subtask.options = {};
        subtask.options.viewports = [viewport];
        subtask.shots = shots;
        configs.screenshots[viewport.name] = subtask;
    });

    grunt.initConfig(configs);

    grunt.registerTask('readpkg', 'Read in the package.json file', function () {
        grunt.config.set('pkg', grunt.file.readJSON('./package.json'));
    });


    grunt.registerTask('build', ['webpack', 'pug', 'grunticon', 'stylus', 'newer:imagemin', 'copy', 'cssmin', 'cacheBust']);

    grunt.registerTask('default', ['build']);

    grunt.registerTask('generate-atlas-files', ['execute:generate-atlas-files', 'copy:content']);



    // grunt.initConfig({
    //     swPrecache: {
    //         dev: {
    //             handleFetch: false,
    //             rootDir: './_build/dev'
    //         }
    //     }
    // });

    function writeServiceWorkerFile(rootDir, handleFetch, callback) {

        var config = {
            cacheId: packageJson.name,

            // If handleFetch is false (i.e. because this is called from swPrecache:dev), then
            // the service worker will precache resources but won't actually serve them.
            // This allows you to test precaching behavior without worry about the cache preventing your
            // local changes from being picked up during the development cycle.
            handleFetch: handleFetch,
            logger: grunt.log.writeln,
            staticFileGlobs: [
                // rootDir + '/css/**.css',
                rootDir + '/**.html',
                rootDir + '/_shared/**/*.*'
                // rootDir + '/js/**.js'
            ],
            stripPrefix: rootDir + '/',
            // verbose defaults to false, but for the purposes of this demo, log more.
            verbose: true
        };

        swPrecache.write(path.join(rootDir, 'service-worker.js'), config, callback);
    }

    grunt.registerTask('swPrecache', function () {

        console.log("XXX")
        var done = this.async();
        var rootDir = './_build/dev'; //this.data.rootDir;
        var handleFetch = false; //this.data.handleFetch;

        writeServiceWorkerFile(rootDir, handleFetch, function (error) {
            if (error) {
                grunt.fail.warn(error);
            }
            done();
        });
    });

};