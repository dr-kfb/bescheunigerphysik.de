"use strict";

var d3 = require("d3")

export class NavigationEnhancer {

    constructor() {

          d3.selectAll(".c-navigation__menu-item--menu")
            .on("click", function() {
                let evt = d3.event;
                evt.preventDefault();
                evt.stopPropagation();
                d3.select(".page").classed("c-navigation--open", !d3.select(".page").classed("c-navigation--open"))
            });

        d3.selectAll(".c-navigation")
            .on("mouseover", function() {
                let evt = d3.event;
                evt.preventDefault();
                evt.stopPropagation();
                d3.select(".page").classed("c-navigation--open", true)
            }).on("mouseout", function() {
                let evt = d3.event;
                evt.preventDefault();
                evt.stopPropagation();
                d3.select(".page").classed("c-navigation--open", false)
            });

        // d3.selectAll(".c-navigation__menu-item--menu")
        //     .on("click", function() {
        //         let evt = d3.event;
        //         evt.preventDefault();
        //         evt.stopPropagation();
        //         d3.select(".page").classed("c-navigation--open", !d3.select(".page").classed("c-navigation--open"))
        //     });


        // d3.selectAll(".c-navigation a.tooltipped.menu__item-link")
        //     .on("mouseover", function(e) {

        //         let evt = d3.event

        //         let target = evt.target || evt.srcElement

        //         while (d3.select(target).node() && d3.select(target).node().tagName !== "A") {
        //             target = d3.select(target).node().parentNode;
        //         }

        //         var anchor = d3.select(target);
        //         var itemGroup = d3.select(".c-navigation__tooltip");
        //         var tooltipText = anchor.attr("title");
        //         itemGroup.text(tooltipText)
        //         // d3.select(".c-navigation__tooltip", itemGroup).text(tooltipText);
        //     })
        //     .on("mouseout",
        //         function(e) {
        //         var itemGroup = d3.select(".c-navigation__tooltip");
        //         itemGroup.text("")
        //         });

        // $(".c-navigation [data-hoverlink!='']")
        //     .hover(
        //         function() {
        //             var hoverLink = $(this).data("hoverlink");
        //             if (hoverLink) {
        //                 $(".c-navigation [data-hoverlink='" + hoverLink + "']").addClass("hover");
        //             }
        //         },
        //         function() {
        //             var hoverLink = $(this).data("hoverlink");
        //             if (hoverLink)
        //                 $(".c-navigation [data-hoverlink='" + hoverLink + "']").removeClass("hover");
        //         }
        //     )
    }
}
