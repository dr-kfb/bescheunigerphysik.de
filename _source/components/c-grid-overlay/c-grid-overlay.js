'use strict';

const d3 = require("d3");


export class PerfectGridBuilder {

    constructor(id, window, document) {

        this.millimetersPerPoint = 0.3528;
        this.width = 210 / this.millimetersPerPoint;
        this.height = 297 / this.millimetersPerPoint;

        this.lineHeight = 10.5;

        this.numberOfBaselines = Math.floor(this.height / this.lineHeight);

        this.columnCount = 12;
        this.columnWidth = this.lineHeight * 3;
        this.columnGap = this.lineHeight * 1;

        this.marginHorizontal =
            this.width -
            (this.columnCount) * this.columnWidth -
            (this.columnCount - 1) * this.columnGap;

        this.marginLeft = this.marginHorizontal * 0.5;
        this.marginRight = this.marginHorizontal * 0.5;

        this.container = d3.select(id)
            .append("div")
            .classed("svg-container", true) //container class to make it responsive
            .append('svg')
            // .classed("svg-content-responsive", true)
            // .attr("preserve-aspect-ratio", "xMidYMid meet")
            // .attr("viewBox", "0 0 " + this.height + " " + this.width)
            .attr("height", this.height)
            .attr("width", this.width)
            //class to make it responsive
            .append('g');

        this.drawGrid();

    }


    draw() {


    }


    drawGrid() {


        this.container.selectAll("line.baseline")
            .data(d3.range(this.numberOfBaselines + 1))
            .enter()
            .append('line')
            .classed("baseline", true)
            .attr('x1', 0)
            .attr('y1', d => d * this.lineHeight)
            .attr('x2', this.width)
            .attr('y2', d => d * this.lineHeight);

        this.container
            .append('line')
            .classed("pageMarginVertical", true)
            .attr('x1', this.width)
            .attr('y1', 0)
            .attr('x2', this.width)
            .attr('y2', this.height);

        let columns = this.container.selectAll("g.column")
            .data(d3.range(this.columnCount))
            .enter().append('g')
            .classed("column", true)
            .attr('transform', d => "translate(" + (d * (this.columnWidth + this.columnGap) + this.marginLeft) + ", 0)")

        columns.selectAll("line.columnLeft")
            .data([0])
            .enter()
            .append("line")
            .classed("columnLeft", true)
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', 0)
            .attr('y2', this.height);
        columns.selectAll("line.columnRight")
            .data([0])
            .enter()
            .append("line")
            .classed("columnRight", true)
            .attr('x1', this.columnWidth)
            .attr('y1', 0)
            .attr('x2', this.columnWidth)
            .attr('y2', this.height);
        columns.selectAll("line.columnSub")
            .data(d3.range(4))
            .enter()
            .append('line')
            .classed("columnSub", true)
            .attr('x1', d => d * (this.lineHeight))
            .attr('y1', 0)
            .attr('x2', d => d * (this.lineHeight))
            .attr('y2', this.height);

        let rows = this.container.selectAll("g.row")
            .data(d3.range(20))
            .enter().append('g')
            .classed("row", true)
            .attr('transform', d => "translate(0, " + (d * (this.columnWidth + this.columnGap)) + ")")
        rows.selectAll("line.rowTop")
            .data([0])
            .enter()
            .append("line")
            .classed("rowTop", true)
            .attr('x1', 0)
            .attr('y1', 0)
            .attr('x2', this.width)
            .attr('y2', 0);
        rows.selectAll("line.rowBottom")
            .data([0])
            .enter()
            .append("line")
            .classed("rowBottom", true)
            .attr('x1', 0)
            .attr('y1', this.lineHeight * 3)
            .attr('x2', this.width)
            .attr('y2', this.lineHeight * 3);


    }

}
