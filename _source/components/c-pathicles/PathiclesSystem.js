const ParticleFactory = require('./ParticleFactory').ParticleFactory;

const Pathicles = require('./Pathicles').Pathicles;

const THREE = require('three')

class PathiclesSystem {


    constructor(initialParticles) {

        this.particles = (initialParticles || []).slice();
        this.fields = [];
        this.t0 = 0;
    }

    addField(field) {

        this.fields.push(field)

    }

    simulate(n_t, d_t) {

        this.maxLocation = new THREE.Vector3(-1000, -1000, -1000);
        this.minLocation = new THREE.Vector3(1000, 1000,
            1000);

        for (let t = 0; t < n_t * d_t; t += d_t) {

            this.particles.forEach((particle, i_particle) => {

                let currentLocation = particle.states[particle.states.length - 1].location;
                let currentVelocity = particle.states[particle.states.length - 1].velocity;

                let acceleration = new THREE.Vector3();

                let newLocation = new THREE.Vector3(currentLocation.x, currentLocation.y, currentLocation.z);
                let newVelocity = new THREE.Vector3(currentVelocity.x, currentVelocity.y, currentVelocity.z);






                this.fields.forEach(field => {


                    let accelerationByField = new THREE.Vector3();

                    if (field.constructor.name === "ParticleInteractionField") {

                        this.particles.forEach((particle_2, i_particle_2) => {
                            let d = currentLocation.distanceTo(particle_2.location)
                            if (d > 0) {

                                accelerationByField.add(currentLocation.clone().sub(particle_2.location).normalize().multiplyScalar(.0001 / d / d))
                            }

                        });

                    } else {

                        accelerationByField = field.getForce(particle);
                    }
                    acceleration.x += accelerationByField.x;
                    acceleration.y += accelerationByField.y;
                    acceleration.z += accelerationByField.z;


                })



                if (particle.mass != 0) {
                    newVelocity.x += acceleration.x * d_t / particle.mass;
                    newVelocity.y += acceleration.y * d_t / particle.mass;
                    newVelocity.z += acceleration.z * d_t / particle.mass;
                }
                // console.log("1", newVelocity.z)

                newLocation.x += newVelocity.x * d_t;
                newLocation.y += newVelocity.y * d_t;
                newLocation.z += newVelocity.z * d_t;


                // console.log("2", newLocation)

                particle.states.push({
                    location: newLocation,
                    velocity: newVelocity
                });
                particle.location = newLocation;
                particle.velocity = newVelocity;

                // console.log(newLocation.z)


                this.maxLocation.x = Math.max(this.maxLocation.x, newLocation.x)
                this.maxLocation.y = Math.max(this.maxLocation.y, newLocation.y)
                this.maxLocation.z = Math.max(this.maxLocation.z, newLocation.z)

                this.minLocation.x = Math.min(this.minLocation.x, newLocation.x)
                this.minLocation.y = Math.min(this.minLocation.y, newLocation.y)
                this.minLocation.z = Math.min(this.minLocation.z, newLocation.z)

            })
        }

        // console.log(this.minLocation, this.maxLocation);


    }


}

PathiclesSystem.create = function (config) {

    let n_y = config.n_y ||  0
    let n_z = config.n_z ||  0
    let spread_y = config.spread_y ||  0
    let spread_z = config.spread_z ||  0
    let jitter_location = config.jitter_location ||  0
    let jitter_velocity = config.jitter_velocity ||  0
    let particle_type_generator = config.particle_type_generator ||  "electrons"

    console.log(particle_type_generator)
    let particleFactory = new ParticleFactory();

    let initialParticles = [];

    let templates = [
        ParticleFactory.PROTON,
        ParticleFactory.PROTON,
        ParticleFactory.ELECTRON,
        ParticleFactory.PROTON,
        ParticleFactory.ELECTRON,
        ParticleFactory.PROTON,
        ParticleFactory.PROTON,
        ParticleFactory.ELECTRON,
        ParticleFactory.PHOTON,
        ParticleFactory.ELECTRON,
        ParticleFactory.ELECTRON,
        ParticleFactory.PHOTON
    ];

    let n = 0;
    for (let i_z = 0; i_z < n_z; i_z++) {

        let z = 0;
        if (n_z !== 1) {
            z = (spread_z / (n_z - 1)) * i_z - (spread_z) * 0.5;
        }

        for (let i_y = 0; i_y < n_y; i_y++) {

            let y = 0;
            if (n_y !== 1) {
                y = (spread_y / (n_y - 1)) * i_y - (spread_y) * 0.5;
            }

            let template;
            n++;


            switch (particle_type_generator) {
                case "electrons":
                    template = ParticleFactory.ELECTRON;
                    break;
                case "photons":
                    template = ParticleFactory.PHOTON;
                    break;
                case "protons":
                    template = ParticleFactory.PROTON;
                    break;
                case "kfb":
                    template = templates[((n - 1) % 12)];
                    break;

                default:
                    template = ParticleFactory.ELECTRON;
                    break;
            }



            let random_location = jitter_location
            let random_x = Pathicles.randomSymmetric() * random_location;
            let random_y = Pathicles.randomSymmetric() * random_location;
            let random_z = Pathicles.randomSymmetric() * random_location;

            let random_velocity = jitter_velocity
            let random_vx = Pathicles.randomSymmetric() * random_velocity;
            let random_vy = Pathicles.randomSymmetric() * random_velocity;
            let random_vz = Pathicles.randomSymmetric() * random_velocity;

            initialParticles.push(particleFactory.createParticle(
                template, new THREE.Vector3(0 + random_x,
                    y + random_y, z + random_z
                ), new THREE.Vector3(1 + random_x,
                    random_vy, random_vy)));

        }
    }


    let system = new PathiclesSystem(initialParticles);
    return system;


}


module.exports = PathiclesSystem;