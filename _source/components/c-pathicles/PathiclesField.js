var THREE = require("three")


class PathiclesField {

}

class ParticleInteractionField extends PathiclesField {
}


class GravitationalField extends PathiclesField {

    constructor() {

        super()
    }

    getForce(particle) {
        return {
            x: 0,
            y: -1 * particle.mass,
            z: 0
        };
    }
}

class DipoleField extends PathiclesField {


    constructor(x0, x1, fieldVector) {

        super()

        this.x0 = x0;
        this.x1 = x1;
        this.fieldVector = fieldVector;


    }

    getFieldValue(location) {

        if (location.x >= this.x0 && location.x <= this.x1) {

            return this.fieldVector.clone();

        } else {

            return new THREE.Vector3()

        }
    }


    getForce(particle) {

        

        var force =
            (new THREE.Vector3(particle.velocity.x, particle.velocity.y, particle.velocity.z))
            .cross(this.getFieldValue(particle.location))
            .multiplyScalar(particle.charge);

        return force;


    }


    createCubeRepresentation(material) {

        if (!material) {

            material = new THREE.MeshLambertMaterial({
                color: 0x999999,
                opacity: 0.5,
                transparent: true
            });
        }
        var cube = new THREE.Mesh(new THREE.CubeGeometry(x1 - x0, 50, 100), material);
        cube.position.x = (x1 - x0) / 2 + x0;
        return cube;
    }
}

class QuadrupoleField extends PathiclesField {


    constructor(x0, x1, strength, rotation) {

        super()

        this.x0 = x0;
        this.x1 = x1;
        this.strength = strength;
        this.rotation = rotation || 0;


    }



    getFieldValue(location) {

        if (location.x >= this.x0 && location.x <= this.x1) {

            if (this.rotation) {
                return new THREE.Vector3(0, -location.z, -location.y).multiplyScalar(this.strength);

            } else {
                return new THREE.Vector3(0, location.z, location.y).multiplyScalar(this.strength);
            }

        } else {

            return new THREE.Vector3();
        }
    }

    getForce(particle) {

        var force =
            (new THREE.Vector3(particle.velocity.x, particle.velocity.y, particle.velocity.z))
            .cross(this.getFieldValue(particle.location))
            .multiplyScalar(100* particle.charge);



        return force;

    }

    createCubeRepresentation(material) {

        if (!material) {

            material = new THREE.MeshLambertMaterial({
                color: 0x999999,
                opacity: 0.5,
                transparent: true
            });
        }
        var cube = new THREE.Mesh(new THREE.CubeGeometry(x1 - x0, 50, 100), material);
        cube.position.x = (x1 - x0) / 2 + x0;
        return cube;
    }
}




module.exports.GravitationalField = GravitationalField;
module.exports.ParticleInteractionField = ParticleInteractionField;
module.exports.DipoleField = DipoleField;
module.exports.QuadrupoleField = QuadrupoleField;