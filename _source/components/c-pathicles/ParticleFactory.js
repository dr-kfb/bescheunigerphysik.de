class ParticleFactory {


    constructor() {}


    createParticle(particleTemplate, location, velocity) {

        var particle = JSON.parse(JSON.stringify(particleTemplate))
        if (location)
            particle.location = location;

        if (velocity)
            particle.velocity = velocity;

        particle.states = []

        particle.states.push({
            location: location,
            velocity: velocity
        })


        return particle;

    }

};





ParticleFactory.PROTON = {
    location: {
        x: 0,
        y: 0,
        z: 0

    },
    velocity: {
        x: 0,
        y: 0,
        z: 0
    },
    type: "proton",
    mass: 1,
    charge: +1,
        color: {
        r: 0xc5,
        g: 0x32,
        b: 0x28,
        value: 0xc53228
    }
    

}
ParticleFactory.PHOTON = {
    location: {
        x: 0,
        y: 0,
        z: 0

    },
    velocity: {
        x: 0,
        y: 0,
        z: 0
    },
    mass: 0,
    type: "photon",
    charge: 0,
        color: {
        r: 0xed,
        g: 0xc5,
        b: 0x00,
        value: 0xedc500
    }
    
}

ParticleFactory.ELECTRON = {
    location: {
        x: 0,
        y: 0,
        z: 0

    },
    velocity: {
        x: 0,
        y: 0,
        z: 0
    },
    type: "electron",
    mass: .1,
    charge: -1,
    color: {
        r: 0x21,
        g: 0x74,
        b: 0xa8,
        value: 0x2174a8
    }, 
    

}


module.exports.ParticleFactory = ParticleFactory;