const ParticleFactory = require('./ParticleFactory').ParticleFactory;
const THREE = require('three');

test('exists', () => {

    let particleFactory = new ParticleFactory();
    
    expect(particleFactory).toBeDefined();

});


test('create proton', () => {

    let particleFactory = new ParticleFactory();

    let proton = particleFactory.createParticle(ParticleFactory.PROTON);
    
    expect(proton).toBeDefined();
    expect(proton.mass).toBe(1);
    expect(proton.location).toEqual({x:0,y:0,z:0});


    let proton_1 = particleFactory.createParticle(ParticleFactory.PROTON, 
    new THREE.Vector3(1,1,1), new THREE.Vector3(2,2,2));

    expect(proton_1.location).toEqual({x:1,y:1,z:1});
    expect(proton_1.velocity).toEqual({x:2,y:2,z:2});

});


// var random = Math.random;
// var seed = 1;
// random = function () {
//     var x = Math.sin(seed++) * 10000;
//     return x - Math.floor(x);
// }

// var fields = [];
// fields.push(new GravitationalField())
// fields.push(new DipoleField(-10, 70))

// var proton = JSON.parse(JSON.stringify(ParticleHistory.PROTON))
// proton.location.z = -10;
// proton.velocity.x = 100;


// var protonHistory = new ParticleHistory(proton, 0, 1, 10, fields);

// console.log(JSON.stringify(protonHistory.asVector3Array(), null, "  "))
// console.log(fields[1].acceleration(proton))