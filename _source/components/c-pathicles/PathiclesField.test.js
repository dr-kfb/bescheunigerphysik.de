var THREE = require("three")
const DipoleField = require('./PathiclesField').DipoleField;
const QuadrupoleField = require('./PathiclesField').QuadrupoleField;


const ParticleFactory = require('./ParticleFactory').ParticleFactory;


test('dipoleField', () => {

    let dipoleField = new DipoleField(50, 100, new THREE.Vector3(0,-1,0));
    
    expect(dipoleField).toBeDefined();

    expect(dipoleField.getFieldValue(new THREE.Vector3(0,0,0))).toEqual(new THREE.Vector3());
    expect(dipoleField.getFieldValue(new THREE.Vector3(75,0,0))).toEqual(new THREE.Vector3(0,-1,0));

});

test('dipoleField', () => {

    let field = new QuadrupoleField(100, 200, 1);
    
    expect(field).toBeDefined();

    expect(field.getFieldValue(new THREE.Vector3(0,0,0))).toEqual(new THREE.Vector3());
    expect(field.getFieldValue(new THREE.Vector3(150,0,0))).toEqual(new THREE.Vector3(0,0,0));
    expect(field.getFieldValue(new THREE.Vector3(150,1,0))).toEqual(new THREE.Vector3(0,0,1));
});


test('QuadrupoleField', () => {

    let field = new QuadrupoleField(0, 1, 1, true);
    
    expect(field).toBeDefined();

    expect(field.getFieldValue(new THREE.Vector3(0,0,0))).toEqual(new THREE.Vector3());
    expect(field.getFieldValue(new THREE.Vector3(.5,0,0))).toEqual(new THREE.Vector3(0,0,0));
    expect(field.getFieldValue(new THREE.Vector3(.5,1,0))).toEqual(new THREE.Vector3(0,0,-1));

    let particleFactory = new ParticleFactory();
    let p1 = particleFactory.createParticle(ParticleFactory.ELECTRON, new THREE.Vector3(.5,1,0),
    new THREE.Vector3(1,0,0));

    expect(field.getFieldValue(p1.location)).toEqual(new THREE.Vector3(0,0,-1));
    expect(field.getForce(p1)).toEqual(new THREE.Vector3(0,0,-1));
});