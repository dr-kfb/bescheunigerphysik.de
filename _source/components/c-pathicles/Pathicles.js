

let Pathicles = {}
Pathicles.seed = 1;

Pathicles.setSeed = function(newSeed) {
    Pathicles.seed = newSeed;
}

Pathicles.random = function () {
    var x = Math.sin(Pathicles.seed++) * 10000;
    return x - Math.floor(x);
}
Pathicles.randomSymmetric = function () {
    var x = Math.sin(Pathicles.seed++) * 10000;
    return (x - Math.floor(x) - 0.5) * 2;
}

Pathicles.setSeed(1)

module.exports.Pathicles = Pathicles;