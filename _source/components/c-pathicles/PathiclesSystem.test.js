const PathiclesSystem = require('./PathiclesSystem');

const Pathicles = require('./Pathicles').Pathicles;

const THREE = require('three')


test('exists', () => {

    let pathiclesSystem = new PathiclesSystem();


    expect(pathiclesSystem).toBeDefined();

});



test('1x1', () => {

    Pathicles.setSeed(1)

    let config = {
        n_y: 1,
        n_z: 1,
        spread_y: 1,
        spread_z: 1,
        jitter_location: 0,
        jitter_velocity: 0
    }

    let pathiclesSystem = new PathiclesSystem.create(config);

    expect(pathiclesSystem).toBeDefined();

    expect(pathiclesSystem.particles.length).toEqual(1);

    expect(pathiclesSystem.particles[0].location).toEqual(new THREE.Vector3());

    // pathiclesSystem.simulate(32, .05);

    // expect(pathiclesSystem.particles[0].states.length).toEqual(33);

    // console.log(JSON.stringify(pathiclesSystem, null, "  "));

});



test('1x3', () => {

    Pathicles.setSeed(1)

    spread_z = 1;

    let config = {
        n_y: 1,
        n_z: 3,
        spread_y: 1,
        spread_z: spread_z,
        jitter_location: 0,
        jitter_velocity: 0
    }

    let pathiclesSystem = new PathiclesSystem.create(config);

    expect(pathiclesSystem).toBeDefined();

    expect(pathiclesSystem.particles.length).toEqual(3);

    expect(pathiclesSystem.particles[0].location).toEqual(new THREE.Vector3(0, 0, -spread_z / 2));
    expect(pathiclesSystem.particles[1].location).toEqual(new THREE.Vector3(0, 0, 0));
    expect(pathiclesSystem.particles[2].location).toEqual(new THREE.Vector3(0, 0, spread_z / 2));


});




test('1x4', () => {

    Pathicles.setSeed(1)

    spread_z = 1;

    let config = {
        n_y: 1,
        n_z: 4,
        spread_y: 1,
        spread_z: spread_z,
        jitter_location: 0,
        jitter_velocity: 0, 
        particle_type_generator: "photons"
    }

    let pathiclesSystem = new PathiclesSystem.create(config);

    expect(pathiclesSystem).toBeDefined();

    expect(pathiclesSystem.particles.length).toEqual(4);

    expect(pathiclesSystem.particles[0].type).toEqual("photon")

    expect(pathiclesSystem.particles[0].location).toEqual(new THREE.Vector3(0, 0, -spread_z / 2));
    expect(pathiclesSystem.particles[1].location).toEqual(new THREE.Vector3(0, 0, spread_z / 3 - spread_z / 2));
    expect(pathiclesSystem.particles[3].location).toEqual(new THREE.Vector3(0, 0, spread_z / 2));


});





test('3x4', () => {

    Pathicles.setSeed(1)
    let spread_y = 1
    let spread_z = 1

    let config = {
        n_y: 3,
        n_z: 4,
        spread_y: spread_y,
        spread_z: spread_z,
        jitter_location: 0,
        jitter_velocity: 0,
        particle_type_generator: "kfb"
    }

    let pathiclesSystem = new PathiclesSystem.create(config);


    expect(pathiclesSystem).toBeDefined();

    expect(pathiclesSystem.particles.length).toEqual(12);

    expect(pathiclesSystem.particles[0].type).toEqual("proton") 
    expect(pathiclesSystem.particles[1].type).toEqual("proton")
    expect(pathiclesSystem.particles[2].type).toEqual("electron")
    expect(pathiclesSystem.particles[11].type).toEqual("photon")

    expect(pathiclesSystem.particles[0].location)
        .toEqual(
            new THREE.Vector3(
                0, -spread_y / 2, -spread_z / 2)
        );

    expect(pathiclesSystem.particles[1].location)
        .toEqual(
            new THREE.Vector3(
                0,
                0,
                -spread_z / 2)
        );


    expect(pathiclesSystem.particles[11].location)
        .toEqual(
            new THREE.Vector3(
                0, spread_y / 2, spread_z / 2)
        );



    pathiclesSystem.simulate(32, .05);

    expect(pathiclesSystem.particles[0].states.length).toEqual(33);

    // console.log(JSON.stringify(pathiclesSystem, null, "  "));

});