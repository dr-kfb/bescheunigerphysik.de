



ParticleHistory = function (particle, t0, t1, steps, fields) {

    this.particle = particle;

    this.move = function (particle, dt, fields) {



        var newParticle = JSON.parse(JSON.stringify(particle));

        var acceleration = {
            x: 0,
            y: 0,
            z: 0
        };

        if (fields) {

            fields.forEach(field => {

                var newAcceleration = field.getForce(newParticle);
                acceleration.x += newAcceleration.x;
                acceleration.y += newAcceleration.y;
                acceleration.z += newAcceleration.z;
            })

        }


        if (particle.mass != 0) {
            newParticle.velocity.x += acceleration.x * dt / particle.mass;
            newParticle.velocity.y += acceleration.y * dt / particle.mass;
            newParticle.velocity.z += acceleration.z * dt / particle.mass;
        }

        newParticle.location.x += newParticle.velocity.x * dt;
        newParticle.location.y += newParticle.velocity.y * dt;
        newParticle.location.z += newParticle.velocity.z * dt;

        return newParticle;

    }



    this.history = [];

    var dt = (t1 - t0) / steps;

    for (var t = t0; t < t1; t += dt) {
        if (!this.history.length) {
            this.history.push({
                t: t,
                particle: particle
            });
        } else {
            this.history.push({
                t: t,
                particle: this.move(this.history[this.history.length - 1].particle, dt, fields)
            });
        }

    }

    this.asVector3Array = function () {

        return this.history.map(d =>
            new THREE.Vector3(
                d.particle.location.x,
                d.particle.location.y,
                d.particle.location.z))

    }

}
