const ParticleFactory = require('./ParticleFactory');


class ParticlesFactory {


    constructor() {

        this.particleFactory = new ParticleFactory();

        this.particles = [];
        this.dy = 2
        this.y_0 = 3;

        this.dz = 2
        this.z_0 = 10;
    }


    create() {


        for (var z_i = -z_0; z_i <= z_0; z_i++) {


            for (var y_i = -y_0; y_i <= y_0; y_i++) {

                var template;

                if (z_i < 0) {

                    template = ParticleFactory.PROTON
                } else if (z_i > 0) {

                    template = ParticleFactory.ELECTRON
                } else {

                    template = ParticleFactory.PHOTON
                }

                var type = random();

                if (type < 0.5) {

                    template = ParticleFactory.PHOTON
                } else if (type < 1.) {

                    template = ParticleFactory.ELECTRON
                } else {

                    template = ParticleFactory.PROTON
                }


                if (type < .3) {}
                // template = ParticleFactory.ELECTRON



                var v_r = 2
                particles.push(particleFactory.createParticle(
                    template,
                    new THREE.Vector3(0, y_i * dy, z_i * dz),
                    new THREE.Vector3(100, v_r * randomSymmetric(), v_r * randomSymmetric())));

                particles.push(particleFactory.createParticle(
                    template,
                    new THREE.Vector3(0, y_i * dy, z_i * dz),
                    new THREE.Vector3(100, v_r * randomSymmetric(), v_r * randomSymmetric())));

                particles.push(particleFactory.createParticle(
                    template,
                    new THREE.Vector3(0, y_i * dy, z_i * dz),
                    new THREE.Vector3(100, 0, 0)));

            }
        }

        return particles;
    }
}




module.exports = ParticlesFactory;