"use strict";


const PathiclesSystem = require('./PathiclesSystem');
const Pathicles = require('./Pathicles').Pathicles;

const ParticleFactory = require('./ParticleFactory').ParticleFactory;


const ParticleInteractionField = require('./PathiclesField').ParticleInteractionField;
const QuadrupoleField = require('./PathiclesField').QuadrupoleField;
const DipoleField = require('./PathiclesField').DipoleField;

function PathiclesRunner() {

    let parameters = {}

    parameters.scenario = "2 dipoles";

    parameters.camera_auto = false;
    parameters.n_steps = 128;
    parameters.pathicle_height = .02;
    parameters.loop_length = 1;
    parameters.grid = 1;
    parameters.x_scale = 1;
    parameters.scale = 1;
    parameters.updateGraph = ""


    parameters.cameraLookAt = [4, 0, 0]
    parameters.cameraPosition = [5, -.1, 3]

    parameters.particleType = "electrons";

    parameters.particleInteraction = true;

    parameters.dipole_1 = false;
    parameters.dipole_1_x0 = 0.0000001;
    parameters.dipole_1_x1 = 0.2;
    parameters.dipole_1_strength_x = 0;
    parameters.dipole_1_strength_y = -.5;
    parameters.dipole_1_strength_z = 0;

    parameters.dipole_2 = false;
    parameters.dipole_2_x0 = 0.3;
    parameters.dipole_2_x1 = 0.4;
    parameters.dipole_2_strength_x = 0;
    parameters.dipole_2_strength_y = 0;
    parameters.dipole_2_strength_z = .5;


    parameters.quadrupole_1 = true;
    parameters.quadrupole_1_x0 = 0.5;
    parameters.quadrupole_1_x1 = 0.55;
    parameters.quadrupole_1_strength = .02;


    parameters.quadrupole_2 = true;
    parameters.quadrupole_2_x0 = 0.7;
    parameters.quadrupole_2_x1 = 0.75;
    parameters.quadrupole_2_strength = .02;





    parameters.dt = 0.05;

    parameters.update = function () {};

    let pathiclesSystem;
    let trajectories;
    let mathbox;
    let view;
    let camera;
    let v_camera = .2;

    function loop(t) {
        return (t - Math.floor(t / parameters.loop_length) * parameters.loop_length)
    };

    run()

    function run() {
        initDatGui(parameters);

        mathbox = createMathbox();
        update();
    }

    function update() {

        createPathiclesSystem();

        if (mathbox) {
            mathbox.remove("#sampler, camera")
        }

        fillMathbox();

    }


    function createPathiclesSystem() {

        Pathicles.setSeed(1);

        let config = {
            n_y: 10,
            n_z: 10,
            spread_y: 1,
            spread_z: 1,
            jitter_location: 0,
            jitter_velocity: 0,
            particle_type_generator: parameters.particleType
        }



        pathiclesSystem = new PathiclesSystem.create(config);


        if (parameters.dipole_1) {
            let dipoleField_1 = new DipoleField(
                parameters.dipole_1_x0,
                parameters.dipole_1_x1,
                new THREE.Vector3(parameters.dipole_1_strength_x, parameters.dipole_1_strength_y, parameters.dipole_1_strength_z))

            // console.log(dipoleField)
            pathiclesSystem.addField(dipoleField_1)
        }

        if (parameters.dipole_2) {
            let dipoleField_2 = new DipoleField(
                parameters.dipole_2_x0,
                parameters.dipole_2_x1,
                new THREE.Vector3(parameters.dipole_2_strength_x, parameters.dipole_2_strength_y, parameters.dipole_2_strength_z))

            // console.log(dipoleField)
            pathiclesSystem.addField(dipoleField_2)
        }


        if (parameters.quadrupole_2) {
            let quadrupoleField_2 = new QuadrupoleField(
                parameters.quadrupole_2_x0,
                parameters.quadrupole_2_x1,
                parameters.quadrupole_2_strength
            )

            // console.log(quadrupoleField)
            pathiclesSystem.addField(quadrupoleField_2)
        }

        if (parameters.quadrupole_1) {
            let quadrupoleField_1 = new QuadrupoleField(
                parameters.quadrupole_1_x0,
                parameters.quadrupole_1_x1,
                parameters.quadrupole_1_strength, 1
            )

            // console.log(quadrupoleField)
            pathiclesSystem.addField(quadrupoleField_1)


        }


        if (parameters.particleInteraction) {
            let particleInteractionField = new ParticleInteractionField();
            pathiclesSystem.addField(particleInteractionField)
        }


        pathiclesSystem.simulate(parameters.n_steps, parameters.dt);


        // console.log(pathiclesSystem)

        // let xs = pathiclesSystem.particles.map(

        //     particle => particle.states.map(s => s.location.z)
        // )
        // console.dir(xs)

        trajectories = pathiclesSystem.particles.map(

            particle => particle.states.map(s => [
                s.location.x,
                s.location.y,
                s.location.z,
                particle.color.value
            ])
        )

        // console.log("pathiclesSystem.maxLocation", pathiclesSystem.maxLocation)
        // console.log("pathiclesSystem.minLocation", pathiclesSystem.minLocation)
        // console.log(trajectories.map(t => t.map(t2 => t2[2])).join(", "))
    }

    function createMathbox() {



        let mathbox = mathBox({
            plugins: ['core', 'controls', 'cursor', 'stats'],
            controls: {
                klass: THREE.OrbitControls
            }
        });
        let three = mathbox.three;

        three.camera.position.set(parameters.cameraPosition[0], parameters.cameraPosition[1], parameters.cameraPosition[2])
        three.controls.target.set(parameters.cameraLookAt[0], parameters.cameraLookAt[1], parameters.cameraLookAt[2])
        
        three.renderer.setClearColor(new THREE.Color(0xFFFFFF), 1);

        view = mathbox.cartesian({

            range: [
                [0, 1],
                [-1, 1],
                [-1, 1]
            ],
            scale: [parameters.x_scale * parameters.scale, 1 * parameters.scale, 1 * parameters.scale],
        });





        if (parameters.grid) {
            view.grid({
                // width: 5,
                width: 1,
                color: 0x999999,
                opacity: .5,
                axes: [1, 3],
                blending: "normal"
            });
            view.grid({
                // width: 5,
                width: 1,
                color: 0x999999,
                opacity: .5,
                axes: [1, 2],
                blending: "normal"
            });
            view.grid({
                // width: 5,
                width: 1,
                color: 0x999999,
                opacity: .5,
                axes: [2, 3],
                blending: "normal"
            });

        }
        if (parameters.axes) {
            view.axis({
                axis: "x",
                color: 0xff0000,
                width: 1
            });
            view.axis({
                axis: "y",
                color: 0x00ff00,
                width: 1
            });
            view.axis({
                axis: "z",
                color: 0x0000ff,
                width: 1
            });
        }

        view.array({
            data: [
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1]
            ],
            channels: 3, // necessary
            live: false,
        }).text({
            data: ["x", "y", "z"],
        }).label({
            color: 0x000000,

        });

        return mathbox
    }

    function fillMathbox() {

        if (parameters.camera_auto) {


            camera = view.camera({

                lookAt: parameters.cameraLookAt
            }, {
                position: function (t) {
                    return [0 * Math.cos(t) * Math.cos(t) + 5, .1 + 0 * Math.sin(t * .381), .8 * Math.cos(t * v_camera)]
                }
            });


        } else {

            camera = view.camera({

                // lookAt: parameters.cameraLookAt,
                proxy: true,
                // position: parameters.cameraPosition
            });

        }



        let sampler = view.area({
            id: 'sampler',
            width: parameters.n_steps,
            height: 2,
            axes: [1, 2],
            // data: data,
            expr: function (emit, x, y, i, j, time) {
                let color = 0;
                // console.log(x, y, .5 * (Math.sin(i / 2)))
                trajectories.forEach(t => {
                    emit(t[i][0], t[i][1] + y * parameters.pathicle_height, t[i][2], t[i]["color"])
                })

            },
            items: trajectories.length,
            channels: 4,
        })


        // let colors = view.area({
        //     width: parameters.n_steps,
        //     height: 2,
        //     channels: 4,
        //     items: 4,
        //     axes: [1, 3],
        //     expr: function (emit, x, z, i, j, t, delta) {

        //         let t_loop = loop(t)

        //         let a = .8;

        //         if (Math.random() < .1) {

        //             // console.log(x, t_loop / loop_length)
        //         }

        //         // if (1 - x > t_loop && x < t_loop + .1) {

        //         if (i < t_loop * parameters.n_steps && i > t_loop * parameters.n_steps - 2) {
        //             a = .8;

        //         }
        //         pathiclesSystem.particles.forEach(
        //             p => {
        //                 emit(p.color.r / 255, p.color.g / 255, p.color.b / 255, a);
        //             }
        //         );
        //     }
        // });


        let color;
        switch (parameters.particleType) {
            case "electrons":
                color = ParticleFactory.ELECTRON.color.value;
                break;
            case "photons":
                color = ParticleFactory.PHOTON.color.value;
                break;
            case "protons":
                color = ParticleFactory.PROTON.color.value;
                break;
            case "kfb":
                color = ParticleFactory.ELECTRON.color.value;
                break;
            default:
                break;
        }





        view.surface({
            shaded: true,
            lineX: true,
            lineY: true,
            points: sampler,
            opacity: 0.8,
            // colors: colors,
            color: color
            // width: 10,

        })

        // // Nest vertex/fragment transform 
        // var xf = view;
        // xf =
        //     xf
        //     .shader({
        //         code: [

        //             "// Enable STPQ mapping",
        //             "#define POSITION_STPQ",
        //             "void getPosition(inout vec4 xyzw, inout vec4 stpq) {",
        //             "// Store XYZ per vertex in STPQ",
        //             "stpq = xyzw;",
        //             "}"
        //         ].join("\n")

        //     })
        //     .vertex({
        //         // Work in data XYZ instead of view XYZ
        //         pass: 'sampler'
        //     });

        // xf =
        //     xf
        //     .shader({
        //         code: [

        //             "// Enable STPQ mapping",
        //             "#define POSITION_STPQ",
        //             "uniform float time;",
        //             "vec4 getColor(vec4 rgba, in vec4 stpq) {",
        //             "// Retrieve interpolated XYZ from vertices and use as RGB color.",
        //             "// Note: gamma correction is applied with .fragment({ gamma: true }).",
        //             "vec3 rgb = stpq.xyz;",

        //             "// Add spatial grid",
        //             "vec3 d3 = abs(fract(stpq.xyz * 16.0) - .5);",
        //             "float level = 1.0 - 2.0 * min(d3.x, min(d3.y, d3.z));",
        //             "float grid = clamp(level * 32.0 - 24.0, 0.0, 1.0) * .5 + .5;",

        //             "return vec4(1., 1., 1., 1.0 - sin(5.0*stpq.x-time*5.0 + 5.0)*sin(5.0*stpq.x-time*5.0+ 5.0) * 0.99 + 0.*sin(time)*sin(time));",
        //             "}"

        //         ].join("\n")
        //     },{
        //         time:  function (t) { return (t/2) - Math.floor(t/2)*2; },


        //     })
        //     .fragment({
        //         // Convert from (web) sRGB to (GL) linear RGB on output
        //         gamma: true
        //     });

        // // Make surface from data
        // xf.surface({
        //     fill: true,
        //     lineX: false,
        //     lineY: false,
        //     width: 2,
        //     zBias: 1,
        //     color: 0xFFFFFF,
        // });





        return mathbox;
    }

    function initDatGui(parameters) {


        var gui = new dat.GUI();

        // gui.add(parameters, 'functionText').name('y = f(x) = ');

        var folder0 = gui.addFolder('Parameters');
        var aGUI = folder0.add(parameters, 'n_steps', {
            "8": 8,
            "16": 16,
            "32": 32,
            "64": 64,
            "128": 128,
        }).name('n_steps ');
        folder0.add(parameters, 'dt').min(0.01).max(1).step(0.01);
        folder0.add(parameters, 'particleType', {
            "electrons": "electrons",
            "protons": "protons",
            "photons": "photons"
        })
        folder0.add(parameters, 'camera_auto');
        folder0.add(parameters, 'particleInteraction');
        folder0.add(parameters, 'dipole_1');
        folder0.add(parameters, 'dipole_2');
        folder0.add(parameters, 'quadrupole_1');
        folder0.add(parameters, 'quadrupole_2');
        folder0.open();

        var folder1 = gui.addFolder('Dipoles');
        folder1.add(parameters, 'dipole_1_x0').min(0).max(1).step(0.1);
        folder1.add(parameters, 'dipole_1_x1').min(0).max(1).step(0.1);
        folder1.add(parameters, 'dipole_1_strength_x').min(-5).max(5).step(0.1)
        folder1.add(parameters, 'dipole_1_strength_y').min(-5).max(5).step(0.1)
        folder1.add(parameters, 'dipole_1_strength_z').min(-5).max(5).step(0.1)
        folder1.add(parameters, 'dipole_2_x0').min(0).max(1).step(0.1);
        folder1.add(parameters, 'dipole_2_x1').min(0).max(1).step(0.1);
        folder1.add(parameters, 'dipole_2_strength_x').min(-5).max(5).step(0.1)
        folder1.add(parameters, 'dipole_2_strength_y').min(-5).max(5).step(0.1)
        folder1.add(parameters, 'dipole_2_strength_z').min(-5).max(5).step(0.1)

        var folder2 = gui.addFolder('Quadrupoles');
        folder2.add(parameters, 'quadrupole_1_x0').min(0).max(1).step(0.1);
        folder2.add(parameters, 'quadrupole_1_x1').min(0).max(1).step(0.1);
        folder2.add(parameters, 'quadrupole_1_strength').min(0).max(.2).step(0.01)
        folder2.add(parameters, 'quadrupole_2_x0').min(0).max(1).step(0.1);
        folder2.add(parameters, 'quadrupole_2_x1').min(0).max(1).step(0.1);
        folder2.add(parameters, 'quadrupole_2_strength').min(0).max(.2).step(0.01)

        var button = gui.add(parameters, "update");



        // var folder1 = gui.addFolder('Window Range');
        // var xMinGUI = folder1.add(parameters, 'xMin');
        // var xMaxGUI = folder1.add(parameters, 'xMax');
        // var yMinGUI = folder1.add(parameters, 'yMin');
        // var yMaxGUI = folder1.add(parameters, 'yMax');
        // folder1.open();

        // gui.add(parameters, 'updateGraph').name("Update Graph");

        // onChange or onFinishChange
        // aGUI.onChange(update);
        button.onChange(update);
        // bGUI.onChange(updateGraphFunc);
        // xMinGUI.onChange(updateGraphFunc);
        // xMaxGUI.onChange(updateGraphFunc);
        // yMinGUI.onChange(updateGraphFunc);
        // yMaxGUI.onChange(updateGraphFunc);

        gui.open();


    }
}



module.exports.PathiclesRunner = PathiclesRunner;