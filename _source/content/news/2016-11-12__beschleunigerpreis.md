---
title: Beschleunigerpreis nimmt wichtige Etappe.
date: 2016-11-12
lead: Vorstandsrat der Deutschen Physikalischen Gesellschaft (DPG) bewilligt die Einführung eines DPG-Beschleunigerpreises.
category: Nachricht
---
Damit steht der Etablierung eines Preises nichts mehr im Wege, der an hervorragende Nachwuchswissenschaftlerinnen und Nachwuchswissenschaftler auf dem Gebiet der Physik und Technik der Beschleuniger gehen wird.

Wesentlicher Zweck des Preises ist die Förderung der Beschleunigerphysik als eigenständiges Forschungsgebiet in Deutschland. Er wird jährlich vom DPG-Arbeitskreis "Beschleunigerphysik" vergeben werden. An seiner Vorbereitung war das Komitee für Beschleunigerphsik maßgeblich beteiligt, unter anderem bei der Erstellung der Preisordnung sowie bei der Sponsoren-Findung.
