---
title: Stellenausschreibung
date: 2017-01-24
lead: Leiter/in der Beschleunigerabteilung am KIT
category: Stellenausschreibung
---

Am Institut für Beschleunigerphysik und Technologie (IPBT) des KIT ist eine Abteilungsleiterstelle im Beschleunigerbereich zu besetzen. Weitere Informationen gibt es unter [stellen.jobs.kit.edu/](https://stellen.jobs.kit.edu/cgi-bin/appl/list.pl?tmpl=job_details&job_nr=FE07FC84-A9B3-43F2-A7DA-6B1AA23D6D96&cat_nr=3CC995C9-F823-4C51-A0E5-68F938AD9FEB&loc_nr=12B87EE0-C700-11D4-8972-0050BAC69B70/). Bewerbungsfrist: 31. Januar 2017 
