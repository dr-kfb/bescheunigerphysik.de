---
title: Ergebnisse der Wahl 2016
date: 2016-12-20
lead: Die Stimmen zur Wahl des dritten KfB sind ausgezählt. 
category: Nachricht
---

Die Ergebnisse finden Sie unter [KfB / Wahlen](/kfb/wahlen).
