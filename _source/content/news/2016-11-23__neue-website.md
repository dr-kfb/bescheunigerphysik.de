---
title: Unsere neue Website ist online
date: 2016-11-23
lead: Komitee für Beschleunigerphysik (KfB) und Forum Beschleunigerphysik präsentieren sich in neuem Gewand.
category: Nachricht
---

Zusammen mit dem Hamburger Gestalter Dirk Rathje haben wir neue Erscheinungsbilder und eine neue Website entwickelt. Wenn Sie diese Nachricht auf unserer Website lesen, haben Sie einige der Ergebnisse schon gesehen. Anfang 2017 wird eine Onine-Version unserer Broschüre "Beschleuniger. Für Teilchen, Wissen und Gesellschaft" folgen.

Kritik und Anregungen nehmen wir gerne unter [kommunikation@beschleunigerphysik.de](mailto:kommunikation@beschleunigerphysik.de) entgegen.
