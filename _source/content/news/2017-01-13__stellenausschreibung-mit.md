---
title: Stellenausschreibung
date: 2017-01-13
lead: Physiker/in zur Vervollständigung des Betriebspersonals am MIT
category: Stellenausschreibung
---
Das MIT in Markburg sucht zum nächstmöglichen Zeitpunkt eine Physikerin bzw. einen Physiker zur Vervollständigung des Betriebspersonals. Weitere Informationen gibt es unter [mit-marburg.de/stellenangebote/](http://mit-marburg.de/stellenangebote/).

