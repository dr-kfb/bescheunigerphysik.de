---
id: european-xfel
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: European XFEL
abbreviation: European XFEL
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.xfel.eu
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Freie-Elektronen-Laser European
locations: 
    -
        lng: 9.829444
        lat: 53.588611
city: Schenefeld
parent: xfel-gmbh
size: 2100
size_label: Länge (Beschleuniger)
energy: 17500000000
label_align: right
label_dy: 0
label_dx: 0
particles: electrons
---
