---
id: csr
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: CSR
abbreviation: CSR
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
        url: https://www.mpi-hd.mpg.de/blaum/storage-rings/csr/index.de.html
description: Ionenspeicherring
locations: 
    -
        lng: 8.708575
        lat: 49.386923
city: Heidelberg
parent: mpik
size: 35
size_label: Umfang
energy: 300000
label_align: right
label_dy: 0
label_dx: 0
particles: ions
---
