---
id: mpik
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: Max-Planck-Institut für Kernphysik MPIK
abbreviation: MPIK
electoral-group: others
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.mpi-hd.mpg.de/mpi/de/start/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.709167
        lat: 49.387778
city: Heidelberg
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 5
acceleration_physicists__forum: 3
---
