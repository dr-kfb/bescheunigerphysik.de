---
id: uni-heidelberg
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Ruprecht-Karls-Universität Heidelberg
abbreviation: Uni Heidelberg
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.uni-heidelberg.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.70659
        lat: 49.410492
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 3
---
