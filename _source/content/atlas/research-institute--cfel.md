---
id: cfel
visible_on_forum_map: 1
visible_in_atlas: 0
type: research-institute
name: Center for Free-Electron Laser Science
abbreviation: XFEL
electoral-group: others
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 9.885067
        lat: 53.5790478
city: Hamburg
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 2
---
