---
id: ess
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: European Spallatation Source
abbreviation: ESS
electoral-group: abroad
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://esss.se
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.249533
        lat: 55.734199
city: Lund
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 30
acceleration_physicists__forum: 0
---
