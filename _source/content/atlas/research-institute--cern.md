---
id: cern
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: CERN
abbreviation: CERN
electoral-group: abroad
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://cern.ch
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 6.049167
        lat: 46.233333
city: Genf
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 35
---
