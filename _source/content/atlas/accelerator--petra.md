---
id: petra
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: PETRA III
abbreviation: PETRA III
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.desy.de/forschung/anlagen__projekte/petra_iii/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
        url: http://petra3.desy.de
description: Synchrotronstrahlungsquelle
locations: 
    -
        lng: 9.879444
        lat: 53.575833
city: Hamburg
parent: desy
size: 2300
size_label: Umfang (Speicherring)
energy: 6000000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
