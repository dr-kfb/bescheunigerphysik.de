---
id: kit
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Karlsruher Institut für Technologie KIT
abbreviation: KIT
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.kit.edu/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.41167
        lat: 49.00947
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 20
acceleration_physicists__forum: 18
---
