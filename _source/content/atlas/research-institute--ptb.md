---
id: ptb
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: Physikalisch-Technische Bundesanstalt PTB
abbreviation: PTB
electoral-group: others
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.ptb.de/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 10.463611
        lat: 52.295278
city: Braunschweig und Berlin
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 5
---
