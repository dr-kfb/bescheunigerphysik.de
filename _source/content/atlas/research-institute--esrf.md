---
id: esrf
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: European Synchrotron Radiation Facility
abbreviation: ESRF
electoral-group: abroad
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.esrf.eu
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 5.689927
        lat: 45.208477
city: Grenoble
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 2
---
