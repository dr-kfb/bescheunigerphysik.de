---
id: desy
visible_on_forum_map: 0
visible_in_atlas: 0
type: research-institute
name: Deutsches Elektronen-Synchrotron DESY
abbreviation: DESY
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.desy.de/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
city: Hamburg und Zeuthen
label_align: left
label_dy: 0
label_dx: 0
---
