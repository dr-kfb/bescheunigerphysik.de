---
id: hzdr
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: Helmholtz-Zentrum Dresden-Rossendorf HZDR
abbreviation: HZDR
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.hzdr.de/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.949722
        lat: 51.063611
city: Dresden
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 20
acceleration_physicists__forum: 13
---
