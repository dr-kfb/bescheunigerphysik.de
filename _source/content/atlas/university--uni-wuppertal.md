---
id: uni-wuppertal
visible_on_forum_map: 1
visible_in_atlas: 0
type: university
name: Bergische Universität Wuppertal
abbreviation: Uni Wuppertal
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 7.149444
        lat: 51.245278
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 3
---
