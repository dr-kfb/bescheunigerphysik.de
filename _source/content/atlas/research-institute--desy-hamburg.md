---
id: desy-hamburg
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: DESY Hamburg
abbreviation: DESY Hamburg
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 9.880547
        lat: 53.575833
city: Hamburg
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 100
acceleration_physicists__forum: 48
---
