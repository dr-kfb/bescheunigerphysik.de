---
id: tu-dresden
visible_on_forum_map: 1
visible_in_atlas: 0
type: university
name: Technische Universität Dresden
abbreviation: TU Dresden
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.726667
        lat: 51.028056
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 1
---
