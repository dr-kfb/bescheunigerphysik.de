---
id: hu-berlin
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Humboldt-Universität zu Berlin
abbreviation: HU Berlin
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://hu-berlin.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.533304
        lat: 52.432122
city: Berlin
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 1
---
