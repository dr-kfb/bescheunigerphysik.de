---
id: gsi-gmbh
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: GSI Helmholtzzentrum für Schwerionenforschung Darmstadt
abbreviation: GSI
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.gsi.de/start/aktuelles.htm
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.679167
        lat: 49.931389
city: Darmstadt
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 41
---
