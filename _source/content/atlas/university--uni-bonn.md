---
id: uni-bonn
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Rheinische Friedrich-Wilhelms-Universität Bonn
abbreviation: Uni Bonn
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.uni-bonn.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 7.102222
        lat: 50.733889
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 20
acceleration_physicists__forum: 12
---
