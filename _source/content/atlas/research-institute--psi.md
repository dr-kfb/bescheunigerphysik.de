---
id: psi
visible_on_forum_map: 1
visible_in_atlas: 0
type: research-institute
name: Paul Scherrer Institut
abbreviation: PSI
electoral-group: abroad
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.222778
        lat: 47.536111
city: Villigen
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 1
---
