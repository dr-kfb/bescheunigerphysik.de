---
id: desy-zeuthen
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: DESY Zeuthen
abbreviation: DESY Zeuthen
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.6329787
        lat: 52.3456192
city: Zeuthen
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 11
---
