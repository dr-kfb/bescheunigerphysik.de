---
id: uni-siegen
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Universität Siegen
abbreviation: Uni Siegen
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.uni-siegen.de/start/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.028333
        lat: 50.906389
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 0
---
