---
id: cosy
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: COSY
abbreviation: COSY
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.fz-juelich.de/ikp/DE/Forschung/Beschleuniger/_doc/COSY.html;jsessionid=181DA374FD1CBA7DDC65EE63582481AA
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Kühlersynchrotron für Hadronen
locations: 
    -
        lng: 6.411944
        lat: 50.905
city: Jülich
parent: fz-juelich
size: 183
size_label: Umfang
energy: 2880000000
label_align: right
label_dy: 0
label_dx: 0
particles: ions
---
