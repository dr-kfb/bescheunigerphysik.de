---
id: uni-rostock
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Universität Rostock
abbreviation: Uni Rostock
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.uni-rostock.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 12.133373
        lat: 54.088133
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 9
---
