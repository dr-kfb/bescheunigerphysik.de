---
id: mls
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: MLS
abbreviation: MLS
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
        url: https://www.ptb.de/mls/
description: Synchrotronstrahlungsquelle
locations: 
    -
        lng: 13.535691
        lat: 52.426766
city: Berlin
parent: ptb
size: 48
size_label: Umfang (Speicherring)
energy: 630000000
label_align: right
label_dy: 0
label_dx: 0
particles: electrons
---
