---
id: uni-mainz
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Johannes Gutenberg-Universität Mainz
abbreviation: Uni Mainz
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.kph.uni-mainz.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.241667
        lat: 49.993056
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 1
---
