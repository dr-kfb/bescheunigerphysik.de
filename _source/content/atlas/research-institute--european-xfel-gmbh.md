---
id: european-xfel-gmbh
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: European XFEL
abbreviation: European XFEL
electoral-group: others
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.xfel.eu
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 9.829444
        lat: 53.588611
city: Schenefeld
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 0
acceleration_physicists__forum: 0
---
