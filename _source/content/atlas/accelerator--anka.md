---
id: anka
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: ANKA
abbreviation: ANKA
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
        url: https://www.anka.kit.edu
description: Synchrotronstrahlungsquelle
locations: 
    -
        lng: 8.428319
        lat: 49.09673
city: Karlsruhe
parent: kit
size: 110
size_label: Umfang (Speicherring)
energy: 2500000000
label_align: right
label_dy: 0
label_dx: 0
particles: electrons
---
