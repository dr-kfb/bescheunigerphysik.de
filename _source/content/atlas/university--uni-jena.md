---
id: uni-jena
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Friedrich-Schiller-Universität Jena
abbreviation: Uni Jena
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.uni-jena.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 11.589444
        lat: 50.929444
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 1
---
