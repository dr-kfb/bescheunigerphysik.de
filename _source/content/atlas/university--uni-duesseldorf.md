---
id: uni-duesseldorf
visible_on_forum_map: 1
visible_in_atlas: 0
type: university
name: Heinrich-Heine-Universität Düsseldorf
abbreviation: Uni Düsseldorf
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 6.794167
        lat: 51.190278
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 2
---
