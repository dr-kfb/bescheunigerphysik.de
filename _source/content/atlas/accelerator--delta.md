---
id: delta
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: DELTA
abbreviation: DELTA
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.delta.tu-dortmund.de/cms/de/Beschleuniger/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Synchrotron
locations: 
    -
        lng: 7.408144
        lat: 51.492257
city: TU Dortmund
parent: tu-dortmund
size: 115
size_label: Umfang (Speicherring)
energy: 1500000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
