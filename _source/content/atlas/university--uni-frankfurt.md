---
id: uni-frankfurt
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Goethe-Universität Frankfurt am Main
abbreviation: Uni Frankfurt
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.uni-frankfurt.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.651389
        lat: 50.119444
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 20
acceleration_physicists__forum: 14
---
