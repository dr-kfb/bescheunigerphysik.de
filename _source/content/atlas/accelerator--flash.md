---
id: flash
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: FLASH
abbreviation: FLASH
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.desy.de/forschung/anlagen__projekte/flash/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Freie-Elektronen-Laser
locations: 
    -
        lng: 9.881408
        lat: 53.579904
city: Hamburg
parent: desy
size: 150
size_label: Länge (Beschleuniger)
energy: 1250000000
label_align: left
label_dy: -1
label_dx: 0
particles: electrons
---
