---
id: elbe
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: ELBE
abbreviation: ELBE
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.hzdr.de/db/Cms?pNid=145
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Linearbeschleuniger mit Freie-Elektronen-Laser HZDR
locations: 
    -
        lng: 13.949722
        lat: 51.063611
city: Dresden
parent: hzdr
size: 20
size_label: Länge
energy: 40000000
label_align: right
label_dy: 0
label_dx: 0
particles: electrons
---
