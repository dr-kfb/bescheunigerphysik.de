---
id: lmu
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Ludwig-Maximilians-Universität München
abbreviation: LMU
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.uni-muenchen.de/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 11.580278
        lat: 48.150833
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 5
---
