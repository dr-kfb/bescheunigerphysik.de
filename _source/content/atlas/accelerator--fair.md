---
id: fair
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: FAIR
abbreviation: FAIR
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.fair-center.eu/index.php?id=120&L=1
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Antiprotonen- und Ionenbeschleuniger
locations: 
    -
        lng: 8.679167
        lat: 49.931389
city: Darmstadt
parent: fair-gmbh
size: 1100
size_label: Umfang (SIS100)
energy: 30000000000
label_align: left
label_dy: -1
label_dx: 0
particles: ions
---
