---
id: lhc
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: Large Hadron Collider
abbreviation: LHC
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://home.cern/topics/large-hadron-collider
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Synchrotron und Speicherring für Protonen und Ionen
locations: 
    -
        lng: 6.049167
        lat: 46.233333
city: Genf
parent: cern
size: 27000
size_label: Umfang
energy: 4000000000000
label_align: left
label_dy: 0
label_dx: 0
particles: ions
---
