---
id: unilac
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: UNILAC
abbreviation: UNILAC
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Linearbeschleuniger
locations: 
    -
        lng: 8.679167
        lat: 49.931389
city: Darmstadt
parent: gsi-gmbh
size: 120
size_label: Länge
label_align: left
label_dy: 0
label_dx: 0
particles: ions
---
