---
id: s-dalinac
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: S-DALINAC
abbreviation: S-DALINAC
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.ikp.tu-darmstadt.de/sdalinac_ikp/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Elektronenbeschleuniger
locations: 
    -
        lng: 8.654923
        lat: 49.879409
city: TU Darmstadt
parent: tu-darmstadt
size: 25
size_label: Grundfläche
energy: 130000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
