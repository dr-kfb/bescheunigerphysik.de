---
id: rwth-aachen
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Rheinisch-Westfälische Technische Hochschule Aachen
abbreviation: RWTH Aachen
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.rwth-aachen.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 6.059911
        lat: 50.778856
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 10
acceleration_physicists__forum: 0
---
