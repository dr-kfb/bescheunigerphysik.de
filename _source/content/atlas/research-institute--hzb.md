---
id: hzb
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: Helmholtz-Zentrum Berlin für Materialien und Energie HZB
abbreviation: HZB
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.helmholtz-berlin.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.129444
        lat: 52.41
city: Berlin Wannsee und Berlin Adlershof
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 44
---
