---
id: esrf-accelerator
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: ESRF synchrotron
abbreviation: ESRF
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.esrf.eu/Accelerators/Accelerators
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Synchrotronstrahlungsquelle
locations: 
    -
        lng: 5.689927
        lat: 45.208477
city: Grenoble
parent: esrf
size: 844
size_label: Umfang (Speicherring)
energy: 600000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
