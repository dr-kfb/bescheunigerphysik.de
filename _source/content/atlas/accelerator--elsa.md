---
id: elsa
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: ELSA
abbreviation: ELSA
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www-elsa.physik.uni-bonn.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Elektronenspeicherring
locations: 
    -
        lng: 7.088386
        lat: 50.727314
city: Bonn
parent: uni-bonn
size: 164
size_label: Umfang (Stretcherring)
energy: 3500000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
