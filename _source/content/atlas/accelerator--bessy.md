---
id: bessy
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: BESSY II
abbreviation: BESSY II
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.helmholtz-berlin.de/quellen/bessy/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Synchrotronstrahlungsquelle
locations: 
    -
        lng: 13.532624
        lat: 52.427635
city: Berlin
parent: hzb
size: 240
size_label: Umfang (Speicherring)
energy: 1700000000
label_align: left
label_dy: 0
label_dx: 0
particles: electrons
---
