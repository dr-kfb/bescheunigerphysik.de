---
id: ess-accelerator
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: ESS
abbreviation: ESS
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://europeanspallationsource.se/accelerator
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Linearbeschleuniger für Protonen
locations: 
    -
        lng: 13.249533
        lat: 55.734199
city: Lund
parent: ess
size: 600
size_label: Länge
energy: 200000000
label_align: left
label_dy: 0
label_dx: 0
particles: ions
---
