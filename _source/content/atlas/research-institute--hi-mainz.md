---
id: hi-mainz
visible_on_forum_map: 1
visible_in_atlas: 0
type: research-institute
name: Helmholtz-Institut Mainz
abbreviation: Helmholtz-Institut Mainz
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.2350083
        lat: 49.9915834
city: Mainz
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 2
---
