---
id: uni-hamburg
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Universität Hamburg
abbreviation: Uni Hamburg
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: https://www.uni-hamburg.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 9.983889
        lat: 53.566944
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 32
---
