---
id: tu-darmstadt
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Technische Universität Darmstadt
abbreviation: TU Darmstadt
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.tu-darmstadt.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 8.656944
        lat: 49.875
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 50
acceleration_physicists__forum: 48
---
