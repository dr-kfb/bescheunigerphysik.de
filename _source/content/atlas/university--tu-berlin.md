---
id: tu-berlin
visible_on_forum_map: 1
visible_in_atlas: 0
type: university
name: Technische Universität Berlin
abbreviation: TU Berlin
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 13.3202612
        lat: 52.5131317
city: Berlin
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 2
---
