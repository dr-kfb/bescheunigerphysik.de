---
id: fz-juelich
visible_on_forum_map: 1
visible_in_atlas: 1
type: research-institute
name: Forschungszentrum Jülich
abbreviation: FZ Jülich
electoral-group: helmholtz
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.fz-juelich.de/
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 6.411944
        lat: 50.905
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 30
acceleration_physicists__forum: 14
---
