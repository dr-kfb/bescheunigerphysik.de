---
id: uni-goettingen
visible_on_forum_map: 1
visible_in_atlas: 0
type: university
name: Georg-August-Universität Göttingen
abbreviation: Uni Göttingen
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 9.934475
        lat: 51.541861
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists__forum: 1
---
