---
id: sis18
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: SIS 18
abbreviation: SIS 18
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
    -
        description: Webangebot (Zielgruppe Wissenschaft)
description: Schwerionensynchrotron
locations: 
    -
        lng: 8.679167
        lat: 49.931389
city: Darmstadt
parent: gsi-gmbh
size: 216
size_label: Umfang
energy: 4000000000
label_align: left
label_dy: 0
label_dx: 0
particles: ions
---
