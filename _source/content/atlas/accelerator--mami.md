---
id: mami
visible_on_forum_map: 0
visible_in_atlas: 1
type: accelerator
name: MAMI
abbreviation: MAMI
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.kph.uni-mainz.de/mami.php
    -
        description: Webangebot (Zielgruppe Wissenschaft)
        url: http://portal.kph.uni-mainz.de/B1//
description: Elektronenbeschleuniger
locations: 
    -
        lng: 8.236307
        lat: 49.990501
city: Mainz
parent: uni-mainz
size: 30
size_label: Grundfläche
energy: 1600000000
label_align: right
label_dy: 0
label_dx: 0
particles: electrons
---
