---
id: tu-dortmund
visible_on_forum_map: 1
visible_in_atlas: 1
type: university
name: Technische Universität Dortmund
abbreviation: TU Dortmund
electoral-group: university
urls: 
    -
        description: Webangebot (Zielgruppe Allgemeinheit)
        url: http://www.tu-dortmund.de
    -
        description: Webangebot (Zielgruppe Wissenschaft)
locations: 
    -
        lng: 7.414267
        lat: 51.492461
label_align: left
label_dy: 0
label_dx: 0
acceleration_physicists: 30
acceleration_physicists__forum: 21
---
