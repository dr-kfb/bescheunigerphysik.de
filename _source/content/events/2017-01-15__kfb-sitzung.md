---
title: <span></span>12. KfB-Sitzung
date: 2017-02-16
location: Darmstadt
lead: Sitzung des Komitees für Beschleunigerphysik an der TU @@ Darmstadt
category: Kfb/Forum-Veranstaltung
---

Beginn: 16. @@ Februar @@ 2017, 11:30 Uhr<br>
Ort: TU Darmstadt (Raum wird noch bekanntgeben)
