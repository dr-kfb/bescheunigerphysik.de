---
title: "ps-fs Electron and Photon Beams"
date: 2017-07-19
date_end: 2017-07-21
location: Zeuthen
lead: Die Beschleunigung extrem kurzer Elektronen- und Photonenpulse steht im Fokus des fünften thematischen Workshops der Helmholtz-Initiative für Beschleunigerforschung und -entwicklung (ARD).
---

Weitere Informationen: [Workshop-Website](https://indico.desy.de/conferenceDisplay.py?confId=17242)
