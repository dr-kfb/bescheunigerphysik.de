---
title: Perspektiven-Workshop
date: 2017-02-16
date_end: 2017-02-17
location: Darmstadt
lead: Im Workshop "Perspectives for Accelerator Physics und Technology" sollen langfristige Perspektiven für die Beschleunigerphysik und Technologie diskutiert werden.
category: Kfb/Forum-Veranstaltung
---

 Der Workshop ist komplementär zu den regelmäßig organisierten KfB-Verbundforschungs-Workshops, die dazu dienen, gemeinsame Projekte zu planen und Anträge für die regelmäßigen BMBF Ausschreibungen zur Verbundforschung in der Teilchenphysik / Kernphysik und der Physik der kondensierten Materie vorzubereiten.

Weitere Informationen und Anmeldung: https://indico.cern.ch/event/581462/
