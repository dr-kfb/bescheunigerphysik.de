---
title: Vollversammlung
date: 2016-02-16T16:00
date_end: 2017-02-16
location: Darmstadt
lead: Vollversammlung des Forums Beschleunigerphysik an der TU @@ Darmstadt
category: Kfb/Forum-Veranstaltung
---

Beginn: 16. @@ Februar @@ 2017, 16:00 Uhr<br>
Ort: TU Darmstadt (Raum wird noch bekanntgeben)
