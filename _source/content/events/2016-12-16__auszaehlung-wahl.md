---
title: Auszählung KfB-Wahl 2016
date: 2016-12-16
lead: Die Auszählung der Wahlstimmen für die KfB-Wahl 2016 erfolgt voraussichtlich am 16. @@ Dezember @@ 2016.
---

Eingangsfrist für die Wahlbriefe ist 12:00 Uhr. Die Ergebnisse werden anschließend auf dieser Website unter [KfB/Wahlen](/kfb/wahlen) veröffentlicht.
