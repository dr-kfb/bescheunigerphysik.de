![](/_shared/images/portraits/2016_prasuhn.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">Dr.</span> Dieter **Prasuhn**

Leiter der Abteilung Beschleunigerphysik,<br>Teilprojektleiter HESR@FAIR,<br>Betriebsleitung COSY
: Institut für Kernphysik, Forschungszentrum Jülich



### Forschungsinteressen

* Strahl- und Spinphysik
* interne Targets in Speicherringen
* Strahlkühlung


### Berufliche Stationen

1983
: Diplom, Universität Bonn

1986
: Promotion, Universität Bonn, Tätigkeit im FZ Jülich

1986 bis 1988
: Postdoc, Universität Bonn

1988 bis 1989
: Postdoc, The Svedberg Laboratory Uppsala, Schweden


seit 1989
: Angestellter im Institut für Kernphysik des FZJ




### Motivation und Zielvorstellungen

Beschleuniger sind ein weit verbreitetes Werkzeug in der Wissenschaft und in vielen angewandten Forschungs- und Lebensbereichen (z. @ B. medizinische Anwendungen). Daher sollte die Beschleunigerphysik als eigenes Forschungsgebiet innerhalb der Physik verfestigt werden. Nur eine sehr enge Zusammenarbeit aller Beschleunigerlabore kann dazu beitragen, die Qualität der Beschleuniger für die unterschiedlichsten Anforderungen zu optimieren. Dazu möchte ich nach Kräften beitragen.
