![](/_shared/images/portraits/2016_kamps.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title"> Dr.</span> Thorsten **Kamps**

Gruppenleiter "Electron Beams"
: Helmholtz-Zentrum Berlin


### Tätigkeiten

* Forschung und Lehre in Beschleunigerphysik
* Entwicklung von neuartigen Elektronenkanonen und Strahldiagnose

### Forschungsinteressen

* Erzeugung, Beschleunigung und Diagnose für hochbrillante Elektronenstrahlen
* Strahldynamik von ultra-kurzen Elektronenpulsen
* Anwendungen von Teilchenbeschleuniger für die Grundlagenforschung (Synchrotronstrahlungsquellen, FEL, UED, HEP)


### Berufliche Stationen

* Studium der Physik an der Universität Dortmund (heute TU Dortmund)
* Doktorabeit bei DESY Zeuthen und der Humboldt-Universität zu Berlin
* PostDoc an der Royal Holloway University in London (Vereinigtes Königreich)
* Staff Scientist am Helmholtz-Zentrum Berlin (vormals Bessy)



### Motivation und Zielvorstellungen

* Darstellung der Beschleunigerphysik als eigenständiges, für die Gesellschaft relevantes Forschungsgebiet
* Unterstützung des Dialogs untereinander, mit anderen Fachbereichen und der Zivilgesellschaft
* Förderung des wissenschaftlichen Nachwuchses von der Kita bis zum/zur PostDoc; neue Lehrkonzepte zur Förderung von innovativem Denken
