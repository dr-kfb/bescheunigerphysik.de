![](/_shared/images/portraits/2016_tecker.jpg)<!-- {.portrait} -->

Wahlkreis **Ausländische Institute**


## <span class="title">Dr.</span> Frank **Tecker**

Senior Staff
: Operationsgruppe, Beams Department, CERN



### Tätigkeiten

* Forschung und Entwicklung an der CLIC Test Facility CTF3
* Zukünftig: Operation des Protonensynchrotrons PS
* Vorlesungen über Linearbeschleuniger und Beschleunigerphysik



### Forschungsinteressen

* Beschleunigerphysik
* Linearbeschleuniger
* Compact Linear Collider CLIC



### Berufliche Stationen


1988 bis 1994
: Physikstudium an der RWTH Aachen, 1-jähriger CERN Aufenthalt zur Diplomarbeit in der Strahlinstrumentierungsgruppe

1994 bis 1998
: Doktorarbeit an der RWTH, 3-jähriger CERN Aufenthalt und Arbeit beim Elektronenspeicherring LEP

1998 bis 2000
: Research Associate am Fermilab, Chicago, IL, USA<br>
Arbeit in der Beschleunigergruppe beim Main Injector und Recycler

seit 2000
: CERN Mitarbeiter in der Operationsgruppe des Beams Departments




### Motivation und Zielvorstellungen

Seit meinem Studium arbeite ich in der Beschleunigerphysik. Beschleuniger sind ein sehr interessantes vielseitiges Forschungsgebiet mit vielen nützlichen Anwendungen. Dies verdient es, gut in der Öffentlichkeit bekannt gemacht zu werden.

Bei meiner Tätigkeit bei CLIC sehe ich deutlich den Nutzen von Kollaborationen. Ich halte Informationsaustausch und die Förderung von Verbünden sehr wichtig, um die Forschung optimal zu gestalten. Bei der Teilnahme an einem KfB Verbundforschungsworkshop hatte ich bereits Gelegenheit, einen Einblick in die Förderung der Verbundbildung zu bekommen. Ich sehe ein großes Potential für zukünftige Zusammenarbeit von CERN und deutschen Universitäten.

Ich würde mich freuen, durch meine Tätigkeit zur Vernetzung der Beschleunigerphysik und zu ihrer generellen Sichtbarkeit beizutragen.
