![](/_shared/images/portraits/2016_osterhoff.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">Dr.</span> Jens **Osterhoff**

Leiter der Forschungsgruppe für plasmabasierte Teilchenbeschleuniger<br>
: DESY



### Tätigkeiten

* Projektkoordinator bei FLASHForward
* Stellvertrender Sprecher, Accelerator Research and Development (ARD), Helmholtz-Gemeinschaft Deutscher Forschungszentren
* Deputy Coordinator, European Network for Novel Accelerators (EuroNNAc, Teil von EuCARD-2)



### Forschungsinteressen


* Kopplung von radiofrequenz- und plasmabasierten Teilchenbeschleunigern
* strahl- und lasergetriebene Plasma-Wakefield-Beschleunigung
* Phasenraumcharakterisierung ultrakurzer Elektronenpulse
* plasmabasierte Röntgen- und XUV-Strahlungsquellen
* Hochtemperaturhydrodynamik und Schockwellen
* Clusterphysik
* Hochintensitätslaser



### Berufliche Stationen


seit 2013
: Deutsches Elektronen-Synchrotron DESY, Hamburg

2010  bis  2012
: Universität Hamburg

2009  bis  2010
: Lawrence Berkeley National Laboratory

2005  bis  2010
: Max-Planck-Institut für Quantenoptik, Garching,
und Ludwig-Maximilians-UniversitÄT München

2003  bis  2004
: The University of Texas at Austin, United States



### Motivation und Zielvorstellungen

Mein Ziel ist es, die traditionelle und etablierte Beschleunigerforschung mit neuen Ideen und Konzepten aus der Laser- und Plasmaphysik zu bereichern. Diese Themen sind zudem hervorragend dazu geeignet, die besten Köpfe unter Studenten und Entscheidungsträgern für Beschleuniger zu begeistern und deren hohe Bedeutung für die Gesellschaft zu vermitteln – eine Grundvoraussetzung, um das internationale Spitzenniveau der Beschleunigerforschung in Deutschland zu behaupten und auszubauen.
