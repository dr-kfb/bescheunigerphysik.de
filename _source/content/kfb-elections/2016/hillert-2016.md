![](/_shared/images/portraits/2016_hillert.jpg)<!-- {.portrait} -->

Wahlkreis **Universitäten**

## <span class="title"> Prof. Dr.</span> Wolfgang **Hillert**

Universitätsprofessor (ab 01/2017)
: Institut für Experimentalphysik der Universität Hamburg (seit @@ 11/2016)

### Tätigkeiten

* Forschung und Lehre in Beschleunigerphysik

### Forschungsinteressen

* Erzeugung ultrakurzer Elektronen- und Photonenpulse
* Röntgenlaser, Seeding von FELs, spiegelbasierte Systeme,  Photoinjektoren
* Kryogene Resonatoren
* Strahldynamik und -diagnose
* Polarisierte Elektronenstrahlen, Polarimetrie


### Berufliche Stationen

1992
: Promotion in Atmosphärenphysik, Universität Bonn

2001
: Habilitation in Physik, Universität Bonn

2002 @ - @ 2016
: Privatdozent am Physikalischen Institut

2003 @ - @ 2016
: Technischer Direktor ELSA

ab 2017
: Björn Wiik-Professur an der Universität Hamburg




### Motivation und Zielvorstellungen


Das im Jahr 2011 gegründete KfB hat bereits beachtliche Erfolge vorzuweisen. So gibt es mittlerweile neben einem Flyer auch eine umfassende Broschüre zur Beschleunigerphysik, ein neuer Arbeitskreis Beschleunigerphysik wurde bei der DPG gegründet, zwei Beschleunigerpreise in Verbindung mit der DPG werden demnächst eingerichtet, und die Beschleunigerphysik genießt mittlerweile ein sehr hohes Ansehen im Bundesforschungsministerium (BMBF). Auf diesen Erfolgen dürfen wir uns jedoch in Zukunft nicht ausruhen! Besonderes Augenmerk erfordert insbesondere die Neugestaltung der Verbundforschungsförderung im BMBF. Weiterhin gibt es die altbekannten Dauerbrenner:

* Stärkung der Beschleunigerphysik als eigenständiges Forschungsgebiet
* Intensivierung der Zusammenarbeit von Universitäten und Helmholtz-Zentren
* Förderung der Ausbildung wissenschaftlichen Nachwuchses
* Unterstützung der Kommunikation untereinander und mit anderen Fachbereichen
* Vertretung unserer Gemeinschaft gegenüber Politik, Förderinstitutionen und Öffentlichkeit

Auf allen diesen Gebieten möchte ich mich auch im zukünftigen KfB weiterhin engagieren und dazu beitragen, dass die bisherige Arbeit erfolgreich fortgesetzt wird.
