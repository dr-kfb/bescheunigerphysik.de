![](/_shared/images/portraits/2016_boine-frankenheim.jpg)<!-- {.portrait} -->

Wahlkreis **Universitäten**

## <span class="title"> Prof. Dr.</span> Oliver **Boine-Frankenheim**

Fachgebietsleiter Beschleunigerphysik
: Technische Universität Darmstadt

Abteilungsleiter Beschleunigerphysik
: GSI Helmholtzzentrum für Schwerionenforschung

### Tätigkeiten / Forschungsinteressen

* Physik intensiver Teilchenstrahlen
* Laserbeschleunigung von Elektronen und Ionen
* Ausbildung und Vorlesungen auf dem Gebiet der Beschleuniger- und Plasmaphysik
* Gremientätigkeiten: KfB, EPS accelerator group, IPAC15/17 SPC



### Berufliche Stationen

1996
: Promotion, TU Darmstadt

1996-1997
: PostDoc, Sandia Nat. Labs., Albuquerque, USA

seit 1998
: GSI Darmstadt, Bereich Beschleuniger

seit 2008
: Abteilungsleiter bei GSI

seit 2011
: Professor an der TU Darmstadt, Fachbereich Elektrotechnik und IT

2014-16
: Studiendekan Elektrotechnik und IT


### Motivation und Zielvorstellungen

Das KfB hat in den letzten Jahren einiges erreicht: Über 400 Beschleunigerphysiker und Physikerinnen haben sich für das Forum registriert, das KfB trägt als Beratungsgremium des BMBF und Organisator der vorbereitenden Workshops wesentlich zur Verbundforschung bei, die Sichtbarkeit und Wahrnehmung der Beschleunigerphysik als eigenständiges Forschungsgebiet in Deutschland wurde durch die starke und koordinierte Präsenz auf den Frühjahrstagungen und die Gründung eines Arbeitskreises (AKBP) innerhalb der DPG erheblich verstärkt, zusammen mit dem AKBP wurde ein Nachwuchspreis für Beschleunigerphysik auf den Weg gebracht. Schließlich wurde die KfB-Broschüre veröffentlicht, welche auch die breitere Öffentlichkeit ansprechen soll.

Neben der Weiterführung und kontinuierlichen Weiterentwicklung des bereits Geleisteten gibt es natürlich immer noch einiges zu tun. Daran möchte ich weiter aktiv mitwirken. Verstärkt werden sollte z. @ B. die Einbindung von Doktoranden und Nachwuchswissenschaftlern. Eine Möglichkeit dazu wäre ein Doktoranden- und/oder Postdoc-Vertreter. Die Einbindung und Rückmeldung an die Forumsmitglieder sollte verstärkt werden. Auch sollte das KfB neben den etablierten Workshops zur Verbundforschung eine aktivere Rolle bei der Diskussion der Zukunftsperspektiven der Beschleunigerphysik- und Technik an den Großforschungseinrichtungen und den Universitäten spielen. Dies kann im Rahmen einer jährlichen Veranstaltung für alle Forumsmitglieder erfolgen, durch spezielle Themen- Workshops und/oder Hinweise auf aktuelle Entwicklungen über andere Informationsmedien.
