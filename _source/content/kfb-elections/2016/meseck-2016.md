![](/_shared/images/portraits/2016_meseck.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">PD Dr.</span> Atoosa **Meseck**

Leiterin der Arbeitsgruppe "High Brightness Photon Beams"
: Helmholtz-Zentrum Berlin


### Tätigkeiten

Forschung am Institut für Beschleunigerphysik des HZB und Lehre (als Privatdozentin) an der Humboldt-Universität zu Berlin.



### Forschungsinteressen

* Beschleunigerphysik im Allgemeinen
* beschleunigerbasierte Lichtquellen
* nichtlineare Strahldynamik
* kollektive Phänomene 
* neue Konzepte in Beschleunigerphysik



### Berufliche Stationen


2000
: Promotion, Universität Hamburg


2000-2002
: wissenschaftliche Mitarbeiterin am Ionenstrahllabor, HMI

seit 2003
: wissenschaftliche Mitarbeiterin am BESSY/HZB

2012
: Habilitation in Experimentalphysik, Humboldt Universität zu Berlin

seit 2012
: Leiterin der Arbeitsgruppe "High Brightness Photon Beams" und Privatdozentin an der Humboldt-Universität zu Berlin



### Motivation und Zielvorstellungen


Als ein Zusammenschluss von Beschleunigerphysikern aus Universitäten, ausländischen Instituten und Helmholtz-Zentren gibt uns das KfB die Möglichkeit die Sachverhalte, Anliegen und Probleme aus verschiedenen Perspektiven zu betrachten und zu bewerten. Damit setzt es uns in die Lage wissenschaftlich angemessenere und gesellschaftlich sinnvollere Lösungen zu finden. Aus dieser Tatsache leitet sich auch der Anspruch des KfBs her, der Politik und Geldgebern gegenüber als Vertreter der Beschleunigerphysiker in Deutschland zu gelten. Diesem Anspruch können wir nur mit einem sehr hohen Maß an Informationsaustausch und Abstimmung genüge tun.

Ich möchte dazu beitragen, diesen Informationsaustausch und die Abstimmung weiterhin zu gewährleisten und zu verbessern, um

* die Beschleunigerphysik als eigenständiges Forschungsgebiet weiter zu etablieren,
* sowohl mehr Nachwuchs an Wissenschaftlern, Ingenieuren und Technikern zu rekrutieren, als auch diesen eine bessere und umfassendere Ausbildung und mehr Zukunftsperspektiven zu bieten,
* mehr Mittel für Forschung und Lehre in Beschleunigerphysik einzuwerben.
