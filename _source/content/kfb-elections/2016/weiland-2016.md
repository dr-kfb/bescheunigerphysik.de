![](/_shared/images/portraits/2016_weiland.jpg)<!-- {.portrait} -->


Wahlkreis **Universitäten**

## <span class="title">Prof. Dr.</span> Thomas **Weiland**

Professor am Institut für Theorie Elektromagnetischer Felder
: Technische Universität Darmstadt






### Forschungsinteressen

* Beschleunigerphysik
* Numerische Verfahren zur Berechnung von Feldern



### Berufliche Stationen

1977
: Promotion, Technische Universität Darmstadt

1979
: Fellow am CERN

1981
: Wissenschaftlicher Mitarbeiter am DESY

1984
: Habilitation in Experimentalphysik, Universität Hamburg

1989
: Leiter des Instituts für Theorie Elektromagnetischer Felder an der TU Darmstadt

2011
: Vorsitzender des Komitees für Beschleunigerphysik (KfB)

Infos
: [www.TEMF.de](http://www.TEMF.de)


### Motivation und Zielvorstellungen

Die Beschleunigerphysik in Deutschland ist durch die Großforschungseinrichtungen in weltweit führender Weise etabliert. Ein für europäische Verhältnisse ungewöhnlich hohes Engagement findet man an den Universitäten in Berlin, Bonn, Darmstadt, Dortmund, Dresden, Hamburg, Karlsruhe, München, Rostock und Wuppertal. Diese umfassende Forschung an gemeinschaftlichen Themenstellungen der Beschleunigerphysik musste bei den Förderinstitutionen eine eigene Stimme bekommen. Im 2011 gegründeten Komitee für Beschleunigerphysik setzen wir uns gemeinsam erfolgreich dafür ein, in Zukunft eine Erweiterung der koordinierten Forschung an Großgeräten zu erreichen und eine deutlich erhöhte Förderung der generischen Forschung auf unserem Gebiet. Das Komitee ist mittlerweile sehr gut etabliert und wird bei den Planungen der entsprechenden Fördermaßnahmen gehört. Zudem veranstalten wir Tagungen, Informationsveranstaltungen, um alle Beschleunigerphysiker miteinander zu vernetzen.

Als Vertreter der Universitäten ist es mein Anliegen neben der Förderung der Forschung die koordinierte Aus- und Weiterbildung im Fach Beschleunigerphysik in Deutschland voranzubringen.
