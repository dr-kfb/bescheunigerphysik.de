![](/_shared/images/portraits/2016_kaluza.jpg)<!-- {.portrait} -->

Wahlkreis **Universitäten**

## <span class="title"> Prof. Dr.</span> Malte C. **Kaluza**

Direktor des Instituts für Optik und Quantenelektronik
: Friedrich-Schiller-Universität Jena

Lehrstuhl für Experimentalphysik / Relativistische Laserphysik
: Friedrich-Schiller-Universität Jena

Mitglied des erweiterten Direktoriums
: des Helmholtz-Instituts Jena



### Tätigkeiten

* Mitglied im Fakultätsrat
* Senatsmitglied der FSU Jena
* Gutachter und Mitglied im Editorial Board verschiedener int. Zeitschriften

### Forschungsinteressen


* Entwicklung von Hochleistungslasern (u.a. Dioden-gepumpt für höchste Spitzenleistungen, burst-mode-Konzepte für hohe Rep-Raten, Entwicklung und Charakterisierung neuartiger Lasermaterialien)
* Teilchenbeschleunigung mit Hochleistungslasern in Plasmen
* zeitlich und räumlich hochauflösende Diagnostiken für Laser-Plasma-Beschleuniger
* Realisierung und Anwendung Laser-basierter Sekundärstrahlungsquellen


### Berufliche Stationen


1994 bis 2000
: Musikstudium an der Hochschule für Musik in München

1994 bis 2000
: Physikstudium an der TU München

2000
: Diplom

2000 bis 2004
: Promotion, TU München und MPI für Quantenoptik

2004 bis 2005
: PostDoc, Research Associate am Imperial College London

2006
: Nachwuchsgruppenleiter, Friedrich-Schiller-Universität Jena

2006 bis 2011
: Junior Professor, Friedrich-Schiller-Universität Jena

seit 2011
: Lehrstuhlinhaber, Friedrich-Schiller-Universität Jena

seit 2009
: Gründungsmitglied des Helmholtz-Instituts Jena





### Motivation und Zielvorstellungen

* noch stärkere Verzahnung von Forschung an Universitäten und Großforschungseinrichtungen (v. @ a. HGF und MPG)
* stärkere Zusammenführung von konventionellen und alternativen Beschleunigertechnologien und -diagnostiken
* weiterer Ausbau der Sichtbarkeit der Beschleunigerphysik in der DPG
* Begeisterung von Studenten und wissenschaftlichem Nachwuchs für Beschleunigerphysik (Spezialvorlesungen, allgemein verständliche Veranstaltungsreihen)
