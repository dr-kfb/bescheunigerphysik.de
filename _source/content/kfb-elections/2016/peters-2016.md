![](/_shared/images/portraits/2016_peters.jpg)<!-- {.portrait} -->

Wahlkreis **Sonstige deutsche Forschungsinstitute**

## <span class="title">Dipl.-Phys.</span> Andreas **Peters**

Leitung des HIT-Beschleuniger-Teams
: Heidelberg Ionenstrahl-Therapie Centrum (HIT)




### Forschungsinteressen

Optimierung und Funktionserweiterung von Beschleunigern für die Partikeltherapie mit allen Teilaspekten (Kontrollsystem, Strahldiagnose, u. @ a.)


### Berufliche Stationen


1989 bis 1993
: Wissenschaftlicher Mitarbeiter der Beschleuniger-Betriebsgruppe bei GSI, Darmstadt

1993 bis 2000
: Stv. Gruppenleiter der Strahldiagnose bei GSI, Darmstadt

2001 bis 2006
: Gruppenleiter der Strahldiagnose bei GSI, Darmstadt

seit 2007
: Leitender Beschleunigerphysiker bei HIT, Heidelberg



### Motivation und Zielvorstellungen

Nach nunmehr drei Jahren aktiver Mitarbeit im KfB würde ich gern meine Tätigkeit dort noch für eine weitere Wahlperiode von drei Jahren fortsetzen. Meine Schwerpunkte in den letzten drei Jahren waren: Schaffung eines Beschleunigerpreises (noch andauernd), Mitarbeit an der KfB-Broschüre "Beschleuniger für Teilchen, Wissen und Gesellschaft" sowie aktive Teilnahme an allen internen Beratungen des KfB.

Wichtig für meine (weitere) Mitarbeit im KfB sind mir folgende Punkte:

* Förderung des wissenschaftlichen Nachwuchses und der Wahrnehmung der Beschleunigerphysik als eigenständiges Teilgebiet der Physik
* Förderung des wissenschaftlichen Austausches (national und international) sowie die Förderung des Kontakts zur Industrie
* aktive Mitgestaltung von Konferenzen, Workshops und der Beteiligung an den Jahrestagungen der DPG.
