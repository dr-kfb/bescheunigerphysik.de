![](/_shared/images/portraits/2016_lehrach.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">Prof. Dr.</span> Andreas **Lehrach**

Abteilungsleiter und Experimentsprecher
: Institut für Kernphysik (IKP-4), Forschungszentrum Jülich

Universitätsprofessor
: RWTH Aachen (III. Physikalische Institut B)



### Tätigkeiten

* Experimentelle und theoretische Beschleunigerphysik mit polarisierten Hadronenstrahlen
* Simulationsrechnungen zur Strahl- und Spindynamik



### Forschungsinteressen

* Messung elektrischer Dipolmomente
* Laser-Plasma Beschleunigung


### Berufliche Stationen

1997
: Promotion, Rheinische Friedrich-Wilhelms Universität Bonn

1999 bis 2000
: Postdoc, Brookhaven National Laboratory (NY, USA)

2008
: Habilitation in Physik, Rheinische Friedrich-Wilhelms Universität Bonn

seit 2013
: Universitätsprofessor (W2) für das Fach "Physik der Teilchenbeschleuniger", RWTH Aachen


### Motivation und Zielvorstellungen

Es ist mir ein besonderes Anliegen, die Ausbildung in der Beschleunigerphysik an Universitäten zu stärken und gleichzeitig Studenten in nationale und internationale Forschungsprojekte einzubinden, um exzellente Forschung auf dem Gebiet der Beschleunigerphysik auch zukünftig zu ermöglichen. Dabei ist mir die Vernetzung von Forschung und Lehre unter Berücksichtigung der verschiedenen Interessen von Forschungszentren und Universitäten überaus wichtig. Bei der Erarbeitung von Zukunftsstrategien ist der enge Kontakt zu anderen Forschungsgebieten entscheidend, die mittel- und langfristige Forschungsprogramme an Beschleunigeranlagen planen. Die Identifizierung von Forschungsschwerpunkten sollte einerseits in enger Abstimmung mit diesen Forschungsbereichen erfolgen, darüber hinaus sollten Forschungsvorhaben aber auch unabhängig davon gezielt unterstützt werden, die Weiter- und Neuentwicklungen in der Beschleunigerphysik und Technologie ermöglichen. Eine wichtige Aufgabe des Komitees ist aus meiner Sicht zudem die Erarbeitung von Kandidatenvorschlägen für nationalen und internationalen Forschungsgremien und eine gezielte Öffentlichkeitsarbeit, um die Interessen und Belange unseres Fachgebiets adäquat kommunizieren und vertreten zu können.
