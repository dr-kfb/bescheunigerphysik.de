![](/_shared/images/portraits/2016_assmann.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">Dr.</span> Ralph **Aßmann**

Leitender Wissenschaftler (W3)
: DESY



### Tätigkeiten

* Forschung im DESY Beschleunigerbereich (DESY-M)
* Koordinator Horizon2020 Design Studie "EuPRAXIA"
* Koordinator "European Network for Novel Accelerators"
* PI ERC Synergy Grant "AXSIS"
* Mitglied EPS-AG, Leiter Task Force für Beschleuniger-Publikationen

### Forschungsinteressen

* Linearbeschleuniger und Speicherringe
* Collider für die Hochenergiephysik
* neue Methoden bei FELs (Seeding)
* kompakte und kosteneffiziente neuartige Beschleuniger (Dielektrisch, Plasma, Accelerator-on-a-Chip)
* Teilchenphysik


### Berufliche Stationen

1990 bis 1994
: Wissenschaftlicher Mitarbeiter, MPI für Physik München

1994 bis 1998
: Post-Doc und Staff, SLAC und Stanford Universität

1998 bis 2014
: Staff Physiker und Senior-Physiker, CERN

seit 2012
: Leitender Wissenschaftler (W3), DESY

### Motivation und Zielvorstellungen

Es motiviert mich, dabei zu helfen die Anerkennung für die Beschleunigerwissenschaften zu stärken und zu optimieren. Wichtige Fortschritte wurden erreicht und das Komitee für Beschleunigerphysik hat daran einen entscheidenden Anteil. Ich denke weitere Verbesserungen in der Außenwahrnehmung unseres Feldes sind möglich. Außerdem sollten wir uns noch mehr um Publikationen für unseren Nachwuchs kümmern, gerade auch in den Großforschungszentren. Schließlich finde ich es wichtig, den breiten Nutzen der Teilchenbeschleuniger weiter bestmöglich darzustellen und damit für eine gute finanzielle Ausstattung unserer Arbeit zu werben. Dabei helfe ich im Fall meiner Wahl gerne mit.
