![](/_shared/images/portraits/2016_mueller.jpg)<!-- {.portrait} -->

Wahlkreis **Helmholtz-Zentren**

## <span class="title">Prof. Dr.</span> Anke-Susanne **Müller**

Direktorin des Instituts für Beschleunigerphysik und Technologie (IBPT)
: Karlsruher Institut für Technologie


### Tätigkeiten

* Forschung und Lehre in der Beschleunigerphysik



### Forschungsinteressen

* Erzeugung und Charakterisierung kohärenter THz-Strahlung,
* nichtlineare Stahldynamik,
* Beschleuniger-Design und -Optik,
* Strahldiagnose für kurze Elektronenpakete,
* Detektorentwicklung,
* Präzisionsmessungen der Strahlenergie



### Berufliche Stationen


* Professur für Beschleunigerphysik am KIT und Direktorin des IBPT
* Helmholtz-Hochschul Nachwuchsgruppenleiterin
* Beschleunigerphysikerin am Forschungszentrum Karlsruhe
* CERN Fellowship
* Doktorarbeit an Universität Mainz und CERN



### Motivation und Zielvorstellungen

* Engagement für das Fachgebiet "Beschleunigerphysik": es ist mir wichtig, dass Beschleunigerphysik als eigenständiges Fach größere Sichtbarkeit erlangt, und zwar sowohl im Universitäts- als auch im Grossforschungsbereich
* Stärkung des Fachs auch im Hinblick auf Fördermöglichkeiten für Forschungsprojekte in der Beschleunigerphysik
* Einsatz für die Lehre der Beschleunigerphysik und aktive Förderung des wissenschaftlichen Nachwuchses im Feld um nachhaltig Perspektiven für junge Beschleunigerphysiker zu schaffen und das Feld für junge Menschen attraktiver zu machen
* Das KfB bietet die einmalige Gelegenheit, anwendungsübergreifend Zusammenarbeiten zu unterstützen (z.B. zwischen Beschleunigern der Hochenergiephysik und Synchrotronstrahlungsquellen)
* Generell: Vertretung der Community gegenüber Politik und Öffentlichkeit
