![](/_shared/images/portraits/2016_maier.jpg)<!-- {.portrait} -->


Wahlkreis **Universitäten**

## <span class="title"> Dr.</span> Andreas R. **Maier**

Nachwuchsgruppenleiter
: Center for Free-Electron Laser Science und<br>Department Physik, Universität Hamburg





### Tätigkeiten

* Leitung des ANGUS 200 TW Hochleistungslasers
* Entwicklung, Aufbau und Betrieb der LUX Beamline zur Erzeugung von Undulatorstrahlung mit Laser-Plasma beschleunigten Elektronen

### Forschungsinteressen

* Laser-Plasma-Beschleunigung
* neue FEL-Konzepte
* Transport und Diagnostik von Laser-Plasma-generierten Strahlen
* Charakterisierung von ultra-kurzen Röntgen-FEL-Pulsen

### Berufliche Stationen

* Promotion an der LMU München, dem Max-Planck Institut für Quantenoptik Garching, und der Universität Hamburg.
* Post-Doc in Beschleunigerphysik an der Universität Hamburg
* Nachwuchsgruppenleiter am Center for Free-Electron Laser Science


### Motivation und Zielvorstellungen

* Vertretung der Interessen von Studenten und Nachwuchswissenschaftlern innerhalb des KfB
* Vernetzung von Universitäten und Großforschungsreinrichtungen in der Beschleunigerphysik
