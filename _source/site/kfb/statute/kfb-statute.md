
## Präambel

Teilchenbeschleuniger spielen eine herausragende Rolle bei der Erforschung von Elementarteilchen, Hadronen und Atomkernen, bei der Untersuchung der kondensierten Materie mit Photonen, Neutronen und geladenen Teilchen sowie in weiteren Bereichen von Wissenschaft, Technik und Medizin. Neben dem Bau und Betrieb von Teilchenbeschleunigern widmen sich Beschleunigerphysiker/innen der Weiterentwicklung von Beschleunigeranlagen und der Erarbeitung neuer Konzepte und Basistechnologien. Um diese zunehmend vernetzte Forschungs- und Entwicklungstätigkeit optimal zu gestalten und die Ausbildung des wissenschaftlichen Nachwuchses zu fördern, bedarf es einer überregionalen Koordinierung sowie einer gemeinsamen Vertretung der Beschleunigerphysiker/innen gegenüber politischen und wissenschaftlichen Organisationen und der Öffentlichkeit.

## Zweck und Aufgaben des Komitees<!-- {#artikel-1 data-outline="Artikel 1"} -->

Das ständige "Komitee für Beschleunigerphysik" (KfB) vertritt die Gemeinschaft der deutschen Beschleunigerphysiker/innen nach außen und fördert den Kontakt innerhalb der Gemeinschaft. Die Gemeinschaft der "deutschen Beschleunigerphysiker/innen" umfasst Personen, die

-   ein Hochschulstudium mit Bachelor, Master, Diplom oder Promotion abgeschlossen haben und
-   entweder an einer wissenschaftlichen Einrichtung in der Bundesrepublik Deutschland oder als Deutsche an Einrichtungen, bei denen die Bundesrepublik Deutschland an der Grundfinanzierung beteiligt ist (z.B. CERN, ESRF), tätig sind und
-   eine Forschungs-, Entwicklungs-, oder Lehrtätigkeit oder eine Leitungsfunktion im Bereich der Beschleunigerphysik ausüben.

Zu den Aufgaben des Komitees gehören insbesondere

-   Mitwirkung an der Erarbeitung von Zukunftsstrategien der Beschleunigerphysik in  Deutschland und an internationalen
-   Einrichtungen mit deutscher Beteiligung
-   Identifizierung von Forschungsschwerpunkten von überregionaler Bedeutung und Beratung forschungsfördernder Institutionen hierzu
-   Vertretung der Gemeinschaft in nationalen und internationalen Gremien
-   Informationsaustausch und Abstimmung der Interessen zwischen Universitäten, Großforschungszentren und anderen wissenschaftlichen Einrichtungen mit Bezug zur Beschleunigerphysik
-   Stärkung der deutschen Beteiligung an internationalen Projekten und Einrichtungen
-   Förderung der Lehre, des wissenschaftlichen Nachwuchses und der Wahrnehmung der  Beschleunigerphysik als eigenständiges Teilgebiet der Physik
-   Förderung des Kontakts zu anderen Forschungsgebieten, in denen Beschleuniger genutzt werden (z.B. Teilchenphysik, Materialwissenschaften, Medizin) oder die von der Beschleunigerphysik genutzt werden (z.B. Elektrotechnik, Laserphysik)
-   Förderung des Kontakts zur Industrie
-   Anregungen zu Konferenzen, Workshops und der Beteiligung an den Jahrestagungen der DPG
-   Information der Gemeinschaft deutscher Beschleunigerphysiker/innen über wichtige gemeinsame Belange
-   Öffentlichkeitsarbeit

## Zusammensetzung des Komitees<!-- {#artikel-2 data-outline="Artikel 2"} -->


Das Komitee setzt sich aus zwölf gewählten Vertretern/innen zusammen, die folgende Wahlkreise vertreten: 

-   deutsche Hochschulen (fünf Vertreter/innen)
-   Helmholtz-Zentren (fünf Vertreter/innen)
-   sonstige deutsche Forschungsinstitute (ein/e Vertreter/in)
-   ausländische Institute mit deutscher Beteiligung (ein/e Vertreter/in) Kommt die genannte Zahl von Vertretern/innen in einem Wahlkreis nicht zustande, verringert sich die Zahl der Komiteemitglieder entsprechend. Der/die Vorsitzende des DPG-Arbeitskreises Beschleunigerphysik ist ex-officio Mitglied des KfB.

Gäste des Komitees sind

-   je ein/e von folgenden Komitees entsandte/r Wissenschaftler/in
    -   Komitee für Elementarteilchenphysik
    -   Komitee für Synchrotronstrahlung
    -   Komitee für Hadronen- und Kernphysik
-   je ein/e vom BMBF, den Projektträgern und der DFG entsandte/r Vertreter/in
-   weitere von Fall zu Fall vom Komitee eingeladene Gäste

## Forum Beschleunigerphysik<!-- {#artikel-3 data-outline="Artikel 3"} -->

Das Forum Beschleunigerphysik ist eine Interessensgemeinschaft deutscher Beschleunigerphysiker/-innen. Es dient dem wissenschaftlichen Austausch und der Vorbereitung von Forschungsverbünden. Jedes Mitglied der Gemeinschaft deutscher Beschleunigerphysiker/-innen im Sinne von Artikel 1 kann durch Registrierung beim KfB Mitglied des Forums Beschleunigerphysik werden.

## Wahlverfahren<!-- {#artikel-4 data-outline="Artikel 4"} -->

Die gewählten Mitglieder werden für eine Dauer von 3 Jahren bestimmt. Wahlberechtigt und wählbar sind alle deutschen Beschleunigerphysiker/innen im Sinne von Artikel 1 und 3 dieser Satzung, die sich mittels der Internetseite des KfB registriert haben. Im Zweifelsfall entscheidet das amtierende Komitee.

Die Wahl ist geheim und erfolgt durch Briefwahl oder ein äquivalentes elektronisches Verfahren. Jeder/e Wahlberechtigte hat vier Stimmen, die beliebig auf Kandidaten/innen aller Wahlkreise verteilt werden können. Die Abgabe von weniger Stimmen ist zulässig. Stimmenhäufung ist auf maximal 2 Stimmen pro Kandidat/in begrenzt. Gewählt werden die Kandidaten/innen mit den meisten Stimmen. Bei Stimmengleichheit entscheidet das Los.

Kandidatenvorschläge sind an den/die Vorsitzende/n des amtierenden Komitees zu richten, der/die eine Vorschlagsliste erstellt. Das Komitee führt die Wahl in Zusammenarbeit mit einer unabhängigen Institution (z.B. dem zuständigen Projektträger des BMBF) durch. Die Vollständigkeit der Wählerliste liegt in der Verantwortung des/r Vorsitzenden des Komitees. Fehler in der Wählerliste sind jedoch in der Regel kein Grund, die Wahl anzufechten. Im Zweifelsfall entscheidet das vor der Wahl amtierende Komitee.

Die Mitglieder des neu gewählten Komitees bestimmen aus ihrer Mitte eine/n Vorsitzende/n und dessen Stellvertreter/in in geheimer Wahl für 3 Jahre. Bei Stimmengleichheit findet eine Stichwahl statt. Unmittelbare Wiederwahl ist nur einmal möglich. Scheidet ein Mitglied des Komitees vor Ende der 3-jährigen Wahlperiode aus, kann das Komitee eine/n Nachfolger/in bis zum Ende der Wahlperiode bestimmen. Scheidet die Hälfte der ursprünglich gewählten Mitglieder vor Ende der Wahlperiode aus, setzt der/die Vorsitzende unverzüglich eine Neuwahl an. Scheidet der/die Vorsitzende oder dessen Stellvertreter/in vorzeitig aus, bestimmen die Mitglieder aus ihrer Mitte eine/n neue/n Vorsitzende/n und Stellvertreter/in.

Die Gäste des Komitees werden von den oben genannten Institutionen benannt, die vom/von der Vorsitzenden zur Entsendung eines/r Vertreters/in aufgefordert werden. Die Gäste werden vom/von der Vorsitzenden der Komitees eingeladen. Der Gaststatus unterliegt keiner zeitlichen Begrenzung.

## Sitzungsfolge, Protokoll, Beschlussfähigkeit<!-- {#artikel-5 data-outline="Artikel 5"} -->


Das Komitee tagt mindestens einmal im Jahr. Auf Wunsch von mindestens 1/3 der Mitglieder des Komitees können zusätzliche Sitzungen einberufen werden. Der/die Vorsitzende beruft die Sitzung 4 Wochen vor dem Termin ein und veröffentlicht die Tagesordnung spätestens 1 Woche vor der Sitzung auf der Webseite des Komitees.

Bei jeder Sitzung wird ein Protokoll geführt, das im Umlaufverfahren von den Mitgliedern und den im Protokoll zitierten Gästen genehmigt oder ergänzt wird und anschließend allen Mitgliedern der Gemeinschaft zugänglich gemacht wird.

Beschlüsse werden auf den einberufenen Sitzungen von den anwesenden Mitgliedern des Komitees mit einfacher Mehrheit gefasst. Das Komitee ist beschlussfähig, wenn mindestens die Hälfte der Mitglieder an der Sitzung teilnimmt. Bei Stimmengleichheit entscheidet der/die Vorsitzende.

Es wird eine jährliche Vollversammlung durchgeführt, deren Termin sechs Monate vorher anzukündigen ist.

## Kosten<!-- {#artikel-6 data-outline="Artikel 6"} -->


Die Kosten, die den Mitgliedern durch die Mitarbeit im Komitee entstehen, z.B. Reisekosten, werden von ihnen selbst getragen. Gemeinsame Kosten, z.B. Portokosten, werden möglichst gleichmäßig verteilt, soweit sich dem Komitee keine weiteren Finanzierungsmöglichkeiten erschließen.

## Satzungsänderung<!-- {#artikel-7 data-outline="Artikel 7"} -->


Änderungen der Satzung sind der Vollversammlung vorzuschlagen und eine Mehrheit von 2/3 der anwesenden Mitglieder muss dem Änderungsvorschlag zustimmen



Fassung vom 01. @@ April @@ 2014
