"use strict";

import {
    KfBElectionsBeautifier
} from "./kfb-elections-beautifier.js"


export function KfBElectionsRunner() {

    let beautifier = new KfBElectionsBeautifier();
    beautifier.beautify();

}
