"use strict";

const d3 = require("d3");

export class KfBElectionsBeautifier {

    constructor() {}

    beautify() {


        function unsetActiveProfile() {
            let profiles = d3.selectAll(".candidate-profiles__profile")
            profiles.classed("hidden", false)
            let activeSelector = d3.selectAll(".candidate-profiles__selector-item")
            activeSelector.classed("active", false)
            history.pushState('', document.title, window.location.pathname);

        }


        function setActiveProfile(selector) {

            let profiles = d3.selectAll(".candidate-profiles__profile")
            profiles.classed("hidden", true)

            let activeProfile = d3.select(selector)
            activeProfile.classed("hidden", false)

            let selectors = d3.selectAll(".candidate-profiles__selector-item")
            selectors.classed("active", false)

            let activeSelector = d3.select(".candidate-profiles__selector-item--" + selector.substr(1))
            activeSelector.classed("active", true)

            window.location.hash = " ";
            window.location.hash = selector;
        }

        if (window.location.hash) {
            console.log(window.location.hash)
            let hashSelection = d3.select(window.location.hash)
            console.log(window.location.hash)

            if (window.location.hash !== "#candidate-profiles__filter" && hashSelection.size() > 0) {
                setActiveProfile(window.location.hash)
            } else {

                console.log("not found: ", window.location.hash)
            }
        }

        // function changeActiveProfile(selector) {
        //     d3.select("#candidate-profiles__filter").attr("class", "")
        //     d3.select("#candidate-profiles__filter").classed("filter-value__" + selectorw.substr(1), true)
        //     window.location.hash = " ";
        //     window.location.hash = selector;
        // }

        function handleSelectorClick(d, i) {

            let event = d3.event;
            event.stopPropagation()

            // let toggleClassName = d3.select(event.target).attr("data-class-toggle__class-name");
            // let toggleTargetSelector = d3.select(event.target).attr("data-class-toggle__target-selector");
            // let toggleValue = d3.select(event.target).property("checked");
            // d3.select(toggleTargetSelector).classed(toggleClassName, toggleValue)


            let el = d3.select(this);
            let target = el.attr("href")
            console.log(target)

            // changeActiveProfile(target)

            if (target === window.location.hash) {
                unsetActiveProfile()
            } else {
                setActiveProfile(target)
            }


        }


        d3.selectAll(".candidate-profiles__selector-item a").on("click", handleSelectorClick)

        var width = 300,
            height = 180,
            radius = 40,
            data = [
                [227, 178],
                [160, 238],
                [70, 193]
            ];

        var pie = d3.pie()
            .value(function(d) {
                return d;
            })
            .sort(null);

        var arc = d3.arc()
            .innerRadius(50)
            .outerRadius(80);

        var svg = d3.selectAll(".turnout-svg").append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g").attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");


        var path = svg.data(data).selectAll("path")
            .data(pie)
            .enter().append("path")
            .attr("class", function(d, i) {
                return "arc-" + i;
            })
            .attr("title", d => JSON.stringify(d))
            .attr("d", arc);

    }

}
