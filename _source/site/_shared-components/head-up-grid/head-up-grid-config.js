definegrid = function() {
    var browserWidth = jQuery(window).width();
    if (browserWidth >= 1200) {
        gridName = "xl";
        pageUnits = 'px';
        colUnits = 'px';
        
        gutterwidth = 30;
        rowheight = 30;

        pagewidth = 1092

        columns = 6;
        columnwidth = (pagewidth - 11 * gutterwidth) / columns;

        pagetopmargin = 0;

        gridonload = 'on';
        makehugrid();
    }
    if (browserWidth < 1200) {
        gridName = "lg";
        pageUnits = 'px';
        colUnits = 'px';
        rowheight = 26;
        gutterwidth = rowheight;
        pagewidth = 910
        columns = 12;
        columnwidth = 52; //(pagewidth - (columns - 1) * gutterwidth) / columns;
        pagetopmargin = 0;
        gridonload = 'on';
        makehugrid();
    }
    if (browserWidth < 992) {
        gridName = "md";
        pageUnits = 'px';
        colUnits = 'px';
        rowheight = 26;
        gutterwidth = 26;
        pagewidth = 768 - gutterwidth;
        columns = 6;
        columnwidth = (pagewidth - (columns - 1) * gutterwidth) / columns;
        console.log(columnwidth)
        pagetopmargin = 0;
        gridonload = 'on';
        makehugrid();
    }
    if (browserWidth < 768) {
        gridName = "sm";
        pageUnits = '%';
        colUnits = '%';
        rowheight = 26;
        pagewidth = 100;
        columns = 2;
        columnwidth = 49;
        gutterwidth = 2;
        pagetopmargin = 0;

        gridonload = 'on';
        makehugrid();
    }
    if (browserWidth < 544) {
        gridName = "xs";
        pageUnits = '%';
        colUnits = '%';
        pagewidth = 100;
        columns = 2;
        columnwidth = 49;
        gutterwidth = 2;
        pagetopmargin = 0;
        rowheight = 26.25;
        gridonload = 'on';
        makehugrid();
    }
};
$(document).ready(function() {
    definegrid();
    setgridonload();
});


$(window).bind('resizeEnd', function() {
    definegrid();
    setgridonresize();
});
$(window).resize(function() {
    if (this.resizeTO) clearTimeout(this.resizeTO);
    this.resizeTO = setTimeout(function() {
        $(this).trigger('resizeEnd');
    }, 500);
});
