"use strict";

var d3 = require("d3")

export class TeaserLinksEnhancer {

    constructor() {
        d3.selectAll(".c-teaser").on("click", function() {
            var evt = d3.event;
            var href = d3.select(evt.target).attr("href")
            if (href) {
                window.location.href = href;
                evt.stopPropagation();
            }
        });
    }
}
