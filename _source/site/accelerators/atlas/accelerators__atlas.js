'use strict';

var d3 = require("d3")

import {
    bboxCollide
} from 'd3-bboxCollide'


function createAtlasChart(parentSelector) {

    let chart = {};
    chart.dimensions = {}
    chart.parentSelector = parentSelector;

    chart.layers = {};
    chart.states = {};
    chart.isReady = false;

    chart.config = {};
    chart.config.paddingLeftRight = 15; // adjust the padding values depending on font and font size
    chart.config.paddingTopBottom = 5;
    chart.config.locationMarkerRadius = 2

    chart.graph = {};
    chart.graph.nodes = [];
    chart.graph.links = [];


    function getDimensions(parentSelector) {

        let bbbox = d3.select(chart.parentSelector).node().getBoundingClientRect()
        return {
            height: bbbox.height,
            width: bbbox.width
        }
    }


    function updateSize() {
        chart.dimensions = getDimensions(chart.parentSelector);

        chart.svg
            .attr("height", chart.dimensions.height)
            .attr("width", chart.dimensions.width)


    }

    function createLayers() {

        chart.layers.background =
            chart.svg.append("g").classed("layer--background", true)
        chart.layers.chart =
            chart.svg.append("g").classed("layer--chart", true)
        chart.layers.map =
            chart.layers.chart.append("g").classed("layer--map", true)
        chart.layers.nodes =
            chart.layers.chart.append("g").classed("layer--nodes", true)
        chart.layers.links =
            chart.layers.chart.append("g").classed("layer--links", true)

    }

    function updateProjection() {

        let extremePoints = {
            "type": "FeatureCollection",
            "features": [{
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [5.689927, 45.208477]
                }
            }, {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [13.249533, 55.734199]
                }
            }]
        }

        chart.projection = d3.geoAlbers()
            .rotate([0, 0])
            .fitExtent([
                [chart.dimensions.width * 0.6, chart.dimensions.height * 0.1],
                [chart.dimensions.width * 0.8, chart.dimensions.height * 0.9]
            ], extremePoints)

        chart.pathProjector = d3.geoPath()
            .projection(chart.projection);
    }

    function drawMap() {

        if (chart.mapData.objects['ne-selection_european-countries']) {

            chart.layers.map.selectAll('.atlas__country')
                .data(topojson.feature(chart.mapData,
                        chart.mapData.objects['ne-selection_european-countries'])
                    .features)
                .enter().append('path')
                .attr('class', d => 'atlas__country ' + d.id)
                .attr('d', chart.pathProjector);
        }
        if (chart.mapData.objects['ne-selection_bundeslaender']) {
            chart.layers.map.selectAll('.atlas__bundesland')
                .data(topojson.feature(chart.mapData,
                    chart.mapData.objects['ne-selection_bundeslaender']).features)
                .enter().append('path')
                .attr('class', 'atlas__bundesland')
                .attr('d', chart.pathProjector);
        }

    }

    function processAtlasData(data) {
        data.forEach(d => {
            d.lat = +d.lat;
            d.lng = +d.lng;
            d.size = +d.size;
            d.max_energy = +d.max_energy;
            d.abroad = !!d.abroad;

            d.label_dx = +d.label_dx;
            d.label_dy = +d.label_dy;

            let projectedLocation = chart.projection([d.lng, d.lat]);
            d.x0 = projectedLocation[0];
            d.y0 = projectedLocation[1];
            d.x = projectedLocation[0];
            d.y = projectedLocation[1];

            d.label_x = projectedLocation[0];
            d.label_y = projectedLocation[1];


            if (d.type === "accelerator") {
                d.label_horizontal_aligment = "right"
                d.label_vertical_aligment = "top"
            } else {
                d.label_horizontal_aligment = "left"
                d.label_vertical_aligment = "bottom"
            }

            if (d.label_alignment.indexOf("top") > -1)
                d.label_vertical_aligment = "top"
            if (d.label_alignment.indexOf("bottom") > -1)
                d.label_vertical_aligment = "bottom"

            if (d.label_alignment.indexOf("left") > -1)
                d.label_horizontal_aligment = "left"
            if (d.label_alignment.indexOf("right") > -1)
                d.label_horizontal_aligment = "right"

        })
        //         let nested = d3.nest().key(d => d.type).map(data)
        // chart.data.accelerators = nested.get("accelerator")
        // chart.data.universities = nested.get("university")
        // chart.data.researchInstitutes = nested.get("research-institute")




        let sizes = data.map(d => d.size);
        chart.sizeScale = d3.scaleLinear().domain([d3.min(sizes), d3.max(sizes)]).range([1, 10]);

        let energies = data.map(d => d.max_energy);
        chart.energyScale = d3.scaleLinear().domain([d3.min(energies), d3.max(energies)]).range([1, 10]);

        data.sort(function (a, b) {
            return a.y < b.y
        })

        return data

    }

    function init() {

        chart.svg = d3.select(chart.parentSelector).append("svg")

        createLayers()

        updateSize()

        d3.json('/_shared/data/maps/ne-selection.topo.json', function (error, mapData) {

            chart.mapData = mapData


            updateProjection()
            drawMap()


            d3.json("/_shared/data/accelerators__atlas.json", function (error, data) {

                if (error) throw error;

                chart.atlasData = processAtlasData(data)

                chart.linkElements = chart.layers.links
                    .selectAll(".link")
                    .data(chart.atlasData)
                    .enter()
                    .append("line")
                    .classed("link", true)
                    .attr("opacity", 0)

                chart.locationMarkerElements = chart.layers.chart
                    .selectAll(".location-marker")
                    .data(chart.atlasData)
                    .enter()
                    .append("circle")
                    .attr("opacity", 0)
                    .attr("cx", d => d.x)
                    .attr("cy", d => d.y)
                    .attr("r", chart.config.locationMarkerRadius)
                    .classed("location-marker", true)
                    .classed("research-institute", d => d.type === "research-institute")
                    .classed("university", d => d.type === "university")
                    .classed("accelerator", d => d.type === "accelerator")
                    .classed("accelerator--electrons", d => d.particles === "electrons")
                    .classed("accelerator--ions", d => d.particles === "ions")


                chart.labelElements = chart.layers.chart
                    .selectAll(".label")
                    .data(chart.atlasData)
                    .enter()
                    .append("g")
                    .attr("opacity", 0)
                    .classed("label", true)
                    .classed("research-institute", d => d.type === "research-institute")
                    .classed("university", d => d.type === "university")
                    .classed("accelerator", d => d.type === "accelerator")
                    .classed("accelerator--electrons", d => d.particles === "electrons")
                    .classed("accelerator--ions", d => d.particles === "ions")

                appendLabel(chart.labelElements)


                chart.tabularElements = chart.layers.chart
                    .selectAll(".tabular-element")
                    .data(chart.atlasData)
                    .enter()
                    .append("g")
                    .attr("opacity", 0)
                    .classed("label", true)
                    .classed("research-institute", d => d.type === "research-institute")
                    .classed("university", d => d.type === "university")
                    .classed("accelerator", d => d.type === "accelerator")
                    .classed("accelerator--electrons", d => d.particles === "electrons")
                    .classed("accelerator--ions", d => d.particles === "ions")


                chart.mapLayoutSimulationNodes = []
                chart.mapLayoutSimulationLinks = []

                chart.atlasData.forEach((d, i) => {

                    chart.mapLayoutSimulationNodes.push({
                        type: "location-marker",
                        width: 5,
                        height: 5,
                        fx: d.x,
                        fy: d.y,
                        atlasDataIndex: i,
                    })

                    let node = {
                        type: "label",
                        width: d.label_bb.width,
                        height: d.label_bb.height,
                        x: d.label_x,
                        y: d.label_y,
                        atlasDataIndex: i
                    }

                    if (d.type === "accelerator") {
                        node.fx = chart.dimensions.width * 0.6
                    }


                    if (d.type === "university") {
                        node.fx = chart.dimensions.width * 0.8
                    }

                    if (d.type === "research-institute") {
                        node.fy = chart.dimensions.height * 0.8
                    }


                    chart.mapLayoutSimulationNodes.push(node)

                    chart.mapLayoutSimulationLinks.push(

                        {
                            source: 2 * i,
                            target: 2 * 1 + 1
                        }
                    )
                })
                layoutMap()

                // chart.symbolElements = chart.layers.chart
                //     .selectAll(".symbol")
                //     .data(chart.atlasData)
                //     .enter()
                //     .append("g")
                //     .classed("symbol", true)
                //     .classed("research-institute", d => d.type === "research-institute")
                //     .classed("university", d => d.type === "university")
                //     .classed("accelerator", d => d.type === "accelerator")
                //     .classed("accelerator--electrons", d => d.particles === "electrons")
                //     .classed("accelerator--ions", d => d.particles === "ions")

                // appendSymbol(chart.symbolElements)


                chart.isReady = true;

                // goto("atlas--map")
            });
        })

    }




    function appendLabel(selection) {

        // selection.call(chart.labelMapLayout)
        let labels = selection
            .append("g")
            .classed("label-group", true)

        let labelBackgrounds = labels.append("rect")
            .classed("label-background", true)

        let labelTexts = labels.append("text")
            .classed("label-text", true)
            .text(d => d.name_short)
            // .attr("dx", d => ((d.label_horizontal_aligment === "right") ? -1 : 1) * chart.config.paddingLeftRight / 2)
            // .attr("dy",  d => -1 * ((d.label_vertical_aligment === "bottom") ? -1 : 1) * chart.config.paddingTopBottom / 2 + 1)
            .each(function (d, i) {
                d.labelText_bb = this.getBBox()
            })


        labelBackgrounds
            .attr("x", function (d) {
                return -0.5 * (d.labelText_bb.width + 1 * chart.config.paddingLeftRight);
            })
            .attr("y", function (d) {

                return -0.5 * (d.labelText_bb.height + 1 * chart.config.paddingTopBottom)
            })
            .attr("width", function (d) {
                return d.labelText_bb.width + chart.config.paddingLeftRight;
            })
            .attr("height", function (d) {
                return d.labelText_bb.height + chart.config.paddingTopBottom;
            }).each(function (d, i) {
                d.label_bb = this.getBBox()
            })

        labels.attr("transform", d => {
            let dx = ((d.label_horizontal_aligment === "right") ? -1 : 1) * (d.label_bb.width / 2);
            let dy = (((d.label_vertical_aligment === "bottom") ? -.5 : .5) + d.label_dy) * (d.label_bb.height);


            return getTranslateString(dx, dy)
        })

    }

    function goto(stepId) {

        if (chart.isReady && chart.states[stepId]) {

            chart.states[stepId]()
        }
        if (!chart.isReady && chart.states[stepId]) {

            setTimeout(() => {
                goto(stepId)
            }, 300)

        }
    }

    function getTranslateString(x, y) {

        return "translate(" + x + ", " + y + ")";
    }


    function layoutMap(nodes) {

        var collide = bboxCollide().bbox(function (d, i) {

            return [
                [-0.5 * d.width, -0.5 * d.height],
                [0.5 * d.width, 0.5 * d.height]
            ]
        })


        function mapTick() {

            chart.mapLayoutSimulationNodes.forEach((d, i) => {
                if (d.type === "label") {
                    chart.atlasData[d.atlasDataIndex].label_x = d.x;
                    chart.atlasData[d.atlasDataIndex].label_y = d.y;
                }
            })

            chart.labelElements
                .attr("transform", d => {
                    return getTranslateString(d.label_x, d.label_y)
                })

            chart.linkElements
                .attr("x1", d => d.x)
                .attr("y1", d => d.y)
                .attr("x2", d => d.label_x)
                .attr("y2", d => d.label_y)
        }
        chart.simulation = d3.forceSimulation(chart.mapLayoutSimulationNodes)
            // .force("x", d3.forceX(function (d) {
            //     return d.x
            // }).strength(1))
            // .force("y", d3.forceY(function (d) {
            //     return d.y
            // }))
            // .force("link", d3.forceLink(chart.mapLayoutSimulationLinks).strength(100).distance(2))
            // .force("collide", collide)
            // .force("collision", collide)
            // .force("collision", d3.forceCollide().radius(15))

            .on('tick', mapTick)

    }

    chart.states["atlas--number"] = function () {

        chart.labelElements
            .attr("opacity", d => {
                return 1
            })
        chart.locationMarkerElements
            .attr("opacity", d => {
                return 1
            })
        chart.linkElements
            .attr("opacity", d => {
                return 1
            })

    }

    chart.states["atlas--number-accelerators"] = function () {

        chart.labelElements
            .attr("opacity", d => {
                return (d.type === "accelerator" && !d.abroad) ? 1 : 0.2
            })
            .attr("transform", d => {
                let scale = (d.type === "accelerato") ? "scale(1.5)" : "scale(1)"
                let translate = getTranslateString(d.label_x, d.label_y)
                return scale + " " + translate;
            })

    }

    chart.states["atlas--number-accelerators-abroad"] = function () {

        chart.labelElements
            .attr("opacity", d => {
                return (d.type === "accelerator" && d.abroad) ? 1 : 0.2
            })
    }

    chart.states["atlas--number-universities"] = function () {

        chart.labelElements
            .attr("opacity", d => {
                return (d.type === "university") ? 1 : 0.2
            })
    }

    chart.states["atlas--number-institutes"] = function () {

        chart.labelElements
            .attr("opacity", d => {
                return (d.type === "research-institute") ? 1 : 0.2
            })
            .attr("transform", d => {
                let scale = (d.type === "research-institute") ? "scale(1.5)" : "scale(1)"
                let translate = getTranslateString(d.label_x, d.label_y)
                return scale + " " + translate;
            })
    }


    // chart.states["atlas--size"] = function () {

    //     chart.acceleratorElements.selectAll(".accelerator-symbol")
    //         .attr("transform", d => {
    //             return "scale(" + chart.sizeScale(d.size) + ")"
    //         })
    // }

    // chart.states["atlas--energy"] = function () {

    //     chart.acceleratorElements.selectAll(".accelerator-symbol")
    //         .attr("transform", d => {
    //             return "scale(" + chart.energyScale(d.max_energy) + ")"
    //         })

    // }


    init();


    return {
        goto
    }
}

exports.createAtlasChart = createAtlasChart;





// let labelPadding = 10

// chart.textLabel = layoutTextLabel()
//     .padding(labelPadding)
//     .value(d => d.name_short);

// chart.textLabelStrategy = layoutAnnealing();


// chart.labelMapLayout = layoutLabel(chart.textLabelStrategy)
//     .size((d, i, g) => {
//         // measure the label and add the required padding
//         const textSize = g[i].getElementsByTagName('text')[0].getBBox();
//         console.log(textSize)
//         return [textSize.width + labelPadding * 2, textSize.height + labelPadding * 2];
//     })
//     .position(d => chart.projection([d.lng, d.lat]))
//     .component(chart.textLabel);



//   function appendSymbol(selection) {

//         selection.filter(d => d.subtype === "synchrotron" || d.subtype === "microtron")
//             .append("rect")
//             .attr("opacity", 0)
//             .classed("accelerator-symbol", true)
//             .attr("x", -35)
//             .attr("y", -35)
//             .attr("rx", 35)
//             .attr("ry", 35)
//             .attr("width", 70)
//             .attr("height", 70)

//         selection
//             .filter(d => d.subtype === "linear-accelerator")
//             .append("rect")
//             .attr("opacity", 0)
//             .classed("accelerator-symbol", true)
//             .attr("x", -30)
//             .attr("y", -30)
//             .attr("width", 60)
//             .attr("height", 60)

//         selection.filter(d => d.subtype === "synchrotron" || d.subtype === "microtron")
//             .append("rect")
//             .attr("opacity", 0)
//             .classed("accelerator-symbol", true)
//             .attr("x", -35)
//             .attr("y", -35)
//             .attr("rx", 35)
//             .attr("ry", 35)
//             .attr("width", 70)
//             .attr("height", 70)
//     }

//     function appendUniversitySymbol(selection) {
//         selection
//             .append("circle")
//             .classed("location-marker", true)
//             .attr("cx", 0)
//             .attr("cy", 0)
//             .attr("r", chart.config.locationMarkerRadius)


//         appendLabel(selection)

//     }

//     function appendResearchInstituteSymbol(selection) {

//         selection
//             .append("circle")
//             .classed("location-marker", true)
//             .attr("cx", 0)
//             .attr("cy", 0)
//             .attr("r", chart.config.locationMarkerRadius)
//         appendLabel(selection)

//     }





//     let simulation = d3.forceSimulation(chart.acceleratorElements)
//     .velocityDecay(0.2)
//        .force("x", d3.forceX().strength(0.002))
// .force("y", d3.forceY().strength(0.002))
//  .force("collide", d3.forceCollide().radius(function(d) { return 10 + 0.5; }).iterations(1))
// .on("tick", ticked);

//             // .force("link", d3.forceLink().id(function (d, i) {
//             //     return d.id;
//             // }).distance(0).strength(link => {
//             //     if (link.type === "organisation") return 0
//             //     return 0;
//             // }))
//             .force("charge", d3.forceManyBody().strength(-1))
//             .force("collide", d3.forceCollide().radius(d => {
//                 console.log(d)

//                 return 20;
//             }).iterations(1).strength(1.1));

// simulation = d3.forceSimulation(nodes)

//     .force("x", d3.forceX().strength(0.002))
//     .force("y", d3.forceY().strength(0.002))
//     .force("collide", d3.forceCollide().radius(function(d) { return d.r + 0.5; }).iterations(2))
//     .on("tick", ticked);



// function ticked() {

//     chart.labelElements
//         .attr("x", d => d.x)
//         .attr("y", d => d.y)


//     // .attr('x', d => d.x )
//     // .attr('y', d => d.y );

//     // thisObject.personElements
//     //     .attr('cx', d => d.x)
//     //     .attr('cy', d => d.y);

//     // thisObject.affiliationElements
//     //     .attr("x1", d => d.target.x)
//     //     .attr("y1", d => d.target.y)
//     //     .attr("x2", d => d.source.x)
//     //     .attr("y2", d => d.source.y)
// }

// // chart.simulation.nodes(chart.acceleratorElements)
// //     .velocityDecay(.2)
// //     .on('tick', ticked);