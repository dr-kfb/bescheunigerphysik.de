'use strict';

var d3 = require("d3")

function createOperatingPrincipleRemote(iFrameSelector) {

    let chart = {};
    chart.isReady = false;
    chart.iframe = document.getElementById(iFrameSelector);
    chart.states = {};

    chart.states["operating-principle--overview"] = function () {

        chart.iframe.contentWindow.b4w.require("accelerators__operating-principle_app").changeView("overview")
    }



    chart.states["operating-principle--particle-source"] = function () {

        chart.iframe.contentWindow.b4w.require("accelerators__operating-principle_app").changeView("injector")
    }
    chart.states["operating-principle--accelerator-module"] = function () {

        chart.iframe.contentWindow.b4w.require("accelerators__operating-principle_app").changeView("module")
    }


    chart.states["operating-principle--focussing-magnet"] = function () {

        chart.iframe.contentWindow.b4w.require("accelerators__operating-principle_app").changeView("quadrupoles")
    }


    chart.states["operating-principle--bending-magnet"] = function () {

        chart.iframe.contentWindow.b4w.require("accelerators__operating-principle_app").changeView("dipole")
    }
    setTimeout(() => {
                chart.isReady = true
            }, 2000)


    function goto(stepId) {


        if (chart.isReady && chart.states[stepId]) {

            console.log(chart.states[stepId])

            chart.states[stepId]();

        }
        if (!chart.isReady && chart.states[stepId]) {

            setTimeout(() => {
                goto(stepId)
            }, 300)

        }
    }




    return {
        goto
    }
}

exports.createOperatingPrincipleRemote = createOperatingPrincipleRemote;