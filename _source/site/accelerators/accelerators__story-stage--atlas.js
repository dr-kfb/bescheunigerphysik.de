// 'use strict';

// var d3 = require("d3")


// function createAtlasStage(id) {

//     let chart = {};
//     chart.dimensions = {}
//     chart.parentId = id;

//     chart.layers = {};
//     chart.states = {};

//     function getDimensions(parentId) {

//         let bbbox = d3.select(parentId).node().getBoundingClientRect()
//         return {
//             height: bbbox.height,
//             width: bbbox.width
//         }
//     }

//     function initChart() {

//         chart.dimensions = getDimensions(chart.parentId);

//         chart.svg = d3.select(id + " svg")
//             .attr("height", chart.dimensions.height)
//             .attr("width", chart.dimensions.width)

//         chart.layers.background =
//             chart.svg.append("g")
//             .classed("layer--background", true)
//         chart.layers.chart =
//             chart.svg.append("g")
//             .classed("layer--chart", true)



//         d3.json("/_shared/data/accelerators__atlas.json", function (error, data) {

//             if (error) throw error;

//             chart.data = data;

//             data.forEach(d => {
//                 d.size = +d.size;
//                 d.max_energy = +d.max_energy;
//                 d.x = Math.random() * chart.dimensions.width * .5 + (chart.dimensions.width * .5)
//                 d.y = (Math.random() * chart.dimensions.height)
//             })




//             let sizes = data.map(d => d.size);
//             chart.sizeScale = d3.scaleLinear().domain([d3.min(sizes), d3.max(sizes)]).range([1, 10]);

//             let energies = data.map(d => d.max_energy);
//             chart.energyScale = d3.scaleLinear().domain([d3.min(energies), d3.max(energies)]).range([1, 10]);
//             console.log(energies)

//             chart.acceleratorsEnter = chart.layers.chart.selectAll(".accelerator").data(chart.data).enter()

//             chart.accelerators = chart.acceleratorsEnter
//                 .append("g")
//                 .classed("accelerator", true)


//             let symbols1 = chart.accelerators
//                 .filter(d => d.accelerator_type === "synchrotron" || d.accelerator_type === "microtron")
//                 .append("rect")
//                 .classed("accelerator-symbol", true)
//                 .classed("accelerator--synchrotron", true)
//                 .classed("accelerator--electrons", d => d.particles === "electrons")
//                 .classed("accelerator--ions", d => d.particles === "ions")
//                 .attr("x", -35)
//                 .attr("y", -35)
//                 .attr("rx", 35)
//                 .attr("ry", 35)
//                 .attr("width", 70)
//                 .attr("height", 70)

//             let symbols2 = chart.accelerators
//                 .filter(d => d.accelerator_type === "linear-accelerator")
//                 .append("rect")
//                 .classed("accelerator-symbol", true)
//                 .classed("accelerator--linear-accelerator", true)
//                 .classed("accelerator--electrons", d => d.particles === "electrons")
//                 .classed("accelerator--ions", d => d.particles === "ions")
//                 .attr("x", -45)
//                 .attr("y", -35)
//                 .attr("width", 90)
//                 .attr("height", 70)

//             chart.accelerators.selectAll(".accelerator-symbol")

//                 .attr("transform", d => {
//                     return "scale(1)"
//                 })


//             chart.accelerators
//                 .append("text")
//                 .classed("accelerator-label", true)
//                 .text(d => d.name_short)


//             // processData(data);

//         });

//     }

//     function goto(stepId) {


//         chart.svg.select(".stepId")
//             .remove();
//         chart.svg.append("text")
//             .classed("stepId", true)
//             .attr("x", chart.dimensions.width - 100)
//             .attr("y", 300)
//             .text(stepId)

//         if (chart.states[stepId]) {

//             chart.states[stepId]()
//         }
//     }

//     function getTranslateString(x, y) {

//         return "translate(" + x + ", " + y + ")";

//     }

//     chart.states["atlas--number"] = function () {

//         chart.accelerators
//             .attr("transform", d => {
//                 return getTranslateString(d.x, d.y)
//             })
//         chart.accelerators.selectAll(".accelerator-symbol").attr("transform", d => {
//             return "scale(1)"
//         })
//     }

//     chart.states["atlas--size"] = function () {

//         chart.accelerators.selectAll(".accelerator-symbol")
//             .attr("transform", d => {
//                 return "scale(" + chart.sizeScale(d.size) + ")"
//             })
//     }

//     chart.states["atlas--energy"] = function () {

//         chart.accelerators.selectAll(".accelerator-symbol")
//             .attr("transform", d => {
//                 return "scale(" + chart.energyScale(d.max_energy) + ")"
//             })

//     }


//     initChart();
//     goto(1)



//     return {
//         goto
//     }
// }

// exports.createAtlasStage = createAtlasStage;