"use strict";

const d3 = require("d3")

const InternationalFacilities = require("./model__international-facilities.js").InternationalFacilities


const colors = {
    "ess": "#333333",
    "european-xfel": "#531796",
    "fair": "#dd5519",
    "cern": "#c53228",
    "esrf": "#0053a1",
    "sesame": "#0098a0",

}

export class AcceleratorsLinkingNations {


    _updateSVGDimensions() {
        this.width = d3.select(this._targetSelector).node().getBoundingClientRect().width;
        this.height = d3.select(this._targetSelector).node().getBoundingClientRect().height;

        this._svgElement
            .attr("width", this.width)
            .attr("height", this.height)
    }

    _createSVGLayers(layerNames) {

        for (let layerName of layerNames) {
            this._svgElement
                .append("g")
                .classed(layerName, true);
        }
    }


    _initProjections() {

        let x = this.width * .75
        let y = this.height * .5
        let alpha = -30
        let beta = -10
        let scale = 500

        this._projection = d3.geoOrthographic()
            .translate([x, y])
            .clipAngle(90)
            .rotate([alpha, beta])
            .scale(scale);

        this._sky_projection = d3.geoOrthographic()
            .translate([x, y])
            .clipAngle(90)
            .rotate([alpha, beta])
            .scale(scale + 180);
    }

    // _updateProjections() {

    //     let x = this.width * .5 + .2 * this.width * (.5 - Math.random())
    //     let y = this.height * .9 + .2 * this.height * (.5 - Math.random())
    //     let alpha = -30 + 10 * (.5 - Math.random())
    //     let beta = -10 + 10 * (.5 - Math.random())
    //     let scale = 1000 + 10 * (.5 - Math.random())


    //     let projection0 = this._projection
    //     let width = this.width
    //     let height = this.height

    //     this._projection = d3.geoOrthographic()
    //         .translate([x, y])
    //         .clipAngle(90)
    //         .rotate([alpha, beta])
    //         .scale(scale);

    //     this._sky_projection = d3.geoOrthographic()
    //         .translate([x, y])
    //         .clipAngle(90)
    //         .rotate([alpha, beta])
    //         .scale(scale + 80);

    //     // this._svgElement.selectAll("path")
    //     //     .transition()
    //     //     .duration(750)
    //     //     .attrTween("d", projectionTween(projection0, this._projection));


    //     // function projectionTween(projection0, projection1 ) {
    //     //     return function (d) {
    //     //         var t = 0;

    //     //         var projection = d3.geoOrthographic(project)
    //     //             .scale(1)
    //     //             .translate([width / 2, height / 2]);

    //     //         var path = d3.geoPath()
    //     //             .projection(projection);

    //     //         function project(λ, φ) {
    //     //             λ *= 180 / Math.PI, φ *= 180 / Math.PI;
    //     //             var p0 = projection0([λ, φ]),
    //     //                 p1 = projection1([λ, φ]);
    //     //             return [(1 - t) * p0[0] + t * p1[0], (1 - t) * -p0[1] + t * -p1[1]];
    //     //         }

    //     //         return function (_) {
    //     //             t = _;
    //     //             return path(d);
    //     //         };
    //     //     };
    //     // }
    // }




    constructor(targetSelector) {


        this._model = new InternationalFacilities();

        this._targetSelector = targetSelector;
        this._svgElement = d3.select(this._targetSelector).append("svg");
        this._createSVGLayers(["globe-layer", "facilities-layer", "member-links-layer", "member-link-shadow-layer"])
        this._facilityLabelsWrapper =
            d3.select("body .l-linking-nations__map")
            .append("div")
            .classed("facility-labels", true);


        this._updateSVGDimensions();


        this._initProjections();
        this._pathGenerator = d3.geoPath().projection(this._projection).pointRadius(0);
        this.graticule = d3.geoGraticule();
        this._swooshGenerator = d3.line()
            .x(function (d) {
                return d[0]
            })
            .y(function (d) {
                return d[1]
            })
            .curve(d3.curveCardinal.tension(0));




        this._initSVG();



        var i = 3;
        let loop = () => {
            this.selectFacility(this._model.facilities[i].name);
            i++
            if (i >= this._model.facilities.length)
                i = 0
        }

        // d3.interval(loop, 3000)
        loop()



        let that = this;

        window.addEventListener("keydown", (event) => {

            let keyCode = event.keyCode

            if (keyCode == 49)
                that.selectFacility(this._model.facilities[0].name)
            if (keyCode == 50)
                that.selectFacility(this._model.facilities[1].name)
            if (keyCode == 51)
                that.selectFacility(this._model.facilities[2].name)
            if (keyCode == 52)
                that.selectFacility(this._model.facilities[3].name)
            if (keyCode == 53)
                that.selectFacility(this._model.facilities[4].name)
            if (keyCode == 54)
                that.selectFacility(this._model.facilities[5].name)

            console.log(keyCode)

        }, false)


    }

    selectFacility(facilityName) {

        let facility = this._model.selectFacility(facilityName)
        console.log(facilityName)
        console.log(facility.countriesGeometryCollection)

        this._model.facilities.forEach(f => {

            d3.select("body").classed("facility--" + f.name, f.name === facilityName);
        })



        this._updateSVG();

        // this._projection.fitExtent([
        //     [this.width / 2, 0],
        //     [this.width, this.height]
        // ], facility.countriesGeometryCollection)

        // this._sky_projection.fitExtent([
        //         [this.width / 2, 0],
        //         [this.width, this.height]
        //     ],
        //     facility.countriesGeometryCollection)

        // this._projection.scale(500)
        // this._sky_projection.scale(this._projection.scale() + 50)

        let centroid = d3.geoCentroid(facility.countriesGeometryCollection);
        let coords = [-centroid[0], -centroid[1]]
        // coords = [-facility.coordinates[0] + 5, -facility.coordinates[1] + 5]
        // coords = []

        // console.log(JSON.stringify(d3.geoBounds(facility.countriesGeometryCollection)))


        let that = this;


        d3.transition()
            .duration(500)
            .tween('rotate', function () {
                var r = d3.interpolate(that._projection.rotate(), coords);
                return function (t) {
                    that._projection.rotate(r(t));
                    that._sky_projection.rotate(r(t));
                    that._updateProjectedPaths()
                };
            })
            .transition();


    }

    // selectAllFacilities() {

    //     this._model.selectAllFacilities()
    //     this._updateSVG();
    //     this._updateProjectedPaths()
    // }







    _initSVG() {

        this._svgElement.select(".globe-layer").append("path")
            .datum(this.graticule)
            .classed("graticule", true)
            .classed("path-generated", true)
            .attr("d", this._pathGenerator);


        this._svgElement.select(".globe-layer").append("path")
            .datum(this._model.borders)
            .classed("border", true)
            .classed("path-generated", true)
            .attr("d", this._pathGenerator);

        this._svgElement.select(".globe-layer").append("path")
            .datum(this._model.land)
            .classed("land", true)
            .classed("path-generated", true)
            .attr("d", this._pathGenerator);


        let _facilitiesSelection = this._svgElement.select(".facilities-layer")
            .selectAll("g.facility")
            .data(this._model.facilities);


        _facilitiesSelection
            .enter()
            .append("g")
            .attr("class", function (d) {
                return "facility " + "facility--" + d.name;
            });


        // this._memberLinkShadowPaths = this._memberLinkShadowsLayer
        //     .selectAll("path.is-member-of-shadow")
        //     .data(this._model.links);

        // this._memberLinkShadowPaths
        //     .enter()
        //     .append("path")
        //     .attr("class", d => {
        //         return "is-member-of-shadow facility--" + d.facility
        //     })

    }

    _updateSVG() {

        let facility = this._model.selectedFacilities[0]

        console.log(facility)


        let facilityLabels = this._svgElement.select(".facility-labels")
            .selectAll(".facility-label")
            .data(this._model.selectedFacilities, function (d) {
                return d;
            });

        facilityLabels
            .exit()
            .remove();

        facilityLabels
            .enter()
            .append('div')
            .attr("id", d => {
                return "facility-label--" + d.name
            })
            .classed("facility-label", true)
            .html(d => d.title);


        let globeLayer = this
            ._svgElement.select(".globe-layer")

        let _memberCountries =
            globeLayer
            .selectAll("path.member-country")
            .data(this._model.selectedFacilities[0].countriesAsFeatures);


        // _memberCountries
        //     .enter()


        // let _memberCountriesPaths = _memberCountriesEntered
        //     .selectAll("path.country")
        //     .data(d => d.countriesAsFeatures)

        _memberCountries
            .exit()
            .remove()


        _memberCountries
            .enter()
            .append("path")
            .classed("member-country path-generated", true)
            .attr("d", this._pathGenerator).attr("id", d => {
                return "id" + d.id
            })



        _memberCountries

            .transition()
            .duration(1000)
            .style("fill", d => {
                return colors[facility.name]
            })




        let memberLinkPathsSelection = this._svgElement.select(".member-links-layer")
            .selectAll("path.is-member-of")
            .data(facility.countryLinks);

        memberLinkPathsSelection
            .enter()
            .append("path")
            .attr("class", d => {
                return "is-member-of facility--" + d.facility
            })


        memberLinkPathsSelection
            .transition()
            .duration(500)
            .style('opacity', 1)

        memberLinkPathsSelection
            .exit()
            .transition()
            .duration(1500)
            .style('opacity', 0)
            .remove();



        // // d3.selectAll("path.country")
        // //     .classed("active", d => {
        // //         return (this._model.selectedCountries.has(d.id));
        // //     })

        // d3.selectAll("path.is-member-of")
        //     .classed("active",
        //         d => {
        //             true; //return this._model.selectedFacility.has(d.facility);
        //         })

    }


    // updateElements() {

    //     let objectReference = this;



    //     // FACILITIES


    //     // var facilityCircles = this._facilitiesLayer
    //     //     .selectAll("path.facility-circle")
    //     //     // .data(function (d) {
    //     //     //     return d;
    //     //     // });

    //     // facilityCircles
    //     //     .enter()
    //     //     .append("path")
    //     //     .attr("class", function (d) {
    //     //         return "facility-circle";
    //     //     })
    //     //     .attr("d", objectReference.pathGenerator);

    //     // this.facilityLabelConnectors =
    //     //     this._facilitiesLayer
    //     //     .selectAll("line.facility-label-connector")
    //     //     .data(function (d) {
    //     //         return d;
    //     //     })

    //     // objectReference.facilityLabelConnectors
    //     //     .enter().append("line")
    //     //     .attr("class", function (d) {
    //     //         return "facility-label-connector";
    //     //     });

    //     // objectReference.facilityLabelConnectors.exit().remove();




    //     // FACILITY LINKS

    //     this.memberLinkShadowsGroupGroups = this.memberLinkShadowsGroup
    //         .selectAll("g.member-link-shadow-group").data(objectReference.facilities);


    //     this.memberLinkShadows = this.memberLinkShadowsGroupGroups
    //         .enter()
    //         .append("g")
    //         .attr("class", function (d) {
    //             return "member-link-shadow-group " + "member-link-shadow-group--" + d.name;
    //         })
    //         .selectAll("path")
    //         .data(function (d) {
    //             return objectReference.arcLines.filter(e => e.facility === d.name);
    //         });

    //     this.memberLinkShadows
    //         .enter()
    //         .append("path")
    //         .attr("class", function (d) {
    //             // console.log(selectedFacilities, d.facility);
    //             return "is-member-link-shadow " + (objectReference.selectedFacilities.has(d.facility) ? "is-member-link-shadow--active" : "");
    //         })
    //         .attr("d", objectReference.pathGenerator)
    //     this.memberLinkShadows
    //         .exit().remove();


    //     this.memberLinks.exit().remove();

    //     this.update();
    // };





    _updateProjectedPaths() {

        let that = this;


        this._svgElement.selectAll("path.path-generated")
            .attr("d", this._pathGenerator);

        d3.selectAll(".facility-label")
            .attr("style", d => {
                return 'left: ' + (this._projection(d.coordinates)[0] + 0) + 'px; top: ' + (this._projection(d.coordinates)[1] + d.labelOffsetY) + 'px;';
            })

        d3.selectAll("path.is-member-of")
            .attr("d", d => {
                return this._swooshGenerator(flying_arc(d, this._projection, this._sky_projection))
            })



        function flying_arc(pts, projection, projection_sky) {

            var source = pts.source,
                target = pts.target;

            var mid = location_along_arc(source, target, .5);
            var result = [projection(source),
                projection_sky(mid),
                projection(target)
            ]
            return result;
        }


        function location_along_arc(start, end, loc) {
            var interpolator = d3.geoInterpolate(start, end);
            return interpolator(loc)
        }


        // objectReference.facilityLabelConnectors
        //     .attr("x1", function (d) {
        //         return objectReference.projection(d.coordinates)[0];
        //     })
        //     .attr("y1", function (d) {
        //         return objectReference.projection(d.coordinates)[1] + d.labelOffsetY;
        //     })
        //     .attr("x2", function (d) {
        //         return objectReference.projection(d.coordinates)[0];
        //     })
        //     .attr("y2", function (d) {
        //         return objectReference.projection(d.coordinates)[1];
        //     })


        // ;

        // this._svgElement.selectAll(".is-member-link-shadow").attr("d", this._pathGenerator)
        //     .attr("opacity", function (d) {
        //         // return fade_at_edge(d)
        //     })
        //     .attr("class", (d) => {
        //         return "is-member-link-shadow " + (this.selectedFacilities.has(d.facility) ? "is-member-link-shadow--active" : "");
        //     });


    }

    // function fade_at_edge(d) {
    //     var centerPos = objectReference.projection.invert([objectReference.width / 2, objectReference.height / 2]),
    //         arc = d3.geoInterpolate(),
    //         start, end;
    //     // function is called on 2 different data structures..
    //     if (d.source) {
    //         start = d.source,
    //             end = d.target;
    //     } else {
    //         start = d.geometry.coordinates[0];
    //         end = d.geometry.coordinates[1];
    //     }

    //     var start_dist = 1.57 - arc.distance({
    //             source: start,
    //             target: centerPos
    //         }),
    //         end_dist = 1.57 - arc.distance({
    //             source: end,
    //             target: centerPos
    //         });

    //     var fade = d3.linear().domain([-.1, 0]).range([0, .1])
    //     var dist = start_dist < end_dist ? start_dist : end_dist;

    //     return fade(dist)
    // }

    sizeChange() {
        this.width = d3.select("#container").node().getBoundingClientRect().width;
        d3.select("g").attr("transform", "scale(" + (width / 900) + ")");
        this._svgElement.attr("height", width * 0.618);
    }


    // // modified from http://bl.ocks.org/1392560
    // var m0, o0;

    // function mousedown() {
    //     m0 = [d3.event.pageX, d3.event.pageY];
    //     o0 = proj.rotate();
    //     d3.event.preventDefault();
    // }

    // function mousemove() {
    //     if (m0) {
    //         var m1 = [d3.event.pageX, d3.event.pageY],
    //             o1 = [o0[0] + (m1[0] - m0[0]) / 6, o0[1] + (m0[1] - m1[1]) / 6];
    //         o1[1] = o1[1] > 45 ? 45 :
    //             o1[1] < -45 ? -45 :
    //             o1[1];
    //         proj.rotate(o1);
    //         sky.rotate(o1);
    //         update();
    //     }
    // }

    // function mouseup() {
    //     if (m0) {
    //         mousemove();
    //         m0 = null;
    //     }
    // };






}