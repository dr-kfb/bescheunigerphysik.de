"use strict";

const d3 = require("d3")
const topojson = require("topojson")

import world_110m from "../../../data/maps/world-110m.json";


export class InternationalFacilities {

    constructor() {

        let that = this

        this.land = topojson.feature(world_110m, world_110m.objects.land)
        this.countries = topojson.feature(world_110m, world_110m.objects.countries).features;
        this.borders = topojson.mesh(world_110m, world_110m.objects.countries, function (a, b) {
            return a !== b;
        });
        this.capitals = this.getCapitals();

        this.capitalsDictionary = {};
        this.capitals.objects.capitals.geometries.forEach((d) => {
            this.capitalsDictionary[d.properties.ADM0_A3] = d;
        });


        this.facilities = this.getFacilities();

        this.facilities.forEach(f => {

            f.countriesAsFeatures = that.countries.filter(d => {
                return f.countries.indexOf(d.id) > -1
            }).map(c => {
                let d = JSON.parse(JSON.stringify(c))
                d.id = f.name + "-" + c.id;
                return d
            });




            f.countryLinks = [];
            f.countries.forEach((c) => {
                var b = that.capitalsDictionary[c];
                if (b) {
                    f.countryLinks.push({
                        source: f.coordinates,
                        target: b.coordinates,
                        facility: f.name
                    });
                }
            });

            f.countriesGeometryCollection = {};
            f.countriesGeometryCollection.type = "GeometryCollection";

            // filter for France to get rid of French Guiana
            f.countriesGeometryCollection.geometries = f.countriesAsFeatures.map(c => {

                if (c.id.indexOf("-FRA") > -1) {
                    
                    let geometry = JSON.parse(JSON.stringify(c.geometry))
                    geometry.coordinates = [geometry.coordinates[2]]
                    console.log(c.geometry, geometry)
                    return geometry
                }

                return c.geometry
            });

        })

        this.facilitiesMap = new Map();
        this.facilities.forEach(d => {
            this.facilitiesMap.set(d.name, d)
        })

        this.facilities.forEach(function (d) {
            d.geometry = d3.geoCircle().precision([.25]).center(d.coordinates).apply();
        })

    }

    // linksAsLineFeatures() {

    //     let linksAsLineFeatures = []

    //     this.links.forEach(function (e, i, a) {
    //         let feature = {
    //             "type": "Feature",
    //             "geometry": {
    //                 "type": "LineString",
    //                 "stroke": e.stroke,
    //                 "coordinates": [e.source, e.target]
    //             },
    //             "facility": e.facility
    //         }
    //         linksAsLineFeatures.push(feature);
    //     });

    //     return linksAsLineFeatures

    // }


    selectFacility(facilityName) {

        let facility = this.facilitiesMap.get(facilityName)
        if (!facility) {
            throw "facilityName not kown: " + facilityName
        }
        this.selectedFacilities = [facility];

        return facility
    }


    selectAllFacilities() {

        this.selectedFacilities = this.facilities.map(f => f)
        this.selectedCountries = new Set(

            this
            .facilities
            .map(f =>
                f.countries
            )
            .reduce(
                (p, c) => p.concat(c), []
            )
        )
        this.selectedCountriesBounds = this.boundsForCountries(this.selectedCountries)
        console.log(this.selectedCountriesBounds)

    }


    // boundsForCountries(features) {
    //     let selectedCountriesAsFeatures = this.countries.filter(d => {
    //         return this.selectedCountries.has(d.id)
    //     });

    //     let boundsArray = selectedCountriesAsFeatures.map(d => d3.geoBounds(d))
    //     let left = d3.min(boundsArray.map(d => d[0][0]))
    //     let bottom = d3.min(boundsArray.map(d => d[0][1]))
    //     let right = d3.max(boundsArray.map(d => d[1][0]))
    //     let top = d3.max(boundsArray.map(d => d[1][1]))

    //     let result = [
    //         [left, bottom],
    //         [right, top]
    //     ]
    //     return result
    // }






    getFacilities() {
        return [{
                "type": "Feature",
                name: 'cern',
                title: 'CERN',
                color: '#9e1c20',
                countries: ['BEL', 'BGR', 'DNK', 'DEU', 'FIN', 'FRA', 'GRC', 'ISR', 'ITA', 'NLD', 'NOR', 'AUT', 'POL', 'PRT', 'SWE', 'CHE', 'ESP', 'CZE', 'HUN', 'GBR'],
                coordinates: [6.049167, 46.233333],
                countries_bounds: [
                    [-10, 2.0536768523685254],
                    [35.83655836558367, 80.65666742807429]
                ],

                labelOffsetY: -20
            }, {
                "type": "Feature",
                name: 'esrf',
                title: 'ESRF',
                color: '#0053a1',
                countries: ['BEL', 'DNK', 'DEU', 'FIN', 'FRA', 'GBR', 'ITA', 'NLD', 'RUS', 'SWE', 'CHE', 'ESP', /* assoicates --> */ 'ISR', 'AUT', 'POL', 'PRT', 'SVK', 'ZAF', 'CZE', 'HUN'],
                coordinates: [5.726389, 45.186944],
                labelOffsetY: 0,
                labelOffsetX: -50,
                countries_bounds: [
                    [-180, -34.81849877358774],
                    [180, 81.2505397113971]
                ]

            }, {
                "type": "Feature",
                name: 'ess',
                title: 'ESS',
                color: '#0095cd',
                countries: ['CZE', 'DNK', 'EST', 'FRA', 'DEU', 'HUN', 'ITA', 'NOR', 'POL', 'SWE', 'CHE', /* intend to participate as of 2016 */ 'BEL', 'NLD', 'ESP', 'GBR'],
                coordinates: [13, 55],
                labelOffsetY: -30,
                countries_bounds: [
                    [-54.52434524345243, 2.0536768523685254],
                    [31.29331293312933, 80.65666742807429]
                ]


            }, {
                "type": "Feature",
                name: 'european-xfel',
                title: 'European XFEL',
                color: '#531796',
                countries: ['DNK', 'DEU', 'FRA', 'ITA', 'POL', 'RUS', 'SWE', 'CHE', 'SVK', 'ESP', 'HUN'],
                coordinates: [9.829444, 53.588611],
                labelOffsetY: 0,
                countries_bounds: [
                    [-180, 2.0536768523685254],
                    [180, 81.2505397113971]
                ],
            }, {
                "type": "Feature",
                name: 'fair',
                title: 'FAIR',
                color: '#f77c21',
                countries: ['DEU', 'FIN', 'FRA', 'IND', 'POL', 'ROU', 'RUS', 'SWE', 'SVN', 'GBR'],
                coordinates: [8, 50],
                labelOffsetY: 0,
                countries_bounds: [
                    [-180, 2.0536768523685254],
                    [180, 81.2505397113971]
                ],
            },


            {
                "type": "Feature",
                name: 'sesame',
                title: 'SESAME',
                color: '#0098a0',
                countries: ['EGY', 'BHR', 'IRN', 'ISR', 'JOR', 'PAK', 'PSE', 'TUR', 'CYP'],
                coordinates: [36, 32],
                labelOffsetY: 0,
                countries_bounds: [
                    [24.70164701647016, 22.000492403624037],
                    [77.83817838178385, 42.141792445324455]
                ],
            }
        ];

    }

    getCapitals() {

        return {
            "type": "Topology",
            "objects": {
                "capitals": {
                    "type": "GeometryCollection",
                    "geometries": [{
                        "type": "Point",
                        "properties": {
                            "name": "Ljubljana",
                            "ADM0_A3": "SVN"
                        },
                        "coordinates": [14.51496903347413, 46.0552883087945]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Bratislava",
                            "ADM0_A3": "SVK"
                        },
                        "coordinates": [17.11698075223461, 48.15001832996171]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Bern",
                            "ADM0_A3": "CHE"
                        },
                        "coordinates": [7.466975462482424, 46.91668275866772]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Manama",
                            "ADM0_A3": "BHR"
                        },
                        "coordinates": [50.58305171591019, 26.236136290485945]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Islamabad",
                            "ADM0_A3": "PAK"
                        },
                        "coordinates": [73.16468862105955, 33.70194180895959]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Bloemfontein",
                            "ADM0_A3": "ZAF"
                        },
                        "coordinates": [26.22991288117737, -29.119993877378704]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Pretoria",
                            "ADM0_A3": "ZAF"
                        },
                        "coordinates": [28.22748321723384, -25.704974695184433]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Amman",
                            "ADM0_A3": "JOR"
                        },
                        "coordinates": [35.93135406687412, 31.951971105827454]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Tallinn",
                            "ADM0_A3": "EST"
                        },
                        "coordinates": [24.72804072947855, 59.43387737948592]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Sofia",
                            "ADM0_A3": "BGR"
                        },
                        "coordinates": [23.314708152110086, 42.68529528393054]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Jerusalem",
                            "ADM0_A3": "ISR"
                        },
                        "coordinates": [35.20662593459866, 31.778407815573303]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Nicosia",
                            "ADM0_A3": "CYP"
                        },
                        "coordinates": [33.36663488641415, 35.166676451654496]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Ankara",
                            "ADM0_A3": "TUR"
                        },
                        "coordinates": [32.862445782356644, 39.929184444075474]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Budapest",
                            "ADM0_A3": "HUN"
                        },
                        "coordinates": [19.081374818759684, 47.50195218499135]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Bucharest",
                            "ADM0_A3": "ROU"
                        },
                        "coordinates": [26.0980007953504, 44.43531766349457]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Lisbon",
                            "ADM0_A3": "PRT"
                        },
                        "coordinates": [-9.14681216410213, 38.72466873648784]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Oslo",
                            "ADM0_A3": "NOR"
                        },
                        "coordinates": [10.748033347372314, 59.91863614500187]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Warsaw",
                            "ADM0_A3": "POL"
                        },
                        "coordinates": [20.998053692465305, 52.25194648839556]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Prague",
                            "ADM0_A3": "CZE"
                        },
                        "coordinates": [14.464033917048539, 50.08528287347832]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Helsinki",
                            "ADM0_A3": "FIN"
                        },
                        "coordinates": [24.93218048284558, 60.17750923256807]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Kobenhavn",
                            "ADM0_A3": "DNK"
                        },
                        "coordinates": [12.561539888703294, 55.68051004902594]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Brussels",
                            "ADM0_A3": "BEL"
                        },
                        "coordinates": [4.33137074969045, 50.83526293533032]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Madrid",
                            "ADM0_A3": "ESP"
                        },
                        "coordinates": [-3.685297544612524, 40.40197212311381]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Stockholm",
                            "ADM0_A3": "SWE"
                        },
                        "coordinates": [18.095388874180912, 59.35270581286585]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Johannesburg",
                            "ADM0_A3": "ZAF"
                        },
                        "coordinates": [28.028063865019476, -26.16809888138414]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Amsterdam",
                            "ADM0_A3": "NLD"
                        },
                        "coordinates": [4.914694317400972, 52.35191454666443]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Berlin",
                            "ADM0_A3": "DEU"
                        },
                        "coordinates": [13.399602764700546, 52.523764522251156]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "New Delhi",
                            "ADM0_A3": "IND"
                        },
                        "coordinates": [77.19998002005303, 28.600023009245433]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Athens",
                            "ADM0_A3": "GRC"
                        },
                        "coordinates": [23.731375225679358, 37.98527209055226]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Tehran",
                            "ADM0_A3": "IRN"
                        },
                        "coordinates": [51.42239817500899, 35.673888627001304]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Vienna",
                            "ADM0_A3": "AUT"
                        },
                        "coordinates": [16.364693096743736, 48.20196113681686]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "London",
                            "ADM0_A3": "GBR"
                        },
                        "coordinates": [-0.118667702475932, 51.5019405883275]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Cape Town",
                            "ADM0_A3": "ZAF"
                        },
                        "coordinates": [18.43304229922603, -33.91806510862875]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Moscow",
                            "ADM0_A3": "RUS"
                        },
                        "coordinates": [37.6135769672714, 55.75410998124818]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Rome",
                            "ADM0_A3": "ITA"
                        },
                        "coordinates": [12.481312562873995, 41.89790148509894]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Cairo",
                            "ADM0_A3": "EGY"
                        },
                        "coordinates": [31.248022361126118, 30.051906205103705]
                    }, {
                        "type": "Point",
                        "properties": {
                            "name": "Paris",
                            "ADM0_A3": "FRA"
                        },
                        "coordinates": [2.33138946713035, 48.86863878981461]
                    }]
                }
            },
            "arcs": []
        };

    }


}