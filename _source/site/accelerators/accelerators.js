import {
    createWhatWherebyWhereforeChart
} from "./what-whereby-wherefore/accelerators__what-whereby-wherefore.js"

// import {
//     createChart
// } from "./accelerators__story-stage.js"


import {
    createAtlasChart
} from "./atlas/accelerators__atlas.js"



import {
    BabylonCreator
} from "./babylon/accelerators__babylon.js"

import {
    createOperatingPrincipleRemote
} from "./operating-principle/accelerators__operating-principle.js"



import {
    graphScroll
} from "./accelerators__story-scroller.js"

module.exports = {

    init: function (name) {

        let chart;

        if (name === "accelerators__what-whereby-wherefore") {

            // chart = createWhatWherebyWhereforeChart("#accelerators__chart--what-whereby-wherefore");
            createScroller()
        }
        if (name === "accelerators__atlas") {

            chart = createAtlasChart("#accelerators__chart--atlas");
            createScroller()
        }
        if (name === "accelerators__operating-principle") {
            chart = createOperatingPrincipleRemote("accelerators__operating-principle-iframe")
            
            createScroller()
        }


        if (name === "accelerators__babylon") {

            let cfreatzor = new BabylonCreator();
            cfreatzor.createAcceleratorsComponents();

        }


        function createScroller(name) {
            return graphScroll()
                .container(d3.select('section.accelerators__page-section'))
                .graph(d3.selectAll('.accelerators__chart'))
                .sections(d3.selectAll('section section'))
                .on('active', function (stepId) {

                    // console.log(stepId)
                    if (chart && chart.goto)
                        chart.goto(stepId)
                })
        }


    },

    // initStory: function () {

    //     // let stages = new Map();
    //     // let names = ["operating-principle", "linking-nations", "atlas"];

    //     // let name = "what-whereby-wherefore";
    //     // stages.set(name, createStageWhatWherebyWherefore("#accelerators__story-stage--" + "what-whereby-wherefore"));
    //     // createScroller(name);

    //     // name = "operating-principle";
    //     // stages.set(name, createStage("#accelerators__story-stage--" + name));
    //     // createScroller(name);

    //     // name = "linking-nations";
    //     // stages.set(name, createStage("#accelerators__story-stage--" + name));
    //     // createScroller(name);

    //     // name = "atlas";
    //     // stages.set(name, createAtlasStage("#accelerators__story-stage--" + name));
    //     // createScroller(name);




    // }

}