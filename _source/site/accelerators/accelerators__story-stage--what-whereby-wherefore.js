'use strict';

var d3 = require("d3")


function createStageWhatWherebyWherefore(id) {

    let svgDimensions = {};
    let parentId = id;

    function getDimensions(parentId) {

        let bbbox = d3.select(parentId).node().getBoundingClientRect()
        return {
            height: bbbox.height,
            width: bbbox.width
        }
    }

    svgDimensions = getDimensions(parentId);

    let svg = d3.select(id + " svg")
        .attr("height", svgDimensions.height)
        .attr("width", svgDimensions.width)


    svg.append("text")
        .attr("x", svgDimensions.height * 0.75)
        .attr("y", 200)
        .text(parentId)


    function goto(stepId) {
        svg.select(".stepId")
            .remove();
        svg.append("text")
            .classed("stepId", true)
            .attr("x", svgDimensions.width - 100)
            .attr("y", svgDimensions.height)
            .text(stepId)
    }

    goto(1)

    let linkColors = {};
    linkColors.ions = "rgba(197, 50, 40, .5)"
    linkColors.electrons = "rgba(33, 116, 168, .5)"
    linkColors.photons = "rgba(237, 197, 0, .5)"
    linkColors.neutrons = "rgba(153, 153, 153, .5)"


    function initChart() {

        let www_chart = {}

        function getDataLinks(data) {

            let links = []

            data.steps.forEach(step => {

                step.options.forEach(option => {

                    if (option.links) {
                        option.links.forEach((link, i) => {

                            let startOffsetScale = d3.scaleLinear()
                                .domain([0, option.links.length - 1])
                                .range([-1, 1]);

                            links.push({
                                "i": links.length,
                                "start": option.name,
                                "startOffset": startOffsetScale(i),
                                "end": link.target,
                                "particle-type": link["particle-type"]
                            })
                        })
                    }
                })
            })

            let nestedByEnd = d3.nest().key(d => d.end).map(links)
            let currentEnd = "";
            let currentSize = 0;
            let endOffsetScale;
            let i = 0;
            let j = 0;

            links.forEach(link => {
                if (link.end != currentEnd) {
                    i = 0;
                    currentEnd = link.end
                    currentSize = nestedByEnd.get(link.end).length
                    endOffsetScale = d3.scaleLinear()
                        .domain([0, currentSize - 1])
                        .range([-1, 1]);

                } else {
                    i++
                }
                link.endOffset = endOffsetScale(i)
                j++





            })


            return links
        }


        function updateOptionBoundingClientRects(www_chart) {

            www_chart.bcrs = new Map();

            www_chart.data.steps.forEach(step => {

                step.options.forEach(option => {

                    let name = "#what-whereby-whatfore__option--" + option.name

                    let optionDiv = d3.select(name + " .what-whereby-whatfore__option-title");
                    let bcr = optionDiv.node().getBoundingClientRect();

                    www_chart.bcrs.set(name, bcr)

                })

            })

        }

        function processData(data) {


            // read all links

            www_chart.links = getDataLinks(data)




            www_chart.data = data

            www_chart.width = 2000
            www_chart.height = 1000


            www_chart.chartCanvasContext = d3.select(".what-whereby-whatfore__chart")
                .append("canvas")
                .attr('width', www_chart.width + "px")
                .attr('height', www_chart.height + "px")
                .node().getContext('2d');

            www_chart.chart = d3.select(".what-whereby-whatfore__chart")
                .append("ol")
                .classed("what-whereby-whatfore__steps", true)


            www_chart.chartCanvasContext.clearRect(0, 0, www_chart.width, www_chart.height);
            // www_chart.chartCanvasContext.rect(10, 10, 100, 100);

            www_chart.chartCanvasContext.fill()

            let steps = www_chart.chart.selectAll(".what-whereby-whatfore__step")
                .data(data.steps)
                .enter()
                .append("li")
                .classed("what-whereby-whatfore__step", true);

            let stepsInfo =  steps
                .append("div")
                .classed("what-whereby-whatfore__step-info", true)

            stepsInfo
                .append("div")
                .classed("what-whereby-whatfore__step-title", true)
                .html(step => step.title)
            stepsInfo
                .append("div")
                .classed("what-whereby-whatfore__step-description", true)
                .html(step => "<p>" + step.description + "</p>")

            let options = steps
                .append("ul")
                .classed("what-whereby-whatfore__options", true)
                .selectAll(".what-whereby-whatfore__option")
                .data(d => d.options)
                .enter()
                .append("li")
                .classed("what-whereby-whatfore__option", true)
                .attr("id", d => {
                    return "what-whereby-whatfore__option--" + d.name
                });


            options.on("click", function () {
                let element = d3.select(this);
                let isVerbose = element.classed("what-whereby-whatfore__option--verbose")

                element.classed("what-whereby-whatfore__option--verbose", !isVerbose)
                updateChart()

            })

            options.append("div")
                .classed("what-whereby-whatfore__option-title", true)
                .html(option => option.title)

            options
                .append("div")
                .classed("what-whereby-whatfore__option-description", true)
                .html(option => option.description)


            // www_chart.chartCanvasContext.beginPath();
            // www_chart.chartCanvasContext.moveTo(0, 0);
            // www_chart.chartCanvasContext.lineTo(100, 100);
            // www_chart.chartCanvasContext.stroke()

            updateChart()

             window.addEventListener("resize", updateChart);

        }

        d3.json("/_shared/data/accelerators__what_whereby_wherefore.json", function (error, data) {

            if (error) throw error;
            processData(data);

        });





        function updateChart() {
            console.log("update")
            updateOptionBoundingClientRects(www_chart)
            www_chart.canvas = {}
            www_chart.canvas.offset = {}
            www_chart.canvas.offset.x = d3.select(".what-whereby-whatfore__chart").node().getBoundingClientRect().left;
            www_chart.canvas.offset.y = d3.select(".what-whereby-whatfore__chart").node().getBoundingClientRect().top;

            www_chart.chartCanvasContext.clearRect(0, 0, www_chart.width, www_chart.height);
            www_chart.links.forEach(
                link => {
                    let sourceRect = www_chart.bcrs.get("#what-whereby-whatfore__option--" + link.start)
                    let targetRect = www_chart.bcrs.get("#what-whereby-whatfore__option--" + link.end)
                    let startPosition = {
                        x: sourceRect.right - www_chart.canvas.offset.x,
                        y: sourceRect.top - www_chart.canvas.offset.y + 0.2 * link.startOffset * sourceRect.height + sourceRect.height / 2
                    };

                    let endPosition = {
                        x: targetRect.left - www_chart.canvas.offset.x,
                        y: targetRect.top - www_chart.canvas.offset.y + sourceRect.height / 2
                    };

                    // console.log(link.startOffset, startPosition, endPosition)


                    www_chart.chartCanvasContext.strokeStyle = linkColors[link["particle-type"]];
                    www_chart.chartCanvasContext.lineWidth = 30;
                    www_chart.chartCanvasContext.beginPath();
                    www_chart.chartCanvasContext.moveTo(startPosition.x, startPosition.y);
                    www_chart.chartCanvasContext.bezierCurveTo(startPosition.x + 50, startPosition.y, endPosition.x - 50, endPosition.y, endPosition.x, endPosition.y);
                    www_chart.chartCanvasContext.stroke()
                }

            );
        }
    }
    // initChart()

    // function drawChart() {


    //     var cols = svg
    //         .selectAll("g.column")
    //         .data(data)
    //         .enter()
    //         .append("g")
    //         // .attr("transform", function(d, j) {
    //         //     return "translate(" + (j * nodeWidth + (j) * columnGutter) + "," + (1 * nodeHeight) + ")";
    //         // })
    //         .attr("class", "column");

    //     var column_label = cols
    //         .append("text")
    //         .text(function (d, i) {
    //             return d.title
    //         })
    //         .attr("class", "level-1-label")
    //         .attr("x", function (d) {
    //             return d.left;
    //         })
    //         .attr("y", function (d, i) {
    //             return 650;
    //         });

    //     var nodes_container = cols
    //         .append("g")
    //         .attr("class", "nodes-container");

    //     var nodes = nodes_container
    //         .selectAll("g.node")
    //         .data(function (d, i) {
    //             return d.children;
    //         })
    //         .enter()
    //         .append("g")
    //         .attr("class", function (d) {
    //             if (d.active) {
    //                 return "node";
    //             } else {
    //                 return "node--inactive";
    //             }
    //         })
    //         .attr("id", function (d, i) {
    //             return "node-" + d.id;
    //         });

    //     var node_borders = nodes
    //         .append("rect")
    //         .attr("class", "level-2-border")
    //         .attr("id", function (d, i) {
    //             return "node-border--" + d.id;
    //         })
    //         .attr("width", nodeWidth)
    //         .attr("height", nodeHeight)
    //         .attr("x", function (d, i) {
    //             return d.left;
    //         })
    //         .attr("y", function (d, i) {
    //             return d.top;
    //         })
    //         .on("click", function (d) {

    //             d.active = !d.active;

    //             var parent = d3.select(d3.select(this).node().parentNode);
    //             if (parent.attr("class") === "node") {
    //                 parent.attr("class", "node--inactive");

    //                 data[0].children[0].active = false;
    //                 updateLinks();

    //             } else {
    //                 parent.attr("class", "node");
    //                 updateLinks();
    //             }

    //         });

    //     var node_labels = nodes
    //         .append("text")
    //         .text(function (d, i) {
    //             return d.title
    //         })
    //         .attr("class", "node-label")
    //         .attr("x", function (d, i) {

    //             return d.left + nodePadding;
    //         })
    //         .attr("y", function (d, i) {
    //             return d.top + nodeHeight / 2;
    //         })
    //         .attr("alignment-baseline", "central");


    //     updateLinks();
    // }

    // function updateLinks() {

    //     linkPathsGroup.selectAll("path").remove();

    //     for (var i_link = 0; i_link < links.length; i_link++) {
    //         var link = links[i_link];
    //         var source = d3.select("#node-" + link.source);

    //         if (d3.select(source).node().attr("class").indexOf("inactive") === -1) {
    //             var linkPath = linkPathsGroup.append("path")
    //                 .attr("d", pathForNodeLink(link))
    //                 .attr("class", "link link--" + link.type);
    //             // linkPath.parentNode.appendChild(linkPath);
    //         }
    //     }
    // }


    // function pathForNodeLink(nodeLink) {

    //     var curvature = .4;

    //     var source = d3.select("#node-border--" + nodeLink.source);
    //     var target = d3.select("#node-border--" + nodeLink.target);

    //     // console.log(source.datum());
    //     var x0 = parseFloat(source.attr("x")) + nodeWidth,
    //         x1 = parseFloat(target.attr("x")),
    //         xi = d3.interpolateNumber(x0, x1),
    //         x2 = xi(curvature),
    //         x3 = xi(1 - curvature),
    //         y0 = parseFloat(source.attr("y")) + nodeHeight / 2 + parseFloat(nodeLink.source_offset) * 10,
    //         y1 = parseFloat(target.attr("y")) + nodeHeight / 2 + parseFloat(nodeLink.target_offset) * 10;

    //     return "M" + x0 + "," + y0 + "C" + x2 + "," + y0 + " " + x3 + "," + y1 + " " + x1 + "," + y1;
    // };




    return {
        goto
    }
}

exports.createStageWhatWherebyWherefore = createStageWhatWherebyWherefore;