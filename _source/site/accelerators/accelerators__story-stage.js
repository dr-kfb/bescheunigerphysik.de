'use strict';

var d3 = require("d3")


function createStage(id) {

    let svgDimensions = {};
    let parentId = id;

    function getDimensions(parentId) {

        let bbbox = d3.select(parentId).node().getBoundingClientRect()
        return {
            height: bbbox.height,
            width: bbbox.width
        }
    }

    svgDimensions = getDimensions(parentId);

    let svg = d3.select(id + " svg")
        .attr("height", svgDimensions.height)
        .attr("width", svgDimensions.width)


    // svg.append("rect")
    //     .attr("x", 0)
    //     .attr("y", 0)
    //     .attr("height", svgDimensions.height)
    //     .attr("width", svgDimensions.width)
    //     .attr("fill", "grey")
    //     .attr("fill-opacity", .5)

    svg.append("text")
        .attr("x", svgDimensions.height * 0.75)
        .attr("y", 200)
        .text(parentId)


    function goto(stepId) {
        svg.select(".stepId")
            .remove();
        svg.append("text")
            .classed("stepId", true)
            .attr("x", svgDimensions.height * 0.75)
            .attr("y", 300)
            .text(stepId)
    }

    goto(1)



    return {
        goto
    }
}

exports.createStage = createStage;