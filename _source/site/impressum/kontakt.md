## Kontakt

###  Anfragen zur Webpräsenz

Lob, Kritik und Verbesserungsvorschäge nehmen wir gerne über [kommunikation@beschleunigerphysik.de](mailto:kommunikation@beschleunigerphysik.de) entgegen.


###  Herausgeber

Herausgeber dieser Webpräsenz ist der Vorsitzende des Komitees für Beschleunigerphysik

**Prof. Dr. Oliver Boine-Frankenheim**<br>
Institut für Theorie Elektromagnetischer Felder<br>
Technische Universität Darmstadt<br>
Schloßgartenstr. 8, D-64289 Darmstadt<br>
boine-frankenheim@temf.tu-darmstadt.de<br>

