"use strict";

const d3 = require("d3");


export class ForumLogoInteraction {

    constructor(targetSelectorBars, targetSelectorChart) {

        this.c = 299792458;
        this.e = 1.6022 * Math.pow(10, -19);
        this.m_e_0 = 9.109382 * Math.pow(10, -31);
        this.m_p_0 = 1892 * this.m_e_0;

        this.randomTimer = null;

        this.targetSelectorBars = targetSelectorBars;
        this.targetSelectorChart = targetSelectorChart;

        this.svgForumLogo = null;
        this.svgForumLogoWidth = null;
        this.svgForumLogoHeight = null;

        this.svgChart = null;
        this.svgChartWidth = null;
        this.svgChartHeight = null;

        this.rowHeight = null;
        this.rowPadding = null;

        this.parameters = {
            E_coefficient: 1,
            E_exponent: 5,
            stepCount: 5,
            stepsPerSecond: 50000,
            log: false,
            random: false
        }

        this.border = 2;

        this.updateSVGDimensions();

        this.data = [];
    }

    updateSVGDimensions() {

        this.svgForumLogoWidth = 200; //d3.select(this.targetSelectorBars).node().getBoundingClientRect().width;
        this.svgForumLogoHeight = 50; //d3.select(this.targetSelectorBars).node().getBoundingClientRect().height;
        this.rowHeight = 10;
        this.rowPadding = 0; //(this.svgForumLogoHeight / 12) * 0;

        if (this.targetSelectorChart) {
            this.svgChartWidth = d3.select(this.targetSelectorChart).node().getBoundingClientRect().width;
            this.svgChartHeight = d3.select(this.targetSelectorChart).node().getBoundingClientRect().height;
        }

    }


    createSVG() {

        this.svgForumLogo = d3
            .select(this.targetSelectorBars)
            .append("svg")
            .attr("viewBox", "0 0 " + this.svgForumLogoWidth + " " + this.svgForumLogoHeight)
            .append("g");

        // this.svgForumLogo
        //     .selectAll("line.grid")
        //     .data(d3.range(0, 11, 1))
        //     .enter()^
        //     .append("line")
        //     .attr("class", "grid")
        //     .attr("fill-opacity", (d, i) => ((i + 1) / 12.0))
        //     .attr("x1", 0)
        //     .attr("y1", d => d * this.rowHeight)
        //     .attr("x2", this.svgForumLogoWidth)
        //     .attr("y2", d => d * this.rowHeight)


        this.svgForumLogo
            .append("image")
            .attr("xlink:href", "/_shared/images/icons/logo-text--forum.svg")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", this.svgForumLogoWidth)
            .attr("height", this.svgForumLogoHeight)


        if (this.targetSelectorChart) {
            this.svgChart = d3.select(this.targetSelectorChart).append("svg")
                .attr("width", this.svgChartWidth)
                .attr("height", this.svgChartHeight).append("g");
        }
        this.initGUI();
        this.update();

    }



    update() {
        this.updateData();
        this.updateChart();
        this.updateLogo();
    }


    getTForStep(step) {
        return step / this.parameters.stepsPerSecond;
    }

    getX(m, q, E, t) {

        let alpha = (m * this.c) / (E * q);
        let x = alpha * this.c * (Math.sqrt(1 + t * t / alpha / alpha) - 1);

        return x;
    }


    updateData() {

        let that = this;

        let E_0 = that.parameters.E_coefficient * Math.pow(10, that.parameters.E_exponent);

        let steps = d3.range(0, this.parameters.stepCount + 1, 1);

        this.data = {};
        this.data.t = steps.map(i => {
            return this.getTForStep(i)
        });
        this.data.photon = steps.map(i => {
            return that.getTForStep(i) * that.c
        });
        this.data.electron = steps.map(i => {
            return that.getX(that.m_e_0, that.e, E_0, that.getTForStep(i))
        });
        this.data.proton = steps.map(i => {
            return that.getX(that.m_p_0, that.e, E_0, that.getTForStep(i))
        });

    }



    updateChart() {
        let that = this;

        let chartPaddingX = 80;
        let chartPaddingY = 50;

        let scaleX_log = d3.scaleLog()
            .domain([that.data.proton[1], d3.max(that.data.photon)])
            .range([chartPaddingX, this.svgChartWidth - chartPaddingX]);

        let scaleX_linear = d3.scaleLinear()
            .domain(d3.extent(that.data.photon))
            .range([chartPaddingX, this.svgChartWidth - chartPaddingX]);

        let scaleY_linear = d3.scaleLinear()
            .domain(d3.extent(that.data.t))
            .range([this.svgChartHeight - chartPaddingY, chartPaddingY]);

        let x = null;
        let photonData = that.data.photon;
        let electronData = that.data.electron;
        let protonData = that.data.proton;
        if (this.parameters.log) {

            x = scaleX_log;
        } else {

            x = scaleX_linear;
        }
        photonData[0] += 0.000000000001;
        electronData[0] += 0.000000000001;
        protonData[0] += 0.000000000001;

        let y = scaleY_linear;

        this.svgChart.selectAll(".axis--x").remove();
        this.svgChart.selectAll(".axis--y").remove();

        let xAxis = d3.axisBottom(x)
            .scale(x).ticks(5).tickFormat(d3.format(".1s"));

        let xAxisGroup = this.svgChart.append("g")
            .attr("class", "axis axis--x")
            .attr("transform", "translate(0," + (this.svgChartHeight - chartPaddingY) + ")")
            .call(xAxis)
            .append("text")
            .attr("class", "axis-title")
            // .attr("transform", "rotate(-90)")
            .attr("x", chartPaddingX)
            .attr("dy", "3em")
            .style("text-anchor", "start")
            .text("Strecke in Meter");


        let yAxis = d3.axisLeft(y)
            .scale(y).ticks(5).tickFormat(d3.format(".2s"));

        this.svgChart.append("g")
            .attr("class", "axis axis--y")
            .attr("transform", "translate(" + chartPaddingX + ",0)")
            .call(yAxis)
            .append("text")
            .attr("class", "axis-title")
            // .attr("transform", "rotate(-90)")
            .attr("y", 50)
            .attr("dy", "-1em")
            .style("text-anchor", "end")
            .text("Zeit in Sekunden");


        let photonSelection = this.svgChart
            .selectAll("circle.photon")
            .data(photonData);
        photonSelection
            .enter()
            .append("circle")
            .attr("class", "photon")
            .merge(photonSelection).transition()
            .attr("cx", d => x(d))
            .attr("cy", (d, i) => y(that.data.t[i]))
            .attr("r", 4);
        photonSelection.exit().remove();

        let electronSelection = this.svgChart
            .selectAll("circle.electron")
            .data(electronData);
        electronSelection
            .enter()
            .append("circle")
            .attr("class", "electron")
            .merge(electronSelection).transition()
            .attr("cx", d => x(d))
            .attr("cy", (d, i) => y(that.data.t[i]))
            .attr("r", 4)
        electronSelection.exit().remove();

        let protonSelection = this.svgChart
            .selectAll("circle.proton")
            .data(protonData);
        protonSelection
            .enter()
            .append("circle")
            .attr("class", "proton")
            .merge(protonSelection).transition()
            .attr("cx", d => x(d))
            .attr("cy", (d, i) => y(that.data.t[i]))
            .attr("r", 4)
        protonSelection.exit().remove();

    }

    updateLogo() {


        let that = this;

        let barsPaddingX = 0;

        let scaleX_log = d3.scaleLog()
            .domain([that.data.proton[1], d3.max(that.data.photon)])
            .range([0, 0.25 * this.svgForumLogoWidth]);

        let scaleX_linear = d3.scaleLinear()
            .domain(d3.extent(that.data.photon))
            .range([0, 0.25 * this.svgForumLogoWidth]);

        let x = null;
        if (this.parameters.log) {
            x = scaleX_log;
        } else {
            x = scaleX_linear;
        }
        let mydata = []

        mydata[0] = this.data.photon.map((d, i) => {
            return [that.data.photon[i - 1], that.data.photon[i]]
        }).slice(1);
        mydata[1] = this.data.electron.map((d, i) => {
            return [that.data.electron[i - 1], that.data.electron[i]]
        }).slice(1);
        mydata[2] = this.data.proton.map((d, i) => {
            return [that.data.proton[i - 1], that.data.proton[i]]
        }).slice(1);


        if (this.parameters.log) {
            mydata[0][0][0] = that.data.proton[1];
            mydata[1][0][0] = that.data.proton[1];
            mydata[2][0][0] = that.data.proton[1];
        }


        let typeGroupSelection = this.svgForumLogo
            .selectAll("g")
            .data(mydata)



        let types = ["photon", "electron", "proton"];

        typeGroupSelection.exit().remove();

        let typeGroups = typeGroupSelection.enter()
            .append("g")
            .merge(typeGroupSelection)
            .attr("class", (d, i) => {
                return types[i] + "s";
            })
            .attr("transform", (d, i) => {
                return "translate(" + (this.svgForumLogoWidth * 0.5 + 5) + ", " + (((i + 1) * (this.rowHeight + this.rowPadding))) + ")";
            });

        let stepSelection = typeGroups
            .selectAll("rect")
            .data(function(d) {
                return d;
            }, function(d, i) {
                return i;
            })

        stepSelection.enter()
            .append("rect")
            .attr("height", d => {
                return this.rowHeight
            })
            .merge(stepSelection).transition()
            .attr("x", (d, i) => {
                return x(d[0]);
            })
            .attr("y", 0)
            .attr("width", function(d, i) {
                let result;
                result = x(d[1]) - x(d[0]) - that.border * 0

                if (result < 0)
                    result = 0
                return result
            });

        stepSelection.exit().remove();


    }




    initGUI() {

        let that = this;

        this.gui = new dat.GUI( { autoPlace: false } );
        var customContainer = d3.select('.page__header__gui-dat').node()
            .append(d3.select(this.gui.domElement).node());


        // gui.remember(this.parameters);

        this.controller_E_coefficient = this.gui.add(that.parameters, 'E_coefficient').min(1).max(9).step(1).listen();
        this.controller_E_exponent = this.gui.add(that.parameters, 'E_exponent').min(0).max(10).step(1).listen();

        this.controller_stepCount = this.gui.add(that.parameters, 'stepCount').min(2).max(10).step(1).listen();
        this.controller__stepsPerSecond = this.gui.add(that.parameters, 'stepsPerSecond', 1000, 100000);

        this.controller__log = this.gui.add(that.parameters, 'log');
        this.controller__random = this.gui.add(that.parameters, 'random');

        this.controller_E_coefficient.onChange(function(value) {
            that.update();
        });
        this.controller_E_exponent.onChange(function(value) {
            that.update();
        });

        this.controller_stepCount.onChange(function(value) {
            that.update();
        });
        this.controller__stepsPerSecond.onChange(function(value) {
            that.update();
        });

        this.controller__log.onChange(function(value) {
            that.update();
        });
        this.controller__random.onChange(function(value) {
            if (value) {
                that.randomTimer = setInterval(() => {
                    that.parameters.E_coefficient = Math.floor(Math.random() * 9 + 1);
                    that.parameters.E_exponent = Math.floor(Math.random() * 5 + 1);

                    that.updateData();
                    that.updateChart();
                    that.updateLogo();
                }, 500);


            } else {
                clearInterval(that.randomTimer);
            }

        });



    }

}
