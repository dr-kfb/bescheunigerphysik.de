"use strict";

import {
    ForumLogoInteraction
} from "./forum-logo-interaction.js"


function ForumLogoRunner() {

    let interaction = new ForumLogoInteraction(".logo-full", ".forum-logo-chart");
    interaction.createSVG();


}

module.exports.ForumLogoRunner = ForumLogoRunner;