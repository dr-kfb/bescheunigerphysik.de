var d3 = require('d3');

export class ForumRegistrationForm {

    constructor(formSelector) {
        console.log("init")
        d3.select(formSelector)
            .on('submit', function (d) {
                d3.select("input.submit").classed("submitted", true)
                let evt = d3.event
                evt.preventDefault()
                let formData = {};
                d3.selectAll("input, select")["_groups"][0].forEach((d, i) => {
                    if (d3.select(d).node().type && d3.select(d).node().type === "checkbox")
                        formData[d3.select(d).attr("id")] = d3.select(d).node().checked
                    else
                        formData[d3.select(d).attr("id")] = d3.select(d).node().value
                })
                console.log(formData)
                d3.request("https://2ic80rmh30.execute-api.eu-central-1.amazonaws.com/dev/hello")
                    .post(JSON.stringify(formData, null, "  "))
                    .on("error", function (error) {
                        d3.select("input.submit").classed("hidden", true)
                        d3.select(".result--error").classed("hidden", false)
                        console.log("error occured posting form: ", error)
                    })
                    .on("load", function (xhr) {
                        d3.select("input.submit").classed("hidden", true)
                        d3.select(".result--success").classed("hidden", false)
                        console.log("form posted successfully", xhr)
                        evt.preventDefault()
                    })

                evt.stopPropagation()

            })
    }
};