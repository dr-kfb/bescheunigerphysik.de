<div class="features">
<div class="features__feature">

## Mitgliedschaft

Das _Forum Beschleunigerphysik_ besteht aus Mitgliedern der Gemeinschaft deutscher Beschleunigerphysikerinnen und -physiker. [Eine Registrierung erfolgt über diese Webpräsenz.](/forum/registrierung/) 

</div>
<div class="features__feature">

## Mitgliederversammlung

Einmal im Jahr findet eine Mitgliederversammlung des _Forum Beschleunigerphysik_ statt.

 
</div>
<div class="features__feature">


## Wahl des KfB

Alle Mitglieder des _Forum Beschleunigerphysik_ dürfen an den Wahlen des Komitees für Beschleunigerphysik teilnehmen. 

</div>
</div>