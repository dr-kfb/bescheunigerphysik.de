"use strict";

import {
    TeaserLinksEnhancer
} from "./_shared-components/teaser-links-enhancer/teaser-links-enhancer"

import {
    NavigationEnhancer
} from "./_shared-components/navigation/navigation-enhancer"

import {
    WeblinksRunner
} from "./weblinks/weblinks.js"

import {
    KfBHomeRunner
} from "./kfb/kfb__homepage.js"


import {
    KfBElectionsRunner
} from "./kfb/elections/kfb-elections.js"

import {
    ForumRegistrationRunner
} from "./forum/registration/forum-registration.js"


import {
    ForumLogoRunner
} from "./forum/logo/forum-logo.js"



import {
    PathiclesRunner
} from "./../components/c-pathicles/PathiclesRunner"


const WebFontLoader = require('webfontloader');

module.exports = {

    KfBHomeRunner: KfBHomeRunner,
    KfBElectionsRunner: KfBElectionsRunner,
    ForumRegistrationRunner: ForumRegistrationRunner,
    ForumLogoRunner: ForumLogoRunner,
    WeblinksRunner: WeblinksRunner,

    PathiclesRunner: PathiclesRunner,

    init: function () {

        // this.initBarba();

        this.performanceMark("AcceleratorLibrary.startInit")

        WebFontLoader.load({
            custom: {
                families: ['Lato'],
                urls: ['/_shared/css/fonts.css']
            }
        });
        this.performanceMark("AcceleratorLibrary.fontsLoaded")

        let t = new TeaserLinksEnhancer();
        let n = new NavigationEnhancer();


        this.performanceMark("AcceleratorLibrary.uxEnhanced")

        this.initPiwik();
        this.performanceMark("AcceleratorLibrary.piwikLoaded")
    },

    initPiwik: function () {
        
        // var _paq = _paq || [];
        // // tracker methods like "setCustomDimension" should be called before "trackPageView"
        // _paq.push(['trackPageView']);
        // _paq.push(['enableLinkTracking']);
        // (function() {
        //     var u="//piwik.beschleunigerphysik.de/piwik/";
        //     _paq.push(['setTrackerUrl', u+'piwik.php']);
        //     _paq.push(['setSiteId', '1']);
        //     var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        //     g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
        //     g.async = true;
        //     g.defer = true;
        // })();
    },

    performanceMark: function (mark) {
        if (!('performance' in window) ||
            !('timing' in window.performance) ||
            !('navigation' in window.performance)
        ) {
            // API not supported
        } else {
            if ('mark' in performance)
                performance.mark(mark)
        }
    },

    initBarba: function () {

        Barba.Pjax.start();

        var FadeTransition = Barba.BaseTransition.extend({
            start: function () {
                /**
                 * This function is automatically called as soon the Transition starts
                 * this.newContainerLoading is a Promise for the loading of the new container
                 * (Barba.js also comes with an handy Promise polyfill!)
                 */

                // As soon the loading is finished and the old page is faded out, let's fade the new page
                Promise
                    // .all([this.newContainerLoading , this.fadeOut()])
                    .all([this.newContainerLoading])
                    .then(this.fadeIn.bind(this));
            },

            fadeOut: function () {
                /**
                 * this.oldContainer is the HTMLElement of the old Container
                 */

                return $(this.oldContainer).animate({
                    opacity: 0
                }, 50).promise();
            },

            fadeIn: function () {
                /**
                 * this.newContainer is the HTMLElement of the new Container
                 * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
                 * Please note, newContainer is available just after newContainerLoading is resolved!
                 */

                var _this = this;
                var $el = $(this.newContainer);

                $(this.oldContainer).hide();

                $el.css({
                    visibility: 'visible',
                    opacity: 0
                });

                $(this.oldContainer).animate({
                    opacity: 0
                }, 150)

                $el.animate({
                    opacity: 1
                }, 150, function () {
                    /**
                     * Do not forget to call .done() as soon your transition is finished!
                     * .done() will automatically remove from the DOM the old Container
                     */

                    _this.done();
                });
            }
        });

        /**
         * Next step, you have to tell Barba to use the new Transition
         */

        Barba.Pjax.getTransition = function () {
            /**
             * Here you can use your own logic!
             * For example you can use different Transition based on the current page or link...
             */

            return FadeTransition;
        };


    }

};