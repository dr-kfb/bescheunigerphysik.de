"use strict";

var d3 = require("d3")

export class WeblinkListEnhancer {

    constructor() {

        let filterChange = function() {

            let event = d3.event;
            let toggleClassName = d3.select(event.target).attr("data-class-toggle__class-name");
            let toggleTargetSelector = d3.select(event.target).attr("data-class-toggle__target-selector");
            let toggleValue = d3.select(event.target).property("checked");
            d3.select(toggleTargetSelector).classed(toggleClassName, toggleValue)

        }

        d3.selectAll("input.class-toggle").on("change", filterChange);

    }

}
