"use strict";

var webpack = require("webpack");
var path = require("path")


let production = {
    // configuration
    entry: {

        "kfb": [
             path.join(__dirname, '_source/site/kfb.js')
        ],

        "accelerators": [
            path.join(__dirname, '_source/site/accelerators/accelerators.js')
        ]
    },
    output: {
        library: "AcceleratorsLibrary",
        path: path.join(__dirname, '_build/production/_shared/scripts/'),
        filename: "[name]--webpacked.js",
        publicPath: '/_build/production'

    },
    externals: {
        "d3": "d3",
        "d3tip": "d3tip",
        "barba.js": "barba.js",
        "topojson": "topojson"
    },


    resolve: {
        // directories where to look for modules

        extensions: [".js"],
        // extensions that are used

    },


    module: {
        rules: [

            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader?presets[]=es2015'
                
            },

            {
                enforce: "pre",
                test: /\.json?$/,
                loader: 'json-loader',
            }
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            output: {
                comments: false
            },
            mangle: true,
            sourcemap: false,
            debug: false,
            minimize: true,
            compress: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true
            }
        })],


    devServer: {
        inline: false,
        hot: false
    },
    stats: {
        colors: true,
        modules: false,
        reasons: true
    },
    watch: false,
    target: "web"
};


module.exports = production;