
# Local installation


## Prerequisite

For building the project, you need to have Git and [https://nodejs.org/en/](Node.js) installed on your system.

## Steps

1. Clone repository
2. Change to project's local folder
3. Run `npm install` (more fun: Use `yarn install` instead, for which you must have run `npm -g install yarn` before)
4. Run `grunt build` for building the project
5. Run `grunt connect` for starting local web server at [localhost:9001](http://localhost:9001)

# Architectural decisions

The project's architecture aims at objectives on the client, development, and server:

## Client

### Objectives

A user experience which is

- non-laggy,
- online and offline possible,
- non-limiting as for the device you use, (unless it is a operating system for which no sercurity patches are provided any more. Yes: No IE < 10)
- fun and activating.

### Stack

- **HTML**
(No explanation given)
- **SVG** for animated and interactive graphics
- **Resolution-adaptive CSS**
(Call it "responsive" if you prefer, which - once upon a time - was used for non-laggy user experiences, right? We are doubly responsive then :)
- **[D3.js](https://d3js.org)** as SVG layout engine
and decent jQuery replacement
- **[Three.js](https://threejs.org)** as WebGL library
(will be used in a coming version)
- **[Service Workers](https://www.w3.org/TR/service-workers/)** for offline usage
(will be used in a coming version)


## Development

### Objectives

* Productivity
* Testability
* Fun

### Stack

- **Git** as versioning tool
- **Node.js / NPM** as development foundation
- **Grunt** as build tool
(Might be replaced by Webpack later.)
- **[Webpack](https://webpack.github.io)** as module builder
No more stitching together too many JavaScript dependencies on the client and either wondering why on earth this turns out to work or spending way too much time to find the bits that don't mix well.
- **[Babel](https://babeljs.io)** as Ecmascript transpiler
(In memory of those) => {who have to code D3.js without arrow functions.}
- **[Stylus](http://stylus-lang.com)** as CSS pre-processor
Peculiarly, Stylus still hasn't gained the same popularity as its older siblings Sass or Less. But sure, to fancy Stylus you have to enjoy **not** wasting your day writing curly brackets.
- **PostCSS** for stuff like CSS autoprefixing
- **[Pug](https://pugjs.org/api)**  as template processor (formerly known as Jade)
After having used Pug for just one day, you will never want to write HTML end tags again. Pug is that good, it doesn't even need a good-looking website (at least this was true on [November 20, 2016](http://web.archive.org/web/20161005120640/https://pugjs.org/api/getting-started.html)).
- **[Markdown-it](https://markdown-it.github.io)** (and some plugins) as markdown processor
Why Markdown? == For composing structured text-based content without having to struggle with HTML mark-up (even if you use Pug, see above, with which you can make Markdown mix quite well)
Why Markdown-it? => Arguably, the best markdown processor for speed (It's pretty fast), literacy (it knows all important flavours), accuracy (It knows the flavours well), and extensibility (You can either extend it yourself or you grab one of the many plugins available, esp. markdown-it-decorate for applying css classes to your markdown.)
- **Nightmare.js** for automated screenshots


## Server / Operation

### Objectives

- **Serverlessliness**
No own web, let alone application or database server
(As of this writing, Google's result list for "serverless" includes more than 1,5 million pages, but it hasn't indexed "serverlessliness" yet. This is about to change :)
- **Speed**
response times << 1s,
load times < 1s
- **Multi-tier deployment architecture**
local-dev, dev, stage, prod (for code AND content development)
- **No pain** (if not fun)

### Stack

- **Github** as versioning repository
- **Amazon S3** as file hosting solution
- **Amazon Lambda** for form processing
- **Amazon Cloudfront** as Content Delevery Network (CDN) and for SSL handling

- Google Docs 


# Progressiveness

## D3.js prerendering


# Testing

## Linting

### CSS linting

* grunt-contrib-csslint


### HTML linting

### Weblinks check

### Javascript Runtime checking



PIWIKI SHOCK



# Objectives


## Speedy user experience


* automatic build process

## Wow-ish user experience

## Ease of development

* two-level seperation of code and content 
* dev/stage/prod environments
* automatic testing


# References


51
down vote
The 100 ms threshold was established over 30 yrs ago. See:

Card, S. K., Robertson, G. G., and Mackinlay, J. D. (1991). The information visualizer: An information workspace. Proc. ACM CHI'91 Conf. (New Orleans, LA, 28 April-2 May), 181-188.

Miller, R. B. (1968). Response time in man-computer conversational transactions. Proc. AFIPS Fall Joint Computer Conference Vol. 33, 267-277.

Myers, B. A. (1985). The importance of percent-done progress indicators for computer-human interfaces. Proc. ACM CHI'85 Conf. (San Francisco, CA, 14-18 April), 11-17.





# Licence

Copyright [2016] [Komitee für Beschleunigerphsik / Dirk Rathje]

The content of this project itself is licensed under the  [Attribution-NonCommercial-NoDerivatives 4.0 International](http://creativecommons.org/licenses/by-nc-nd/4.0/) license.

The underlying source code used to format and display that content is licensed under the [Apache Licence, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html). Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
