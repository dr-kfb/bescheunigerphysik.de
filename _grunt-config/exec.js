module.exports = function(grunt, options) {

    return {
        "webpack_production":
            './node_modules/webpack/bin/webpack.js -p --config webpack.config.production.js',

        "webpack_development":
            './node_modules/webpack/bin/webpack.js --config webpack.config.development.js',
        
    }
}
