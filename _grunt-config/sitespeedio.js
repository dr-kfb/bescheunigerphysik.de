"use strict";

module.exports = function (grunt, options) {


    return {

        "beschleunigerphysik_local_dev": {
            options: {
                urls: ['http://localhost:9001/'],
                browsertime: {
                    delay: 2000,
                    browser: 'chrome',
                    // "connectivity": 'cable',
                    iterations: 5,
                },
                urlsMetaData: [],
                outputFolder: '<%= config.paths.build.reports %>/sitespeedio/sitespeedio_local_dev'
            }
        },
        "beschleunigerphysik_local_prod": {
            options: {
                urls: ['http://localhost:9002/'],
                browsertime: {
                    delay: 2000,
                    browser: 'chrome',
                    // "connectivity": 'cable',
                    iterations: 3,
                },
                urlsMetaData: [],
                outputFolder: '<%= config.paths.build.reports %>/sitespeedio/sitespeedio_local_prod'
            }
        },
        "beschleunigerphysik_dev": {
            options: {
                urls: ['https://dev.beschleunigerphysik.de/'],
                browsertime: {
                    delay: 2000,
                    preURL: "https://dev.beschleunigerphysik.de/",
                    browser: 'chrome',
                    // "connectivity": {
                    //     "profile": 'cable',
                    //     // "engine": "tc"
                    // },
                    iterations: 1,
                },
                urlsMetaData: [],
                outputFolder: '<%= config.paths.build.reports %>/sitespeedio/sitespeedio_dev'
            }
        },
        "beschleunigerphysik_stage": {
            options: {
                urls: ['https://stage.beschleunigerphysik.de/'],
                browsertime: {
                    delay: 2000,
                    preURL: "https://stage.beschleunigerphysik.de/",
                    // "connectivity": {
                    //     "profile": '3gfast',
                    //     // "engine": "tc"
                    // },
                    iterations: 1,
                },
                urlsMetaData: [],
                outputFolder: '<%= config.paths.build.reports %>/sitespeedio/sitespeedio_stage'
            }
        }
    }
}