module.exports = function (grunt, options) {

    return {
        options: {
            csslintrc: '.csslintrc',
            quiet: true,
            formatters: [{
                id: 'junit-xml',
                dest: '<%= config.paths.build.reports %>/csslint_junit.xml'
            }]
        },
        strict: {
            options: {
                import: 2,
            },
            src: ['<%= config.paths.build.development %>/_shared/css/default.css']
        }
    }
}
