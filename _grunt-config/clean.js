"use strict";

module.exports = function (grunt, options) {

    return {
        build_dev: {
            src: ["<%= config.paths.build.development %>"]
        },
        build_prod: {
            src: ["<%= config.paths.build.production %>"]
        },
        htmllint: {
            src: ["<%= config.paths.build.reports %>/htmllint.xml"]
        }
    }
}