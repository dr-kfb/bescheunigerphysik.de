"use strict";

module.exports = function (grunt, options) {

    return {
        options: {
            "force": true,
            "reporter": "junit",
            "reporterOutput": "<%= config.paths.build.reports %>/htmllint.xml",
            "ignore": /Lorem ipsum/
        },
        "dev": {
            "options": {},
            "src": "<%= config.paths.build.development %>/**/*.html"
        },
        "prod": {
            "src": "<%=config.paths.build.prod%>/**/*.html"
        }
    }
}