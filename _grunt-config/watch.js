/* jshint node: true */

"use strict";


module.exports = function(grunt, options) {

    var path = require("path");

    return {
        "options": {
            "livereload": true,
            "nospawn": true
        },
        "copy": {
            "files": [
                "_source/data/**.*",
                "_source/media/**.*",
                "_source/scripts/require-config.js",
                "_source/images/accelerator-layouts/**.*",
                "_source/images/accelerator-components/**.*"
            ],
            "tasks": [
                "copy:dev"
            ]
        },

        "webpack": {
            "files": [
                "_source/**/*.js"
            ],
            "tasks": [
                "exec:webpack_development"
            ]
        },

        "imagemin": {
            "files": [
                "_source/images/**/*.*"
            ],
            "tasks": [
                "newer:imagemin", "stylus:dev"
            ]
        },
        "pug": {
            "files": [
                "_source/**/*.pug",
                "_source/**/*.md",
                "_source/**/*.yaml"
            ],
            "tasks": [
                "pug:navigation_dev", "pug:dev"
            ],

        },
        "stylus": {
            "files": [
                "_source/components/**/*.styl",
                "_source/styles/**/*.styl",
                "_source/site/**/*.styl",
            ],
            "tasks": [
                "stylus:dev"
            ]
        }
    };
};

