"use strict";

module.exports = function (grunt, options) {

    return {
        options: {},
        "target": {
            src: ['_build/dev/**/*.html', '_build/dev/**/*.js'],
            css: ['_build/dev/_shared/css/default.css'],
            dest: '_build/dev/_shared/css/default.purified.css'
        }
    }

}