/*eslint-env node*/

module.exports = function (grunt, options) {

    return {

        options: {
            awsProfile: 'kfb-s3',
            region: 'eu-central-1',
            uploadConcurrency: 5,
            downloadConcurrency: 5,
            params: {
                CacheControl: 'max-age=60'
            }
        },
        put_development: {
            options: {
                bucket: 'dev.beschleunigerphysik.de',
                differential: true, // Only uploads the files that have changed
            },
            files: [

                {
                    expand: true,
                    cwd: '<%= config.paths.build.development %>',
                    src: ['**'],
                    dest: '',
                    params: {
                        CacheControl: 'max-age=3600'
                    }
                }
            ]
        },
        put_staging: {
            options: {
                bucket: 'stage.beschleunigerphysik.de',
                differential: true, // Only uploads the files that have changed
            },
            files: [

                {
                    expand: true,
                    cwd: '<%= config.paths.build.production %>',
                    src: ['_shared_hashed/**', '_shared_requirejs/**'],
                    dest: '',
                    params: {
                        CacheControl: 'max-age=31536000'
                    }
                }, {
                    expand: true,
                    cwd: '<%= config.paths.build.production %>',
                    src: ['**', '!_shared_hashed/**', '!_shared_requirejs/**'],
                    dest: '',
                    params: {
                        CacheControl: 'max-age=31536000'
                    }
                }


            ]
        },
        put_production:  {
            options: {
                bucket: 'www.beschleunigerphysik.de',
                differential: true, // Only uploads the files that have changed
            },
            files: [

                {
                    expand: true,
                    cwd: '<%= config.paths.build.production %>',
                    src: ['_shared_hashed/**', '_shared_requirejs/**'],
                    dest: '',
                    params: {
                        CacheControl: 'max-age=31536000'
                    }
                }, {
                    expand: true,
                    cwd: '<%= config.paths.build.production %>',
                    src: ['**', '!_shared_hashed/**', '!_shared_requirejs/**'],
                    dest: '',
                    params: {
                        CacheControl: 'max-age=31536000'
                    }
                }


            ]
        },
        clean_development: {
            options: {
                bucket: 'dev.beschleunigerphysik.de',
                debug: false // if true: doesn't actually delete but shows log
            },
            files: [{
                    dest: '/',
                    action: 'delete'
                }

            ]
        },
        clean_staging: {
            options: {
                bucket: 'stage.beschleunigerphysik.de',
                debug: false // if true: doesn't actually delete but shows log
            },
            files: [{
                    dest: '/',
                    action: 'delete'
                }

            ]
        }
    }
}