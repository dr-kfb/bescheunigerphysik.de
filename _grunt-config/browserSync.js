/*eslint-env node*/

module.exports = function (grunt, options) {

    return {

        dev: {
            bsFiles: {
                src: "<%= config.paths.build.development %>/**/*.*"
            },
            options: {
                notify: {
                    styles: {
                        top: 'auto',
                        bottom: '0'
                    }
                },
                watchTask: true,
                browser: ["google chrome"],
                server: {
                    baseDir: "<%= config.paths.build.development %>"
                }
            }
        }

    }
}