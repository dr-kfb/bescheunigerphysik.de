"use strict";

const Fs = require("fs")


module.exports = function (grunt, options) {


    return {

        "fonts_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/fonts/lato",
            "src": "**",
            "dest": "<%= config.paths.build.development %>/_shared/fonts/lato"
        },
        "fonts_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/fonts/lato",
            "src": "**",
            "dest": "<%= config.paths.build.production %>/_shared/fonts/lato"
        },

        "media_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/media",
            "src": "**",
            "dest": "<%= config.paths.build.development %>/_shared/media"
        },
        "media_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/media",
            "src": "**",
            "dest": "<%= config.paths.build.production %>/_shared/media"
        },


        "robotstxt_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/site",
            "src": "robots.txt",
            "dest": "<%= config.paths.build.development %>/"
        },
        "robotstxt_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/site",
            "src": "robots.txt",
            "dest": "<%= config.paths.build.production %>/"
        },


        "favicon_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/images/",
            "src": "favicon.ico",
            "dest": "<%= config.paths.build.development %>/"
        },
        "favicon_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/images/",
            "src": "favicon.ico",
            "dest": "<%= config.paths.build.production %>/"
        },



        "data_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/data",
            "src": ["maps/world-110m.json", "maps/ne-selection.topo.json"],
            "dest": "<%= config.paths.build.development %>/_shared/data"
        },
        "data_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/data",
            "src": ["maps/world-110m.json", "maps/ne-selection.topo.json"],
            "dest": "<%= config.paths.build.production %>/_shared/data"
        },



        "content_dev": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/content",
            "src": ["atlas/__atlas.json", "forum-members.json", "forum-affiliations.json"],
            "dest": "<%= config.paths.build.development %>/_shared/data"
        },
        "content_prod": {
            "expand": true,
            "cwd": "<%= config.paths.source %>/content",
            "src": ["atlas/__atlas.json", "forum-members.json", "forum-affiliations.json"],
            "dest": "<%= config.paths.build.production %>/_shared/data"
        },

        "dev_scripts": {
            "expand": true,
            "flatten": true,
            "cwd": "",
            "src": [
                "node_modules/dat.gui/build/dat.gui.css",
                "node_modules/dat.gui/build/dat.gui.js",
                "node_modules/mathbox/build/mathbox-bundle.js",
                "node_modules/topojson/dist/topojson.min.js",
                "node_modules/d3/build/d3.min.js", "_source/scripts/d3tip.js", "node_modules/jquery/dist/jquery.min.js", "node_modules/barba.js/dist/barba.js", "node_modules/barba.js/dist/barba.js.map",
                "node_modules/jquery/dist/jquery.min.js"
            ],
            "dest": "<%= config.paths.build.development %>/_shared/scripts/dev",
        },


        "dev_service_worker_registration": {
            "expand": true,
            "flatten": true,
            "cwd": "",
            "src": ["_source/scripts/service-worker-registration.js"],
            "dest": "<%= config.paths.build.development %>/_shared/scripts",
        }

        // {
        //     "expand": true,
        //     "cwd": "./_source/scripts",
        //     "src": ["require.js"],
        //     "dest": "<%= config.paths.build.development %>/_shared_requirejs",
        // },
        // "requirejs_prod": {
        //     "expand": true,
        //     "cwd": "./_source/scripts",
        //     "src": ["require.js"],
        //     "dest": "<%= config.paths.build.production %>/_shared_requirejs",
        // },
        // "requirejs_config_dev": {
        //     "expand": true,
        //     "cwd": "./_source/scripts",
        //     "src": ["require-config.js"],
        //     "dest": "<%= config.paths.build.development %>/_shared_requirejs",
        // },

        // "requirejs_config_prod": {
        //     "expand": true,
        //     "cwd": "./_source/scripts",
        //     "src": ["require-config.js"],
        //     "dest": "<%= config.paths.build.production %>/_shared_requirejs",
        //     "options": {

        //         "process": function (content, srcpath) {
        //             try {

        //                 let statsJSON = Fs.readFileSync("/Users/dirk/Projekte/kunde-KfB/beschleunigerphysik.de/201-web/beschleunigerphysik.de/_build/prod/_shared_requirejs/webpack-stats.json")


        //                 let stats = JSON.parse(statsJSON)
        //                 let accelerators_filename = "";
        //                 let kfb_filename = "";

        //                 if (stats.assetsByChunkName.accelerators.constructor === Array) {
        //                     accelerators_filename = stats.assetsByChunkName.accelerators[0]
        //                 } else {
        //                     accelerators_filename = stats.assetsByChunkName.accelerators

        //                 }
        //                 let replacement_accelerators = accelerators_filename.replace(".js", "")

        //                 if (stats.assetsByChunkName.kfb.constructor === Array) {
        //                     kfb_filename = stats.assetsByChunkName.kfb[0]
        //                 } else {
        //                     kfb_filename = stats.assetsByChunkName.kfb

        //                 }
        //                 let replacement_kfb = kfb_filename.replace(".js", "")

        //                 console.log("replacement_accelerators", replacement_accelerators)
        //                 content = content.replace("accelerators--webpacked", replacement_accelerators);
        //                 content = content.replace("accelerators--webpacked", replacement_accelerators);
        //                 return content.replace("kfb--webpacked", replacement_kfb);

        //             } catch (e) {

        //                 console.log(e)
        //                 return content
        //             }
        //         }
        //     }
        // }

    }
}