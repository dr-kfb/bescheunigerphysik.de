"use strict";


module.exports = function (grunt, options) {

    var postcss = function () {
        return require('poststylus')(
            [
                require('autoprefixer')(),
                'css-mqpacker'
           ]);
    };

    return {
        "dev": {
            "options": {
                "compress": false,
                "debug": true,
                "urlfunc": "data-uri",
                "resolve url": true,
                "use": [
                    postcss
                ],
                "paths": ['./']
            },
            "files": {
                "<%= config.paths.build.development %>/_shared/css/fonts.css":
                    "<%= config.paths.source %>/styles/fonts.styl",
                
                "<%= config.paths.build.development %>/_shared/css/accelerators.css":
                    "<%= config.paths.source %>/styles/__accelerators.styl",
                
                "<%= config.paths.build.development %>/_shared/css/default.css":
                    "<%= config.paths.source %>/styles/__default.styl",
                
                // "<%= config.paths.build.development %>/_shared/css/grid.css":
                //     "<%= config.paths.source %>/styles/__grid.styl",
                
                // "<%= config.paths.build.development %>/_shared/css/typography.css":
                //     "<%= config.paths.source %>/styles/__typography.styl",
        }
        },
        "prod": {
            "options": {
                "compress": true,
                "use": [
                    postcss
                ],
                "paths": []
            },
            "files": {
                "<%= config.paths.build.production %>/_shared/css/fonts.css":
                    "<%= config.paths.source %>/styles/fonts.styl",
                    
                "<%= config.paths.build.production %>/_shared/css/default.css":
                    "<%= config.paths.source %>/styles/__default.styl"
            }
        }
    }
}