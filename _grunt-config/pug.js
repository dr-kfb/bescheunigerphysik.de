'use strict';

module.exports = function (grunt, options) {

    const PATH = require("path");
    const FS = require("fs");
    const YAML = require('yamljs');
    const CSVParse = require('csv-parse/lib/sync');
    const Stringjs = require('string');
    const GLOB = require("glob")
    const GRAY = require("gray-matter")


    const PUG = require("pug")

    const Sitemap = require('../_bin/sitemap');
    let sitemapObject = new Sitemap(options.config.paths.source + '/content/sitemap.yaml');


    function objToStrMap(obj) {
        let strMap = new Map();
        for (let k of Object.keys(obj)) {
            strMap.set(k, obj[k]);
        }
        return strMap;
    }
    let md = require('markdown-it')
        ({
            html: true,
            linkify: true,
            typographer: true,
            quotes: '»«›‹'
        })
        .use(require('markdown-it-katex'))
        .use(require('markdown-it-deflist'))
        .use(require('markdown-it-decorate'))
        .use(require("markdown-it-sub"))
        .use(require("markdown-it-sup"))


    function supermarkdown(block) {

        if (!block) return null;
        let result = md.render(block)

        result = result.replace(/\s+@@\s+/g, '&nbsp;');
        result = result.replace(/\s+@\s+/g, '<span class="thinnbsp">&nbsp;</span>');

        return result;
    }

    function supermarkdownInline(block) {

        if (!block) return null;
        let result = md.renderInline(block)

        result = result.replace(/\s+@@\s+/g, '&nbsp;');
        result = result.replace(/\s+@\s+/g, '<span class="thinnbsp">&nbsp;</span>');

        return result;
    }


    const urlTranslatations = [{
        key: "elections/",
        val: "wahlen/"
    }, {
        key: "candidates/",
        val: "kandidaten-2016/"
    }, {
        key: "meetings/",
        val: "sitzungen/"
    }, {
        key: "statute/",
        val: "satzung/"
    }, {
        key: "registration/",
        val: "registrierung/"
    }, {
        key: "assemblies/",
        val: "vollversammlungen/"
    }, {
        key: "accelerators/",
        val: "beschleuniger/"
    }, {
        key: "/linking-nations/",
        val: "/weltweite-zusammenarbeit/"
    }, {
        key: "/brochure/",
        val: "/broschuere/"
    }, {
        key: "/operating-principle/",
        val: "/funktionsweise/"
    }, {
        key: "/what-whereby-wherefore/",
        val: "/was-womit-wozu/"
    }];


    let helpers = {
        supermarkdown: supermarkdown,
        supermarkdownInline: supermarkdownInline,


        getSitemap: function () {
            return sitemapObject
        },

        getContentOfFolder: function (pattern, sort = "ascending") {
            let result = []
            let filenames = GLOB.sync(pattern);
            if (sort != "descending")
                filenames.reverse()
            filenames.forEach(
                f => {

                    let obj = GRAY.read(f)
                    result.push(obj)
                }
            );

            return result

        },

        Stringjs: function (s) {
            return Stringjs(s);
        },
        slugify: function (s) {
            let step_1 = s.replace("ü", "ue").replace("ä", "ae").replace("ö", "oe").replace("ß", "ss");
            return Stringjs(step_1).slugify();
        },
        readJSON: function (path) {
            const fileContent = FS.readFileSync(path).toString();
            let obj = JSON.parse(fileContent);
            return obj;
        },
        readTSV: function (path) {
            const TSV = require("tsv");
            const fileContent = FS.readFileSync(path).toString();
            return TSV.parse(fileContent);
        },
        readCSV: function (path) {

            const fileContent = FS.readFileSync(path).toString();
            return CSVParse(fileContent, {
                delimiter: ";",
                columns: true
            });
        },
        readYAML: function (filepath) {
            let obj = YAML.load(filepath);
            return obj;
        },
        getAffiliations: function (filepath) {
            const fileContent = FS.readFileSync("./_source/content/forum-affiliations.json").toString();
            let obj = JSON.parse(fileContent);
            let affiliations = {};
            obj.forEach(entry => {
                affiliations[entry.id] = entry
            })
            return affiliations;
        },
        d3: require("d3"),
        fs: require('fs'),
        moment: require('moment')
    };

    let opts = {

        helpers: helpers

    };
    const navigation = PUG.renderFile("_source/site/_shared-components/navigation/navigation.pug", opts)

    return {
        "navigation_dev": {
            options: {
                debug: false,
                pretty: true,
                basedir: options.config.paths.source + '/site',
                data: {
                    navigation: navigation,
                    features: {
                        barbajs: false,
                        service_worker_cache: false
                    },
                    build_target: "dev",
                    helpers: helpers,
                    viewports: options.viewports,
                    atlasData: grunt.file.readJSON(options.config.paths.source + "/content/atlas/__atlas.json")
                },
                filters: {
                    supermarkdown: supermarkdown
                }
            },
            files: {
                "_build/tmp/navigation_dev.html": "_source/site/_shared-components/navigation/navigation.pug"
            }
        },
        "navigation_prod": {
            options: {
                debug: false,
                pretty: true,
                basedir: options.config.paths.source + '/site',
                data: {
                    features: {
                        barbajs: false,
                        service_worker_cache: false
                    },
                    build_target: "prod",
                    helpers: helpers,
                    viewports: options.viewports,
                    atlasData: grunt.file.readJSON(options.config.paths.source + "/content/atlas/__atlas.json")
                },
                filters: {
                    supermarkdown: supermarkdown
                }
            },
            files: {
                "_build/tmp/navigation_prod.html": "_source/site/_shared-components/navigation/navigation.pug"
            }
        },
        "dev": {
            options: {
                debug: false,
                pretty: true,
                basedir: options.config.paths.source + '/site',
                data: {
                    navigation: function () {
                        return FS.readFileSync("_build/tmp/navigation_dev.html").toString()
                    },
                    features: {
                        barbajs: false,
                        service_worker_cache: false
                    },
                    build_target: "dev",
                    helpers: helpers,
                    viewports: options.viewports,
                    atlasData: grunt.file.readJSON(options.config.paths.source + "/content/atlas/__atlas.json")
                },
                filters: {
                    supermarkdown: supermarkdown
                }
            },
            files: [{
                cwd: '<%= config.paths.source %>/site',
                src: ['**/*.pug', '!_mixins/*.pug', '!_shared-components/**/*.pug', '!_shared-jade/*.pug', '!xperiments/**/*.*'],
                dest: '<%= config.paths.build.development %>',
                expand: true,
                ext: '.html',
                rename: function (dest, matchedSrcPath) {

                    let newPath = matchedSrcPath;

                    try {
                        if (newPath) {
                            urlTranslatations.forEach(function (o) {
                                let regEx = new RegExp(o.key, "g");
                                newPath = newPath.replace(regEx, o.val);
                            });
                        }
                    } catch (error) {

                        console.error(error);
                    }
                    // console.log(newPath);
                    return PATH.join(dest, PATH.dirname(newPath), "index.html");
                }
            }]
        },
        "prod": {
            options: {
                debug: false,
                pretty: false,
                basedir: options.config.paths.source + '/site',
                data: {
                    navigation: function () {
                        return FS.readFileSync("_build/tmp/navigation_prod.html").toString()
                    },
                    features: {
                        barbajs: false,
                        service_worker_cache: false
                    },
                    build_target: "prod",
                    helpers: helpers,
                    viewports: options.viewports,
                    atlasData: grunt.file.readJSON(options.config.paths.source + "/content/atlas/__atlas.json")
                },
                filters: {
                    supermarkdown: supermarkdown
                }
            },
            files: [{
                cwd: '<%= config.paths.source %>/site',
                src: ['**/*.pug', '!_mixins/*.pug', '!_shared-components/**/*.pug', '!_shared-jade/*.pug', '!xperiments/**/*.*'],
                dest: '<%= config.paths.build.production %>',
                expand: true,
                ext: '.html',
                rename: function (dest, matchedSrcPath) {

                    let newPath = matchedSrcPath;

                    try {
                        if (newPath) {
                            urlTranslatations.forEach(function (o) {
                                let regEx = new RegExp(o.key, "g");
                                newPath = newPath.replace(regEx, o.val);
                            });
                        }
                    } catch (error) {

                        console.error(error);
                    }
                    // console.log(newPath);
                    return PATH.join(dest, PATH.dirname(newPath), "index.html");
                }
            }]


        }

    }
}