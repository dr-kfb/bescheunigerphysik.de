"use strict";

module.exports = function (grunt, options) {

    return {
        prod: {
            options: {
                baseDir: "<%= config.paths.build.production %>",
                assets: ['_shared/**'],
                outputDir: '_shared_hashed/',
                clearOutputDir: true
            },
            files: [{
                expand: true,
                cwd: "<%= config.paths.build.production %>",
                src: ['**/*.html', '**/*.js', '**/*.css']
            }]
        }
    }
}