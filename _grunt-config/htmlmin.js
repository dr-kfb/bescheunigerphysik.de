"use strict";

module.exports = function (grunt, options) {

    return {

        prod: { // Target
            options: { // Target options
                removeComments: true,
                collapseWhitespace: true
            },
            files: [{
                cwd: '<%= config.paths.build.production %>',
                src: ['**/*.html'],
                dest: '<%= config.paths.build.production %>',
                expand: true,
                ext: '.html'
            }]

        }
    }

}