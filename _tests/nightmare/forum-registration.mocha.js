"use strict;"
require('mocha-generators').install();

var expect = require('chai').expect;
var Nightmare = require('nightmare');
var options = {
    show: true
};

let prodBaseUrl = 'http://localhost:9002/';


describe('/forum/', function () {

    // before(function* () {
    //     nightmare = Nightmare(options);
    // });

    // after(function* () {
    //     var endTest = yield nightmare
    //         .end()
    // });
    this.timeout(4000);


    it('should list forum members', function* (done) {

        let numberOfMembers = yield new Nightmare()
            .goto(prodBaseUrl + "forum")
            .evaluate(function () {
                return document.querySelectorAll('tbody tr').length;
            })
            .end();
        expect(numberOfMembers).to.equal(399);
    });

    it('should list Wofgang Hillert', function* (done) {

        let numberOfMembers = yield new Nightmare()
            .goto(prodBaseUrl + "forum")
            .evaluate(function () {
                let hillertFound = false
                let tds = document.querySelectorAll('td');
                tds.forEach(td => { if (td.innerText.indexOf("Hillert") > -1) hillertFound = true })

                return hillertFound;
            })
            .end();
        expect(numberOfMembers).to.equal(true);
    });
});

// describe('/forum/registration/', function () {

//     this.timeout(4000);
//     let nightmare;
//     before(function* () {
//         nightmare = Nightmare(options);
//     });

//     after(function* () {
//         var endTest = yield nightmare
//             .end()
//     });

//     var selector = ".result--success"


//     it('Should show sucess message', function* (done) {

//         let nm = yield nightmare
//             .goto('http://localhost:9002/forum/registrierung')
//             .type('form [name=given-name]', 'Rathje')
//             .click('form [type=submit]')
//             .wait(2000)
//             .visible(selector)
//             .then((result) => {
//                 expect(result).to.eql(true);
//                 done();
//             }).catch((err) => {
//                 console.error("Test-runner failed:", err);
//             });

//     })
// })