"use strict";

var webpack = require("webpack");
var path = require("path")


let dev = {
    // configuration
    entry: {

        "kfb": [
             path.join(__dirname, '_source/site/kfb.js')
        ],

        "accelerators": [
            path.join(__dirname, '_source/site/accelerators/accelerators.js')
        ]
    },
    output: {
        library: "AcceleratorsLibrary",
        path: path.join(__dirname, '_build/development/_shared/scripts/'),
        filename: "[name]--webpacked.js",
        publicPath: '/_build/development'

    },
    externals: {
        "d3": "d3",
        "d3tip": "d3tip",
        "barba.js": "barba.js",
        "topojson": "topojson"
    },


    resolve: {
        // directories where to look for modules

        extensions: [".js"],
        // extensions that are used

    },


    module: {
        rules: [

            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader?presets[]=es2015'
                
            },

            {
                enforce: "pre",
                test: /\.json?$/,
                loader: 'json-loader',
            }
        ]
    },
    plugins: [
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: true
        //     }
        // }),
        // new BundleAnalyzerPlugin()
    ],

    devServer: {
        inline: true,
        hot: true
    },
    stats: {
        colors: true,
        modules: false,
        reasons: true
    },
    watch: false,
    target: "web"
};


module.exports = dev;