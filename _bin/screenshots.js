'use strict';

var Nightmare = require("Nightmare");
var moment = require("moment");

module.exports = function(grunt) {

    grunt.task.registerMultiTask(
        'screenshots',
        'Grunt plugin for creating visual histories of websites',
        function() {

            grunt.log.writeln(this.target + ': ' + this.data);

            // async
            var done = this.async();

            // remember self
            var self = this;

            // Merge task-specific and/or target-specific options with these defaults.
            var options = this.options({
                "viewports": [{
                    "name": "md",
                    "size": [980, 1800]
                }, {
                    "name": "lg",
                    "size": [1280, 1000]
                }, ],
                "output": "shots"
            });


            // async memory
            var shot_all = this.data.shots.length * options.viewports.length;
            var shot_cnt = 0;

            // loop viewports
            options.viewports.forEach(function(viewport) {

                // loop shots
                self.data.shots.forEach(function(shot) {

                    console.log("screenshotting " + viewport.size[0] + " x " + viewport.size[1] + " of " + shot.url)

                    // create nightmare
                    var nightmare = new Nightmare();

                    // create dir
                    var dir = options.output + "/" + shot.name;
                    var timestamp = ""; //moment().format("YYYY-MM-DD_hhmm");

                    // make dir
                    grunt.file.mkdir(dir);

                    // set viewport
                    nightmare.viewport(viewport.size[0], viewport.size[1]);

                    // goto url
                    nightmare.goto(shot.url);

                    // wait for loading
                    nightmare.wait(2000);

                    // run beforeShot hook, if function is provided
                    if (typeof shot.beforeShot == "function") {
                        shot.beforeShot(nightmare);
                    }

                    // create screenshot
                    nightmare.screenshot(dir + "/" + viewport.name + '.png');

                    // nightmare.pdf(dir + "/" + viewport.name + '.pdf');

                    // wait for screenshot saving to be finished
                    nightmare.wait(500)

                    // add end-command into nightmare queue
                    nightmare.run(function() {

                        // cnt shots
                        shot_cnt++;

                        // if all shots are done, complete task
                        if (shot_cnt >= shot_all) {
                            done();
                        }
                    });

                    // end nightmare instance
                    nightmare.end();

                });
            });

            // // loop viewports
            // options.viewports.forEach(function(viewport) {
            //
            //
            //
            //     // loop shots
            //     self.data.shots.forEach(function(shot) {
            //         console.log(shot)
            //
            //         // create nightmare
            //         var nightmare = new Nightmare();
            //
            //         // create dir
            //         var dir = options.output + "/" + shot.name;
            //         var timestamp = moment().format("YYYY-MM-DD_hhmm");
            //
            //         // make dir
            //         grunt.file.mkdir(dir);
            //
            //         // set viewport
            //         nightmare.viewport(viewport.size[0], viewport.size[1]);
            //
            //         // goto url
            //         nightmare.goto(shot.url);
            //
            //         // wait for loading
            //         nightmare.wait();
            //
            //         // run beforeShot hook, if function is provided
            //         if (typeof shot.beforeShot == "function") {
            //             shot.beforeShot(nightmare);
            //         }
            //
            //         // create screenshot
            //         nightmare.screenshot(dir + "/" + viewport.name + '.png');
            //
            //         // wait for screenshot saving to be finished
            //         nightmare.wait(1500)
            //
            //         // add end-command into nightmare queue
            //         nightmare.run(function() {
            //
            //             // cnt shots
            //             shot_cnt++;
            //
            //             // if all shots are done, complete task
            //             if (shot_cnt >= shot_all) {
            //                 done();
            //             }
            //         });
            //
            //         // end nightmare instance
            //         nightmare.end();
            //
            //     });
            // });


        });

};
