/**
 * Created by dirk on 03.05.16.
 */
"use strict";

// module.exports = function()
{

    let fs = require('fs');
    let path = require('path');
    let _ = require('lodash');
    const yamlFront = require('yaml-front-matter');
    const glob = require("glob");
    const d3 = require("d3");

    var sourceFilePaths = glob.sync("_source/content/atlas/*.md");
    console.info(sourceFilePaths)



    let objs = [];

    for (let sourceFilePath of sourceFilePaths) {

        const fileContent = fs.readFileSync(sourceFilePath, 'utf8');
        let obj = yamlFront.loadFront(fileContent);

        obj.body = obj['__content'];
        delete obj['__content'];
        objs.push(obj);
        // console.log(obj);
    }

    var nest = d3.nest()
        .key(function(d) {
            return d.type;
        })
        .sortKeys(d3.ascending)
        .sortValues((a,b) => a.name.localeCompare(b.name))
        .map(objs);

    console.info(JSON.stringify(nest, null, "  "));

    var outputFileName = path.join(__dirname, '../_source/content/atlas/__atlas.json');
    console.log("Writing map objects to " + outputFileName + ".");

    fs.writeFileSync(outputFileName, JSON.stringify(nest, null, "  "), "utf-8");

    console.log("Done.");

}
