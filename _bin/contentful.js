/**
 * Created by dirk on 03.05.16.
 */
"use strict";

// module.exports = function()
{

    var contentful = require('contentful');
    var fs = require('fs');
    var path = require('path');
    var client = contentful.createClient({
        space: 'lufo4p5n8b35',
        accessToken: '9e1d47bd791cf62237d326e6268b903b21580859649764b0a98ee4e5fbe2e735'
    });
    var wait = require('wait.for');
    wait.launchFiber(readAndSave);

    function readAndSave() {
        wait.for(readAndSaveAsync);
    }

    function readAndSaveAsync() {
        client.getEntries({
                'content_type': 'event'
            })
            .then(function(entries) {
                console.log("Reading events from contentful.com ...");

                var events = [];

                entries.items.forEach(function(d) {
                    var event = d.fields;
                    events.push(event);
                });
                events.sort(function(a, b) {
                    return a.date > b.date;
                });

                console.log(events.length + " events read.");

                var outputFile = path.join(__dirname, '../_source/data', 'events.json');
                console.log("Writing events to " + outputFile + ".");

                fs.writeFileSync(outputFile, JSON.stringify(events), "utf-8");
                console.log("Events written.");
            })


        client.getEntries({
                'content_type': 'newsItem'
            })
            .then(function(entries) {

                console.log("reading news items.");

                var newsItems = [];

                entries.items.forEach(function(d) {

                    var newsItem = d.fields;
                    newsItems.push(newsItem);
                });
                newsItems.sort(function(a, b) {
                    return a.date > b.date;
                });
                console.log(newsItems.length + " news items read.");

                var outputFile = path.join(__dirname, '../_source/data', 'newsItems.json');
                console.log("Writing news items to " + outputFile + ".");
                fs.writeFileSync(outputFile, JSON.stringify(newsItems), "utf-8");
                console.log("News items written.");
            })
    }
}
