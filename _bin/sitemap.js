'use strict';

const Yaml = require('js-yaml');
const Fs = require('fs');


module.exports = class Sitemap {

    constructor(path) {

        try {
            this.data = Yaml.safeLoad(Fs.readFileSync(path, 'utf8'));
            
        } catch (e) {
            console.log(e);
        }


        this.linearization = this.getLinearization(this.data, []);
        this.pageMap = new Map();
        this.linearization.forEach(p => this.pageMap.set(p.url, p))
    }

    getLinearization(o, result) {
        for (let i in o) {
            if (!!o[i] && typeof(o[i]) == 'object') {
                // console.log(o[i])
                if (o[i].title && o[i].url) {
                    result.push({
                        title: o[i].title,
                        url: o[i].url,
                        site: o[i].site,
                        header_supertitle: o[i].supertitle,
                        header_title: o[i].header_titlem ? o[i].header_title : o[i].title,
                        header_subtitle: o[i].subtitle,
                        hide_menu_item: o[i].hide_menu_item,
                        // name: o[i].outline + o[i].url.replace(/\/$/, '').replace(/\//g, '__'),
                        name: o[i].url.replace(/^\//, '').replace(/\/$/, '').replace(/\//g, '__')
                    });
                }
                this.getLinearization(o[i], result);
            }
        }
        return result
    }


    next(url) {

        let nextPage;
        this.linearization.forEach(
            (item, index) => {

                if (item.url === url) {
                    let tmpNextPage = this.linearization[index + 1];
                    if (tmpNextPage) return nextPage = tmpNextPage;
                }
            }
        );
        if (nextPage) {
            return nextPage;
        } else

        {
            return this.linearization[0];
        }
    }

    prev(url) {

        let prevPage;
        this.linearization.forEach(
            (item, index) => {

                if (item.url === url) {
                    let tmpPrevPage = this.linearization[index + 1];
                    if (tmpPrevPage) return prevPage = tmpPrevPage;
                }
            }
        );
        if (prevPage) {
            return prevPage;
        } else

        {
            return this.linearization[0];
        }
    }
};
