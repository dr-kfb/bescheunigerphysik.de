/**
 * Created by dirk on 03.05.16.
 */
"use strict";

// module.exports = function()
{


    var fs = require('fs');
    var path = require('path');
    var _ = require('lodash');
    var parse = require('csv-parse');


    var csvContent = fs.readFileSync('_source/content/atlas/_atlas-generator.csv').toString();




    parse(csvContent, {
        columns: true
    }, function(err, parsed) {

        console.log(parsed);

        _.forEach(parsed, function(value, key) {


            const file_id = value.type + "--" + value.id;

            let mdContent = "---\n";
            mdContent += makeYamlLine("id", value.id);
            mdContent += makeYamlLine("visible_on_forum_map", value.visible_on_forum_map);
            mdContent += makeYamlLine("visible_in_atlas", value.visible_in_atlas);
            mdContent += makeYamlLine("type", value.type);
            mdContent += makeYamlLine("accelerator_type", value.subtype);
            mdContent += makeYamlLine("name", value.name);
            mdContent += makeYamlLine("abbreviation", value.name_short);
            mdContent += makeYamlLine("electoral-group", value["electoral-group"]);

            mdContent += "urls: " + "\n";
            mdContent += "    -\n";
            mdContent += "        description: Webangebot (Zielgruppe Allgemeinheit)\n";
            mdContent += makeYamlLine("        url", value.url_pr);
            mdContent += "    -\n";
            mdContent += "        description: Webangebot (Zielgruppe Wissenschaft)\n";
            mdContent += makeYamlLine("        url", value.url_science);

            mdContent += makeYamlLine("description", value.typ);
            mdContent += "locations: " + "\n";
            mdContent += "    -\n";
            mdContent += makeYamlLine("        lng", value.lng);
            mdContent += makeYamlLine("        lat", value.lat);
            mdContent += makeYamlLine("city", value.location);
            mdContent += makeYamlLine("parent", value.parent);
            mdContent += makeYamlLine("size", value.size);
            mdContent += makeYamlLine("size_label", value.size_label);
            mdContent += makeYamlLine("energy", value.max_energy);
            mdContent += makeYamlLine("label_align", ((value.label_align) ? value.label_align : "left"));
            mdContent += makeYamlLine("label_dy", "" + ((value.label_dy) ? value.label_dy : 0));
            mdContent += makeYamlLine("label_dx", "" + ((value.label_dx) ? value.label_dx : 0));
            mdContent += makeYamlLine("particles", value.particles);


            mdContent += makeYamlLine("acceleration_physicists", value.acceleration_physicists);
            mdContent += makeYamlLine("acceleration_physicists__forum", value.acceleration_physicists__forum);
            mdContent += "---\n";



            var outputFileName = path.join(__dirname, '../_source/content/atlas', file_id + '.md');
            console.log("Writing map item to " + outputFileName + ".");
            console.log(mdContent);

            fs.writeFileSync(outputFileName, mdContent, "utf-8");

        });



        console.log("Done.");



    });



    function makeYamlLine(keyName, value) {

        if (value)
            return keyName + ": " + value + "\n";
        else
            return "";

    }

}
