/**
 * Created by dirk on 03.05.16.
 */
"use strict";

// module.exports = function()
{

    let fs = require('fs');
    let path = require('path');

    const yaml = require('js-yaml');



    const fileContent = fs.readFileSync("_source/content/kfb-meetings.json", 'utf8');
    let meetings = JSON.parse(fileContent);



    

    for (let meeting of meetings) {

        let y = "---\ntitle: " + meeting.title + "\n"
        y += "date: " + meeting.date + "\n"
        y += "location: " + meeting.location + "\n"
        y += "category: KfB-Sitzung\n"
        y += "archetype: event\n"

        y = y + "---\n"

        fs.writeFileSync("_source/content/kfb/events/" + meeting.date + ".md", y);

    }
    // let objs = [];

    // for (let sourceFilePath of sourceFilePaths) {


    //     obj.body = obj['__content'];
    //     delete obj['__content'];
    //     objs.push(obj);
    //     // console.log(obj);
    // }

    // var nest = d3.nest()
    //     .key(function(d) {
    //         return d.type;
    //     })
    //     .sortKeys(d3.ascending)
    //     .sortValues((a,b) => a.name.localeCompare(b.name))
    //     .map(objs);

    // console.info(JSON.stringify(nest, null, "  "));

    // var outputFileName = path.join(__dirname, '../_source/content/atlas/__atlas.json');
    // console.log("Writing map objects to " + outputFileName + ".");

    // fs.writeFileSync(outputFileName, JSON.stringify(nest, null, "  "), "utf-8");

    // console.log("Done.");

}
