/**
 * Created by dirk on 02.05.16.
 */
"use strict";
//

let unorm = require("unorm");
let fs = require('fs');
let glob = require('glob');
let path = require('path');

const optionDefinitions = [
    { name: 'write', alias: 'w', type: Boolean }
]

const commandLineArgs = require('command-line-args')(optionDefinitions)



glob("_source/content/**//*.md", function (er, files) {


    files.forEach(f => {

        let content = fs.readFileSync(f).toString();
        let contentNormed = unorm.nfc(content);
        if (content == contentNormed) {
            console.log("ok: ", f)
        } else {
            console.log("to be normed:", contentNormed);
            if (commandLineArgs.write) {
                fs.writeFileSync(f, contentNormed);
            } else {
                console.warn("changes not written to file. Use --write to change files.");
            }
        }

    })
})

