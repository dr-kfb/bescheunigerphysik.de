/**
 * Created by dirk on 03.05.16.
 */
"use strict";

// module.exports = function()
{

    var fs = require('fs');
    var path = require('path');
    var _ = require('lodash');
    var parse = require('csv-parse');
    var d3 = require('d3');


    var csvContent = fs.readFileSync('_data/mapData.csv').toString();


    parse(csvContent, {
        columns: true
    }, function(err, parsed) {

        console.log(parsed);

        var nest = d3.nest()
            .key(function(d) {
                return d.type;
            })
            .sortKeys(d3.ascending)
            .sortValues((a, b) => a.name_short.localeCompare(b.name_short))
            .map(parsed);

        console.info(JSON.stringify(nest, null, "  "));

        var outputFileName = path.join(__dirname, '../_source/data/atlas.json');
        console.log("Writing map objects to " + outputFileName + ".");

        fs.writeFileSync(outputFileName, JSON.stringify(nest, null, "  "), "utf-8");

        console.log("Done.");


    });



}
