/**
 * Created by dirk on 02.05.16.
 */
"use strict";

let fs = require('fs');
let path = require('path');
var Faker = require("../node_modules/faker/index.js");
Faker.locale = "de";

let titles = ["", "Dr.", "Prof. Dr."];
let electoralGroups = ["university", "helmholtz", "others", "abroad"];
let forum_members = [];

for (let i = 0; i < 500; i++) {

    let anonymize = Math.random();
    let member = {};


    if (anonymize < 0.1) {

        member = {
            firstName: "*********",
            lastName: "*********",
            institute: Faker.company.companyName(),
            registrationYear: (2016 - ( Math.floor(Math.random() * 4)   )),
            title: "*********",
            electoralGroup: electoralGroups[Math.floor(Math.random() * (electoralGroups.length ))]
        };

    } else {

        member = {

            firstName: Faker.name.firstName(),
            lastName: Faker.name.lastName(),
            institute: Faker.company.companyName(),
            registrationYear: (2016 - ( Math.floor(Math.random() * 4)   )),
            title: titles[Math.floor(Math.random() * 3)],
            electoralGroup: electoralGroups[Math.floor(Math.random() * (electoralGroups.length ))]
        }
    }
    forum_members.push(member);
}

forum_members.sort(function (a, b) {

    if (a.lastName.startsWith("*")) return 1;

    if (b.lastName.startsWith("*")) return -1;

    if (a.lastName.localeCompare(b.lastName) != 0) {
        return a.lastName.localeCompare(b.lastName);
    } else {
        return a.firstName.localeCompare(b.firstName);
    }

});
console.log(forum_members)
fs.writeFileSync(path.join(__dirname, '../_source/data/forum-members.json'), JSON.stringify(forum_members), "utf-8");
